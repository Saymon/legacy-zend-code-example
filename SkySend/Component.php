<?php

/**
 * @author Saymon
 *
 * @method SkySend_Model_Manager modelManager()
 */
class SkySend_Component extends System_Component_Abstract
{

	use System_Singleton;

	//---------------------------------------------------------------------------------------------------------------------------------------

	/* (non-PHPdoc)
	 * @see System_Component_Abstract::update($objVersionCurrent)
	 */
	public function update($objVersionCurrent)
	{
// 		$strVersion = '0.0.2';
// 		if ($objVersionCurrent->isOlder($strVersion))
// 		{
// 			$objVersionCurrent->setVersion($strVersion)->save();
// 		}
	}

	//---------------------------------------------------------------------------------------------------------------------------------------

	/**
	 * @return SkySend_Bridge_Abstract
	 */
	public function getBridge()
	{
		$bridgeName			= $this->getConfig()->bridgeName;

		$bridgeModelName	= 'SkySend_Bridge_' . ucfirst(strtolower($bridgeName));
		return new $bridgeModelName;
	}

}