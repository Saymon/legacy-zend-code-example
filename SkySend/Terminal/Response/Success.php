<?php

/**
 * @author Saymon
 */
abstract class SkySend_Terminal_Response_Success extends SkySend_Terminal_Response_Abstract
{

	/**
	 *
	 */
	public function __construct()
	{
		$this->setStatus('ok');
	}
	
}