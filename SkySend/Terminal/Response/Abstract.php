<?php

/**
 * @author Saymon
 */
abstract class SkySend_Terminal_Response_Abstract
{
	
	/**
	 * @var string
	 */
	protected $_status;
	
	/**
	 * @return string
	 */
	abstract public function render();
	
	/**
	 * @param string $value
	 * @return self
	*/
	public function setStatus($value)
	{
		$this->_status = $value;
	
		return $this;
	}
	
}