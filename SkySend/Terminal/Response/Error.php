<?php

/**
 * @author Saymon
 */
class SkySend_Terminal_Response_Error extends SkySend_Terminal_Response_Abstract
{
	
	/**
	 * @var array
	 */
	protected $_errors;
	
	/**
	 *
	 */
	public function __construct()
	{
		$this->setStatus('error');
	}
	
	/* (non-PHPdoc)
	 * @see SkySend_Terminal_Response_Abstract::render()
	 */
	public function render()
	{
		$response = [
			'status' => $this->_status,
			'errors' => $this->_errors,
		];
		
		return Zend_Json::encode($response);
	}
	
	/**
	 * @param array $value
	 * @return self
	 */
	public function setErrors(array $value)
	{
		$this->_errors = $value;

		return $this;
	}
	
	/**
	 * @param SkySend_Exception_Abstract $exception
	 * @return self
	 */
	public function setExceptionErrors(SkySend_Exception_Abstract $exception)
	{
		$errors = [$exception->getKey() => $exception->getMessage()];
			
		$this->setErrors($errors);
	
		return $this;
	}
	
	/**
	 * @param Core_Validation_Set $validator
	 * @return self
	 */
	public function setValidatorErrors(Core_Validation_Set $validator)
	{
		$errors = [];
			
		/* @var $errorItem Core_Validation_Error */
		foreach ($validator->getErrors() as $errorItem)
		{
			$item = $errorItem->getItem();
				
			/* @var $validatorErrors Core_Validate_Abstract */
			foreach ($item->getValidator()->errors() as $validatorErrors)
			{
				foreach ($validatorErrors['errors'] as $error)
				{
					$key = $item->getValidated()->getValidationName() . '_' . $error;
					$errors[] = [$key => $error];
				}
			}
		}
	
		$this->setErrors($errors);
	
		return $this;
	}
	
}