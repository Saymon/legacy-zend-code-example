<?php

/**
 * @author Saymon
 */
class SkySend_Terminal_Auth_Manager
{
	
	use System_Singleton;
	
	/**
	 * @var boolean
	 */
	private $_isIdentified = FALSE;
	
	/**
	 * @var boolean
	 */
	private $_isAuthorized = FALSE;
	
	/**
	 * @var System_Controller_Request_HTTP
	 */
	private $_httpRequest;
	
	/**
	 * @param System_Controller_Request_HTTP $request
	 * @return SkySend_Terminal_Auth_Manager
	 */
	public function setHttpRequest(System_Controller_Request_HTTP $request)
	{
		$this->_httpRequest = $request;

		return $this;
	}
	
	/**
     * Идентифицирует агента по ключу и подписи
	 */
	public function identify()
	{
		$crypter = System_Crypt_Hash_Manager::getInstance();
			
		$httpRequest = $this->_getHttpRequest();
		
		$authKey	= $httpRequest->getParam('auth_key');
		$authSign	= $httpRequest->getParam('auth_sign');
		
		if (!$authKey)
		{
			throw new SkySend_Terminal_Auth_Exception_EmptyAuthKey();
		}
		
		if (!$authSign)
		{
			throw new SkySend_Terminal_Auth_Exception_EmptyAuthSign();
		}
		
		$this
			->_verifyAuthKey($authKey)
			->_verifyAuthSign($authSign)
			->_setIdentified()
		;
		
		return $this;
	}
	
	/**
	 * Выполняет проверки на разрешенный IP адрес, и вызывает метод проверки у моста на соответствующую компоненту
	 */
	public function authorize()
	{
		$this->_authorizeIp();
		
		SkySend_Bridge_Manager::getInstance()->getCurrent()->isServiceAvailable();
		
		$this->_setAuthorized();
	}
	
	/**
	 * @param string $key
	 * @return SkySend_Terminal_Auth_Manager
	 */
	protected function _verifyAuthKey($key)
	{
		$optionManager = SkySend_Option_Manager::getInstance();
		
		$authKeyHash = $optionManager->authKeyHash();
		
		if (!$this->_verifyHash($key, $authKeyHash))
		{
			throw new SkySend_Terminal_Auth_Exception_InvalidAuthKey();
		}
		
		return $this;
	}
	
	/**
	 * @param string $sign
	 * @return SkySend_Terminal_Auth_Manager
	 */
	protected function _verifyAuthSign($sign)
	{
		$optionManager = SkySend_Option_Manager::getInstance();
		
		$authSignHash = $optionManager->authSignHash();
		
		if (!$this->_verifyHash($sign, $authSignHash))
		{
			throw new SkySend_Terminal_Auth_Exception_InvalidAuthSign();
		}
		
		return $this;
	}
	
	/**
	 * @param string $value
	 * @param string $hash
	 * @return boolean
	 */
	protected function _verifyHash($value, $hash)
	{
		$crypter = System_Crypt_Hash_Manager::getInstance();
		
		$optionManager = SkySend_Option_Manager::getInstance();
		
		$hashMethod = $optionManager->authHashMethod();
		
		$isValid = $crypter->verify($hashMethod, $value, $hash);
		
		return $isValid;
	}
	
	/**
	 * @throws SkySend_Terminal_Auth_Exception_IpUndefined
	 * @throws SkySend_Terminal_Auth_Exception_IpDenied
	 */
	protected function _authorizeIp()
	{
		$clientIP = $this->_getHttpRequest()->getClientIp();
		
		if (!$clientIP)
		{
			throw new SkySend_Terminal_Auth_Exception_IpUndefined();
		}
		
		$ipsAllowed = SkySend_Option_Manager::getInstance()->allowedIps();
		
		if (in_array($clientIP, $ipsAllowed))
		{
			return;
		}
		
		throw new SkySend_Terminal_Auth_Exception_IpDenied();
	}
	
	/**
	 * @return SkySend_Terminal_Auth_Manager
	 */
	protected function _setIdentified()
	{
		$this->_isIdentified = TRUE;
		
		return $this;
	}
	
	/**
	 * @return SkySend_Terminal_Auth_Manager
	 */
	protected function _setAuthorized()
	{
		$this->_isAuthorized = TRUE;
		
		return $this;
	}
	
	/**
	 * @return System_Controller_Request_HTTP
	 */
	protected function _getHttpRequest()
	{
		return $this->_httpRequest;
	}
	
}