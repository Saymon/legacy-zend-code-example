<?php

/**
 * @author Saymon
 */
class SkySend_Terminal_Auth_Exception_EmptyAuthSign extends SkySend_Terminal_Auth_Exception_Abstract
{
	
	/**
	 * @var string
	 */
	protected $_key = SkySend_Exception_Enum::SERVICE_TERMINAL_EMPTY_AUTH_SIGN;
	
}