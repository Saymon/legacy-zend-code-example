<?php

/**
 * @author Saymon
 *
 * @method SkySend_Terminal_Recharge_Row skysendTransactionId()
 * @method SkySend_Terminal_Recharge_Row skysendTransactionDatetimeCreated()
 * @method SkySend_Terminal_Recharge_Row skysendTerminalNumber()
 * @method SkySend_Terminal_Recharge_Row skysendReceiptNumber()
 * @method SkySend_Terminal_Recharge_Row recipientName()
 * @method SkySend_Terminal_Recharge_Row amountReceived()
 * @method SkySend_Terminal_Recharge_Row amountToEnroll()
 * @method SkySend_Terminal_Recharge_Row status()
 * @method SkySend_Terminal_Recharge_Row inserted()
 */
class SkySend_Terminal_Recharge_Row extends System_Db_Object implements Bank_PaymentOrder_InitialDocument_Interface
{

	/* (non-PHPdoc)
	 * @see System_Db_Object::_insert()
	 */
	protected function _insert()
	{
		if (!$this->skysendTerminalNumber())
		{
			$this->skysendTerminalNumber(0);
		}

		if (!$this->skysendReceiptNumber())
		{
			$this->skysendReceiptNumber(0);
		}

		$this
			->status(SkySend_Terminal_Recharge_Status_Enum::CREATED)
			->inserted($this->_currentDateUtc())
		;
	}

	//---------------------------------------------------------------------------------------------------------------------------------------

	/* (non-PHPdoc)
	 * @see Bank_PaymentOrder_InitialDocument_Interface::id()
	 */
	public function id()
	{
		return $this->id;
	}

	/* (non-PHPdoc)
	 * @see Bank_PaymentOrder_InitialDocument_Interface::getAccount()
	 */
	public function getAccount()
	{
		$bridge = SkySend_Bridge_Manager::getInstance()->getCurrent();
		return $bridge->getAccount($this->recipientName());
	}

	/* (non-PHPdoc)
	 * @see Bank_PaymentOrder_InitialDocument_Interface::amount()
	 */
	public function getAmount()
	{
		return $this->amountToEnroll();
	}

	/* (non-PHPdoc)
	 * @see Bank_PaymentOrder_InitialDocument_Interface::comments()
	 */
	public function getComments()
	{
		return 'Recharge through SkySend terminal. Transaction #' . $this->skysendTransactionId() . '';
	}

	/* (non-PHPdoc)
	 * @see Bank_PaymentOrder_InitialDocument_Interface::getPaymentOrder()
	 */
	public function getPaymentOrder()
	{
		return NULL;
	}

}