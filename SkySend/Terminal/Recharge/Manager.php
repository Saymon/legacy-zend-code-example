<?php

/**
 * @author Saymon
 * 
 * @method SkySend_Terminal_Recharge_Row createNew()
 */
class SkySend_Terminal_Recharge_Manager extends Core_Model_Manager
{
	
	/**
	 * @param integer $transactionId
	 * @param string $transactionDatetime
	 * @param integer $terminalNumber
	 * @param integer $receiptNumber
	 * @param string $recipientName
	 * @param float $amountReceived
	 * @param float $amountToEnroll
	 * @return SkySend_Terminal_Recharge_Row
	 */
	public function add($transactionId, $transactionDatetime, $terminalNumber, $receiptNumber, $recipientName, $amountReceived, $amountToEnroll)
	{
		$row = $this->createNew();
		$row
			->skysendTransactionId($transactionId)
			->skysendTransactionDatetimeCreated($transactionDatetime)
			->skysendTerminalNumber($terminalNumber)
			->skysendReceiptNumber($receiptNumber)
			->recipientName($recipientName)
			->amountReceived($amountReceived)
			->amountToEnroll($amountToEnroll)
			->save()
		;
		
		return $row;
	}
	
}