<?php

/**
 * @author Saymon
 */
class SkySend_Terminal_Recharge_Table extends Core_Db_Table_Abstract
{

    /**
     * @var string
     */
    protected $_name = 'skysend_terminal_recharge';

    /**
     * @var array
     */
    protected $_arrFields = [
        'id'									=> ['type' => 'bigint', 'length' => 20, 'not_null' => true, 'unsigned' => true, 'auto_increment' => true],
        'skysend_transaction_id'				=> ['type' => 'bigint', 'length' => 20, 'not_null' => true, 'unsigned' => true],
        'skysend_transaction_datetime_created'	=> ['type' => 'datetime', 'not_null' => true],
        'skysend_terminal_number'				=> ['type' => 'int', 'length' => 10],
        'skysend_receipt_number'				=> ['type' => 'int', 'length' => 10],
        'recipient_name'						=> ['type' => 'varchar', 'length' => 20, 'not_null' => true],
        'amount_received'						=> ['type' => 'decimal', 'float_dimension' => 'amount', 'not_null' => true, 'unsigned' => true],
        'amount_to_enroll'						=> ['type' => 'decimal', 'float_dimension' => 'amount', 'not_null' => true, 'unsigned' => true],
        'status'								=> ['type' => 'tinyint', 'length' => 3, 'not_null' => true, 'unsigned' => true, 'periodical' => 'SkySend_Terminal_Recharge_Status'],
        'inserted'								=> ['type' => 'datetime'],
    ];

    /**
     * @var array
     */
    protected $_arrKeys = [
        ['primary key', 'id'],
        ['unique key', 'skysend_transaction_id'],
        ['key', 'recipient_name'],
        ['key', 'skysend_terminal_number'],
        ['key', 'skysend_receipt_number'],
        ['key', 'status'],
    ];

}