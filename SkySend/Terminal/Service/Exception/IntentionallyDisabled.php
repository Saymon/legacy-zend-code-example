<?php

/**
 * @author Saymon
 */
class SkySend_Terminal_Service_Exception_IntentionallyDisabled extends SkySend_Terminal_Service_Exception
{
	
	/**
	 * @var string
	 */
	protected $_key = SkySend_Exception_Enum::SERVICE_TERMINAL_INTENTIONALLY_DISABLED;
	
}