<?php

/**
 * @author Saymon
 */
class SkySend_Terminal_Service_Exception_InvalidAction extends SkySend_Terminal_Service_Exception
{
	
	/**
	 * @var string
	 */
	protected $_key = SkySend_Exception_Enum::SERVICE_TERMINAL_INVALID_FUNCTION;
	
}