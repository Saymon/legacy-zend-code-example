<?php

/**
 * @author Saymon
 */
class SkySend_Terminal_Action_RechargePerform_Request extends SkySend_Terminal_Request_Abstract
{
	
	/**
	 * @var integer
	 */
	public $transactionId;
	
	/**
	 * @var string
	 */
	public $transactionDatetime;
	
	/**
	 * @var integer
	 */
	public $terminalNumber;
	
	/**
	 * @var integer
	 */
	public $receiptNumber;
	
	/**
	 * @var string
	 */
	public $recipientName;
	
	/**
	 * @var float
	 */
	public $amountReceived;
	
	/**
	 * @var float
	 */
	public $amountToEnroll;
	
	/**
	 * @param System_Controller_Request_HTTP $httpRequest
	 */
	public function fill(System_Controller_Request_HTTP $httpRequest)
	{
		$this->transactionId		= trim($httpRequest->getParam('transaction_id'));
		$this->transactionDatetime	= trim($httpRequest->getParam('transaction_datetime'));
		$this->terminalNumber		= trim($httpRequest->getParam('terminal_number'));
		$this->receiptNumber		= trim($httpRequest->getParam('receipt_number'));
		$this->recipientName		= trim($httpRequest->getParam('recipient_name'));
		$this->amountReceived		= trim($httpRequest->getParam('amount_received'));
		$this->amountToEnroll		= trim($httpRequest->getParam('amount_to_enroll'));
	}
	
	/* (non-PHPdoc)
	 * @see SkySend_Terminal_Request_Abstract::_validatorInit()
	 */
	protected function _initValidator()
	{
		$datetimeFormatPattern = '/' . System_Form_Element_Html5_Validate_Pattern::MYSQL_DATETIME . '/';
		
		$tblRecharge = new SkySend_Terminal_Recharge_Table();
		
		$this->_validator()
			->add(
					new Core_Validation_Validated_KeyValue('transaction_id', $this->transactionId),
					(new Core_Validation_Chain())
					->add(new Core_Validate_NotEmpty(), TRUE)
					->add(new Core_Validate_Int(), TRUE)
					->add(new System_Validate_Unsigned(), TRUE)
					->add(new Core_Validate_Digits(), TRUE)
					->add(new Core_Validate_Db_NoRecordExists([
						'adapter' => $tblRecharge->getAdapter(),
						'table'   => $tblRecharge->getTableName(),
						'field'   => 'skysend_transaction_id'
					]), TRUE)
			)
			->add(
					new Core_Validation_Validated_KeyValue('transaction_datetime', $this->transactionDatetime),
					(new Core_Validation_Chain())
					->add(new Core_Validate_NotEmpty(), TRUE)
					->add(new System_Validate_DateFormat($datetimeFormatPattern), TRUE)
			)
			->add(
					new Core_Validation_Validated_KeyValue('terminal_number', $this->recipientName),
					(new Core_Validation_Chain())
// 					->add(new Core_Validate_Int(), TRUE)
// 					->add(new System_Validate_Unsigned(), TRUE)
// 					->add(new Core_Validate_Digits(), TRUE)
			)
			->add(
					new Core_Validation_Validated_KeyValue('receipt_number', $this->recipientName),
					(new Core_Validation_Chain())
// 					->add(new Core_Validate_Int(), TRUE)
// 					->add(new System_Validate_Unsigned(), TRUE)
// 					->add(new Core_Validate_Digits(), TRUE)
			)
			->add(
					new Core_Validation_Validated_KeyValue('recipient_name', $this->recipientName),
					(new Core_Validation_Chain())
					->add(new Core_Validate_NotEmpty(), TRUE)
					->add(new System_Validate_Phone(), TRUE)
			)
			->add(
					new Core_Validation_Validated_KeyValue('amount_received', $this->amountReceived),
					(new Core_Validation_Chain())
					->add(new Core_Validate_NotEmpty(), TRUE)
					->add(new System_Validate_Amount_Format(System_Float::dim('amount')), TRUE)
					->add(new System_Validate_Unsigned(), TRUE)
					->add(new Core_Validate_GreaterThan(100), TRUE)
					->add(new Core_Validate_LessThan(15000), TRUE)
			)
			->add(
					new Core_Validation_Validated_KeyValue('amount_to_enroll', $this->amountToEnroll),
					(new Core_Validation_Chain())
					->add(new Core_Validate_NotEmpty(), TRUE)
					->add(new System_Validate_Amount_Format(System_Float::dim('amount')), TRUE)
					->add(new System_Validate_Unsigned(), TRUE)
			)
		;
	}
	
}