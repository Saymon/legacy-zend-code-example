<?php

/**
 * @author Saymon
 */
class SkySend_Terminal_Action_RechargePerform_Response extends SkySend_Terminal_Response_Success
{
	
	/**
	 * @var array
	 */
	protected $_result;
	
	/**
	 * @param array $result
	 */
	public function __construct(array $result)
	{
		$this->_result = $result;
		
		parent::__construct();
	}
	
	/* (non-PHPdoc)
	 * @see SkySend_Terminal_Response_Abstract::render()
	 */
	public function render()
	{
		$response = [
			'status' 	=> $this->_status,
			'result' 	=> ['transaction_id' => $this->_result['recharge_id'], 'transaction_uid' => $this->_result['recharge_uid']],
		];
		
		return Zend_Json::encode($response);
	}

}