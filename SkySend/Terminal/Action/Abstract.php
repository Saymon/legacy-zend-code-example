<?php

/**
 * @author Saymon
 */
abstract class SkySend_Terminal_Action_Abstract
{
	
	/**
	 * @var SkySend_Terminal_Auth_Manager
	 */
	private $_auth;
	
	/**
	 * @var SkySend_Terminal_Request_Abstract
	 */
	private $_request;
	
	/**
	 * @var SkySend_Terminal_Response_Abstract
	 */
	private $_response;
	
	abstract public function perform();
	
	/**
	 * @param SkySend_Terminal_Auth_Manager $auth
	 * @return SkySend_Terminal_Action_Abstract
	 */
	public function setAuth($auth)
	{
		$this->_auth = $auth;
		
		return $this;
	}
	
	/**
	 * @param SkySend_Terminal_Request_Abstract $terminalRequest
	 * @return SkySend_Terminal_Action_Abstract
	 */
	public function setRequest(SkySend_Terminal_Request_Abstract $terminalRequest)
	{
		$this->_request = $terminalRequest;
		
		return $this;
	}
	
	/**
	 * @return SkySend_Terminal_Response_Abstract
	 */
	public function getResponse()
	{
		return $this->_response;
	}
	
	/**
	 * @return SkySend_Terminal_Auth_Manager
	 */
	protected function _getAuth()
	{
		return $this->_auth;
	}
	
	/**
	 * @return SkySend_Terminal_Request_Abstract
	 */
	protected function _getRequest()
	{
		return $this->_request;
	}
	
	/**
	 * @param SkySend_Terminal_Response_Abstract $terminalResponse
	 * @return SkySend_Terminal_Action_Abstract
	 */
	protected function _setResponse(SkySend_Terminal_Response_Abstract $terminalResponse)
	{
		$this->_response = $terminalResponse;
		
		return $this;
	}
	
}