<?php

/**
 * @author Saymon
 */
class SkySend_Terminal_Action_RechargeOpportunityCheck_Model extends SkySend_Terminal_Action_Abstract
{
	
	/* (non-PHPdoc)
	 * @see SkySend_Terminal_Action_Abstract::perform()
	 */
	public function perform()
	{
		$request	= $this->_getRequest();
		
		$manager = new SkySend_Bridge_Manager();
		$bridge = $manager->getCurrent();
		
		$bridge->rechargeOpportunityCheck($request);
		$response = new SkySend_Terminal_Action_RechargeOpportunityCheck_Response();
		
		$this->_setResponse($response);
	}
	
}