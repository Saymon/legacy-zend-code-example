<?php

/**
 * @author Saymon
 */
class SkySend_Terminal_Action_RechargeOpportunityCheck_Response extends SkySend_Terminal_Response_Success
{
	
	/* (non-PHPdoc)
	 * @see SkySend_Terminal_Response_Abstract::render()
	 */
	public function render()
	{
		$response = [
			'status' 	=> $this->_status,
			'result'	=> 'yes',
		];
		
		return Zend_Json::encode($response);
	}
	
}