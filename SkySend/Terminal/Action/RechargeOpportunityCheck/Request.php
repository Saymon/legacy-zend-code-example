<?php

/**
 * @author Saymon
 */
class SkySend_Terminal_Action_RechargeOpportunityCheck_Request extends SkySend_Terminal_Request_Abstract
{
	
	/**
	 * @var string
	 */
	public $recipientName;
	
	/**
	 * @param System_Controller_Request_HTTP $httpRequest
	 */
	public function fill(System_Controller_Request_HTTP $httpRequest)
	{
		$this->recipientName = $httpRequest->getParam('recipient_name');
	}
	
	/* (non-PHPdoc)
	 * @see SkySend_Terminal_Request_Abstract::_validatorInit()
	 */
	protected function _initValidator()
	{
		$this->_validator()
			->add(
					(new Core_Validation_Validated_KeyValue('recipient_name', $this->recipientName)),
					(new Core_Validation_Chain())
						->add(new Core_Validate_NotEmpty(), TRUE)
						->add(new System_Validate_Phone(), TRUE)
			)
		;
	}
	
}