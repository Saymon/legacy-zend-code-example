<?php

/**
 * @author Saymon
 */
class SkySend_Terminal_Service
{
	
	/**
	 * @var SkySend_Terminal_Auth_Manager
	 */
	private $_auth;
	
	/**
	 * @return SkySend_Terminal_Auth_Manager
	 */
	public function auth()
	{
		if (is_null($this->_auth))
		{
			$this->_auth = SkySend_Terminal_Auth_Manager::getInstance();
		}
		
		return $this->_auth;
	}
	
	/**
	 * @param System_Controller_Request_HTTP $httpRequest
	 * @return SkySend_Terminal_Response_Abstract
	 */
	public function processRequest(System_Controller_Request_HTTP $httpRequest)
	{
		try
		{
			$this->_dumpRequest($httpRequest);
			$this->_authorize($httpRequest);
			$terminalRequest = $this->_route($httpRequest);
			$terminalRequest->validate();
			$response = $this->_dispatch($terminalRequest);
		}
		catch (SkySend_Terminal_Request_Exception_ValidatorError $e)
		{
			$response = new SkySend_Terminal_Response_Error();
			$response->setValidatorErrors($e->getValidator());
		}
		catch (SkySend_Exception_Abstract $e)
		{
			$response = new SkySend_Terminal_Response_Error();
			$response->setExceptionErrors($e);
		}
		catch (Exception $e)
		{
			$response = new SkySend_Terminal_Response_Error();
			$response->setErrors(['internalApplicationError' => $e->getMessage()]);
			(new Application_Error_Manager())->log($e, $httpRequest);
		}
		
		return $response;
	}
	
	/**
	 * @param System_Controller_Request_HTTP $httpRequest
	 */
	protected function _dumpRequest(System_Controller_Request_HTTP $httpRequest)
	{
		$paramDump = $httpRequest->getParam('dump_request');
		if (!$paramDump)
		{
			return;
		}
		
		Development_Debug::dumpDie($httpRequest->getParams());
	}
	
	/**
	 * @param System_Controller_Request_HTTP $httpRequest
	 * @throws SkySend_Terminal_Service_Exception_InnvalidAction
	 * @return SkySend_Terminal_Request_Abstract
	 */
	protected function _route(System_Controller_Request_HTTP $httpRequest)
	{
		$actionName = $httpRequest->getActionName();
		
		switch ($actionName)
		{
			case 'recharge-opportunity-check':
			{
				$terminalRequest = new SkySend_Terminal_Action_RechargeOpportunityCheck_Request();
				$terminalRequest
					->setMethodName('RechargeOpportunityCheck')
					->setHttpRequest($httpRequest)
				;
				break;
			}
			case 'recharge-perform':
			{
				$terminalRequest = new SkySend_Terminal_Action_RechargePerform_Request();
				$terminalRequest
					->setMethodName('RechargePerform')
					->setHttpRequest($httpRequest)
				;
				break;
			}
			default:
			{
				throw new SkySend_Terminal_Service_Exception_InvalidAction();
			}
		}
		
		$terminalRequest->fill($httpRequest);
		
		return $terminalRequest;
	}
	
	protected function _authorize(System_Controller_Request_HTTP $httpRequest)
	{
		$auth = $this->auth();
		
		$auth
			->setHttpRequest($httpRequest)
			->identify()
			->authorize()
		;
	}
	
	/**
	 * @param SkySend_Terminal_Request_Abstract $terminalRequest
	 * @return SkySend_Terminal_Response_Abstract
	 */
	protected function _dispatch(SkySend_Terminal_Request_Abstract $terminalRequest)
	{
		$action = $this->_actionFactory($terminalRequest);
		
		$action
			->setAuth($this->_auth)
			->setRequest($terminalRequest)
			->perform()
		;
		
		$response = $action->getResponse();
		
		return $response;
	}
	
	/**
	 * @param SkySend_Terminal_Request_Abstract $terminalRequest
	 * @return SkySend_Terminal_Action_Abstract
	 */
	protected function _actionFactory(SkySend_Terminal_Request_Abstract $terminalRequest)
	{
		$methodName = $terminalRequest->getMethodName();
		
		if ($methodName == 'RechargeOpportunityCheck')
		{
			$action = new SkySend_Terminal_Action_RechargeOpportunityCheck_Model();
		}
		
		if ($methodName == 'RechargePerform')
		{
			$action = new SkySend_Terminal_Action_RechargePerform_Model();
		}
		
		System_Exception_NotObject::getInstance()->validate($action);
		
		return $action;
	}
	
}