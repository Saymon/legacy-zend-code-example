<?php

/**
 * @author Saymon
 */
abstract class SkySend_Terminal_Request_Abstract
{
	
	/**
	 * @var string
	 */
	private $_methodName;
	
	/**
	 * @var Core_Validation_Set
	 */
	private $_validator;
	
	/**
	 * @var System_Controller_Request_HTTP
	 */
	private $_httpRequest;
	
	abstract protected function _initValidator();
	
	/**
	 * Validate current request
	 *
	 * @throws Wmt_Api_Request_Exception_ValidateError
	 */
	final public function validate()
	{
		$this->_validator = new Core_Validation_Set();
	
		$this->_initValidator();
	
		if (!$this->_validator()->isValid())
		{
			throw new SkySend_Terminal_Request_Exception_ValidatorError($this->_validator());
		}
	}
	
	/**
	 * @return string
	 */
	public function getMethodName()
	{
		return $this->_methodName;
	}
	
	/**
	 * @param string $name
	 * @return SkySend_Terminal_Request_Abstract
	 */
	public function setMethodName($name)
	{
		$this->_methodName = $name;
		
		return $this;
	}
	
	/**
	 * @param System_Controller_Request_HTTP $request
	 * @return SkySend_Terminal_Request_Abstract
	 */
	public function setHttpRequest(System_Controller_Request_HTTP $request)
	{
		$this->_httpRequest = $request;
		
		return $this;
	}
	
	/**
	 * @return System_Controller_Request_HTTP
	 */
	public function getHttpRequest()
	{
		return $this->_httpRequest;
	}
	
	/**
	 * @throws System_Exception_Null
	 * @return Core_Validation_Set
	 */
	final protected function _validator()
	{
		System_Exception_Null::getInstance()->validate($this->_validator);
	
		return $this->_validator;
	}
	
}