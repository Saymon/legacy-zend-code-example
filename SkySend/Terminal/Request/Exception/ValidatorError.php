<?php

/**
 * @author Saymon
 */
class SkySend_Terminal_Request_Exception_ValidatorError extends SkySend_Terminal_Service_Exception
{

	/**
	 * @var Core_Validation_Set
	 */
	private $_validator;
	
	/**
	 * @var string
	 */
	protected $_key = SkySend_Exception_Enum::SERVICE_TERMINAL_REQUEST_VALIDATION_ERROR;
	
	/**
	 * @param Core_Validation_Set $validator
	 */
	public function __construct(Core_Validation_Set $validator)
	{
		$this->_validator = $validator;
	
		parent::__construct();
	}
	
	/**
	 * @return Core_Validation_Set
	 */
	public function getValidator()
	{
		return $this->_validator;
	}
	
}