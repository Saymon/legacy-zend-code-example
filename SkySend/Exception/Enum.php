<?php

/**
 * @author Saymon
 */
class SkySend_Exception_Enum extends Core_Enum_Abstract
{
	
	//Routing errors
	const SERVICE_TERMINAL_INVALID_FUNCTION									= 'invalidFunction';

	//Service errors
	const SERVICE_TERMINAL_INTENTIONALLY_DISABLED							= 'serviceDisabled';
	
	//Authentication errors
	const SERVICE_TERMINAL_EMPTY_AUTH_KEY									= 'emptyAuthKey';
	const SERVICE_TERMINAL_EMPTY_AUTH_SIGN									= 'emptyAuthSign';
	const SERVICE_TERMINAL_INVALID_AUTH_CREDENTIALS							= 'invalidCredentials';
	
	//Authorization errors
	const SERVICE_TERMINAL_AUTHORIZATION_IP_DENIED							= 'ipDenied';
	const SERVICE_TERMINAL_AUTHORIZATION_IP_UNDEFINED						= 'ipUndefined';
	
	//Validation errors
	const SERVICE_TERMINAL_REQUEST_VALIDATION_ERROR							= 'validationError';
	
	//RechargeOpportunityCheck errors
	const SERVICE_TERMINAL_ACCOUNT_NOT_FOUND								= 'accountNotFound';
	const SERVICE_TERMINAL_WALLET_NOT_FOUND									= 'walletNotFound';
	const SERVICE_TERMINAL_ACCOUNT_SUSPENDED								= 'accountSuspended';
	
	//RechargePerform errors
	const SERVICE_TERMINAL_ACCOUNT_RECHARGE_FAILED							= 'rechargeFailed';
	
	//Other errors
	const INTERNAL_APPLICATION_ERROR										= 'internalApplicationError';
	
	const SKYSEND_CLIENT_EXCEPTION											= 'skysendClientException';
	
	/**
	 * @var array
	 */
	protected $_arrMessages = [
		self::SERVICE_TERMINAL_INVALID_FUNCTION								=> 'Invalid terminal service function name',
		
		self::SERVICE_TERMINAL_INTENTIONALLY_DISABLED						=> 'Terminal service is intentionally disabled',

		self::SERVICE_TERMINAL_EMPTY_AUTH_KEY								=> 'Empty auth key',
		self::SERVICE_TERMINAL_EMPTY_AUTH_SIGN								=> 'Empty auth sign',
		self::SERVICE_TERMINAL_INVALID_AUTH_CREDENTIALS						=> 'Invalid auth credentials',
		
		self::SERVICE_TERMINAL_AUTHORIZATION_IP_DENIED						=> 'IP undefined',
		self::SERVICE_TERMINAL_AUTHORIZATION_IP_UNDEFINED					=> 'IP denied',
		
		self::SERVICE_TERMINAL_REQUEST_VALIDATION_ERROR						=> 'SkySend terminal service request validation error',
		
		self::SERVICE_TERMINAL_ACCOUNT_NOT_FOUND							=> 'Account not found',
		self::SERVICE_TERMINAL_WALLET_NOT_FOUND								=> 'Wallet for currency WMTRUB not found',
		self::SERVICE_TERMINAL_ACCOUNT_SUSPENDED							=> 'Recharge operation is not available for account',
		
		self::SERVICE_TERMINAL_ACCOUNT_RECHARGE_FAILED						=> 'Account recharge failed',
		
		self::INTERNAL_APPLICATION_ERROR									=> 'Internal application error',
		
		self::SKYSEND_CLIENT_EXCEPTION										=> 'SkySend PHP client error'
	];
	
	/**
	 * @param string $value
	 * @return string
	 */
	public function getMessage($value)
	{
		$this->validate($value);
		return $this->_arrMessages[$value];
	}
	
}