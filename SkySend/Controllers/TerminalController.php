<?php

/**
 * @author Saymon
 */
class SkySend_TerminalController extends System_Controller_Base
{
	
	/**
	 * @var SkySend_Terminal_Service
	 */
	private $_service; 
	
	/* (non-PHPdoc)
	 * @see System_Controller_Base::init()
	 */
	public function init()
	{
		$this->_disableView();
		
		parent::init();
	}
	
	/**
	 * Проверка возможности пополнения счета
	 */
	public function rechargeOpportunityCheckAction()
	{
		$this->_processRequest();
	}
	
	/**
	 * Performing recharge
	 */
	public function rechargePerformAction()
	{
		$this->_processRequest();
	}
	
	/**
	 * @return SkySend_Terminal_Service
	 */
	protected function _getService()
	{
		if (is_null($this->_service))
		{
			$this->_service = new SkySend_Terminal_Service();
		}
		
		return $this->_service;
	}
	
	/**
	 * Выполняет запрос к сервису обслуживания терминалов
	 */
	protected function _processRequest()
	{
		$request = $this->getRequest();
		if (!$request->isPost())
		{
			return;
		}
		
		$response = $this->_getService()->processRequest($request);
		
		echo $response->render();
	}
	
}