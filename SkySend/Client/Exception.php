<?php

/**
 * @author Saymon
 */
class SkySend_Client_Exception extends SkySend_Exception_Abstract
{
	
	/**
	 * @param string $message
	 */
	public function __construct($message)
	{
		$this->setKey(SkySend_Exception_Enum::SKYSEND_CLIENT_EXCEPTION);
		
		parent::__construct($message);
	}
	
}