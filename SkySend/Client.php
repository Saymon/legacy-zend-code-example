<?php

/**
 * @author Saymon
 */
class SkySend_Client
{

	/**
	 * @var integer
	 */
	private $pointID = 7;

	/**
	 * @var string
	 */
	private $serverHost = 'work.inf-sys.ru';

	/**
	 * @var integer
	 */
	private $serverPort = 888;

	/**
	 * @var unknown
	 */
	private $agentKey = false;

	/**
	 * @var unknown
	 */
	private $serverKey = false;

	/**
	 * @var unknown
	 */
	private $cipher = 'DES-EDE3';

	/**
	 * @var integer
	 */
	private $cipher_key_len = 24;

	/**
	 * @var integer
	 */
	private $signer = OPENSSL_ALGO_SHA1;

	/**
	 * @var unknown
	 */
	public $answer = false;

	/**
	 * @param string $path
	 * @param string $passphrase
	 * @throws SkySend_Client_Exception
	 * @return SkySend_Client
	 */
	function LoadAgentPrivateKey($path, $passphrase)
	{
		if (!($this->agentKey = openssl_pkey_get_private(["file://$path", $passphrase])))
		{
			throw new SkySend_Client_Exception('Unable to load agent private key');
		}
		
		return $this;
	}

	/**
	 * @param string $path
	 * @throws SkySend_Client_Exception
	 * @return SkySend_Client
	 */
	function LoadServerPublicKey($path)
	{
		if (!($this->serverKey = openssl_pkey_get_public(["file://$path", ''])))
		{
			throw new SkySend_Client_Exception('Unable to load server public key');
		}
		
		return $this;
	}

	function Send($body)
	{
		$kod = openssl_random_pseudo_bytes($this->cipher_key_len);
		$iv = substr($kod, 0, min($this->cipher_key_len, openssl_cipher_iv_length($this->cipher)));
		$body_crypt = openssl_encrypt($body, $this->cipher, $kod, true, $iv);
		$body_lenght = strlen($body_crypt);
		
		if (! openssl_private_encrypt($kod, $kod_crypt, $this->agentKey))
		{
			echo "Error in openssl_private_encrypt\n";
			return 1;
		}
		
		$kod_crypt_base64 = base64_encode($kod_crypt);
		
		if (! openssl_sign($kod_crypt_base64 . $body_crypt, $signature, $this->agentKey, $this->signer))
		{
			echo "Error in openssl_sign\n";
			return 1;
		}
		
		$signature_base64 = base64_encode($signature);
		
		$packet = "POST /fcgixmld HTTP/1.0\r\n" . "Content-Type: skysend/xml\r\n" . "Sky-Point: {$this->pointID}\r\n" . "Sky-Sign: {$signature_base64}\r\n" . "Sky-Kod: {$kod_crypt_base64}\r\n" . "Content-length: {$body_lenght}\r\n" . "\r\n" . "{$body_crypt}";
		
		$fp = fsockopen($this->serverHost, $this->serverPort);
		fputs($fp, $packet);
		$answer = stream_get_contents($fp);
		fclose($fp);
		
		$pos = strpos($answer, "\r\n\r\n");
		$header = substr($answer, 0, $pos);
		$body_crypt = substr($answer, $pos + 4);
		
		$httpCode = $skyError = 0;
		$signature_base64 = $kod_crypt_base64 = '';
		
		$headers = explode("\r\n", $header);
		foreach ($headers as $header)
		{
			if (strpos($header, 'HTTP/1.0 ') === 0)
			{
				$httpCode = intval(substr($header, 9));
			}
			else 
				if (strpos($header, 'Sky-Error: ') === 0)
				{
					$skyError = intval(substr($header, 11));
				}
				else 
					if (strpos($header, 'Sky-Sign: ') === 0)
					{
						$signature_base64 = substr($header, 10);
					}
					else 
						if (strpos($header, 'Sky-Kod: ') === 0)
						{
							$kod_crypt_base64 = substr($header, 9);
						}
		}
		
		if (($httpCode != 200) || ($skyError != 100))
		{
			echo "Http code {$httpCode}; skyError {$skyError}\n";
			return 1;
		}
		
		$signature = base64_decode($signature_base64);
		
		if (openssl_verify($kod_crypt_base64 . $body_crypt, $signature, $this->serverKey, $this->signer) != 1)
		{
			echo "error in openssl_verify\n";
			return 1;
		}
		
		$kod_crypt = base64_decode($kod_crypt_base64);
		
		if (! openssl_private_decrypt($kod_crypt, $kod, $this->agentKey))
		{
			echo "error in openssl_private_decrypt\n";
			return 1;
		}
		
		if (strlen($kod) != $this->cipher_key_len)
		{
			echo "bad lenght kod\n";
			return 1;
		}
		
		$iv = substr($kod, 0, min($this->cipher_key_len, openssl_cipher_iv_length($this->cipher)));
		
		$body = openssl_decrypt($body_crypt, $this->cipher, $kod, true, $iv);
		
		echo $body;
	}

}