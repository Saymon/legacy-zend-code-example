<?php

/**
 * @author Saymon
 */
abstract class SkySend_Bridge_Abstract
{
	
	/**
	 * @param SkySend_Terminal_Request_Abstract $request
	 */
	abstract public function rechargeOpportunityCheck(SkySend_Terminal_Request_Abstract $request);
	
	/**
	 * @param SkySend_Terminal_Request_Abstract $request
	 */
	abstract public function rechargePerform(SkySend_Terminal_Request_Abstract $request);
	
	/**
	 * @return boolean
	 */
	abstract public function isServiceAvailable();
	
}