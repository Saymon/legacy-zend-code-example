<?php

/**
 * @author Saymon
 */
class SkySend_Bridge_Wmt_Exception_Terminal_AccountSuspended extends SkySend_Bridge_Exception_Abstract
{
	
	/**
	 * @var string
	 */
	protected $_key = SkySend_Exception_Enum::SERVICE_TERMINAL_ACCOUNT_SUSPENDED;
	
}