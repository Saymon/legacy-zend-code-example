<?php

/**
 * @author Saymon
 */
class SkySend_Bridge_Wmt extends SkySend_Bridge_Abstract
{
	
	/**
	 * @var Bank_Person_Row
	 */
	private $_bankPerson;
	
	/**
	 * @var UsersCenter_Account_Row
	 */
	private $_userAccount;
	
	/* (non-PHPdoc)
	 * Проверка включен ли сервис обслуживания терминалов
	 * @see SkySend_Bridge_Abstract::isServiceAvailable()
	 */
	public function isServiceAvailable()
	{
		$this->_isTerminalServiceEnabled();
	}
	
	/* (non-PHPdoc)
	 * @see SkySend_Bridge_Abstract::rechargeOpportunityCheck()
	 */
	public function rechargeOpportunityCheck(SkySend_Terminal_Request_Abstract $terminalRequest)
	{
		$this
			->_isWmtAccountExists($terminalRequest->recipientName)
			->_isWmtUserBanned()
			->_isBankAccountExists()
		;
	}
	
	/* (non-PHPdoc)
	 * @see SkySend_Bridge_Abstract::rechargePerform()
	 */
	public function rechargePerform(SkySend_Terminal_Request_Abstract $terminalRequest)
	{
		$this->rechargeOpportunityCheck($terminalRequest);
		/* @var $terminalRequest SkySend_Terminal_Action_RechargePerform_Request */
		$rechargeManager = new SkySend_Terminal_Recharge_Manager();
		$rowRecharge = $rechargeManager->add(
			$terminalRequest->transactionId, 
			$terminalRequest->transactionDatetime, 
			$terminalRequest->terminalNumber, 
			$terminalRequest->receiptNumber, 
			$terminalRequest->recipientName, 
			$terminalRequest->amountReceived, 
			$terminalRequest->amountToEnroll
		);
		
		$bank = new Bank_PaymentOrder_Manager();
		
		try
		{
			$rowRecharge->status(SkySend_Terminal_Recharge_Status_Enum::PROCESSING)->save();
			/* @var $order Bank_PaymentOrder_Model_Load */
			$order = $bank->performLoad($rowRecharge);
			$rowRecharge->status(SkySend_Terminal_Recharge_Status_Enum::COMPLETED)->save();
		}
		catch (Exception $e)
		{
			$rowRecharge->status(SkySend_Terminal_Recharge_Status_Enum::FAILED)->save();
			$httpRequest = $terminalRequest->getHttpRequest();
			$httpRequest->setParam('skysend_terminal_recharge_id', $rowRecharge->id());
			(new Application_Error_Manager())->log($e, $httpRequest);//логирование оригинальной ошибки
			throw new SkySend_Bridge_Wmt_Exception_Terminal_AccountRechargeFailed();
		}
		
		$result = [
			'recharge_id'	=> $order->getId(),
			'recharge_uid'	=> $order->getUid(),
		];
		
		return $result;
	}
	
	/**
	 * @param string $recipientName
	 * @return Bank_Account_Row
	 */
	public function getAccount($recipientName)
	{
		$bankPerson = Bank_Person_Manager::getInstance()->getByName($recipientName, TRUE);
		return $bankPerson->getAccountByCurrency('WMTRUB');
	}

	/**
	 * @throws SkySend_Terminal_Service_Exception
	 * @return SkySend_Bridge_Wmt
	 */
	protected function _isTerminalServiceEnabled()
	{
		if ((int)SkySend_Option_Manager::getInstance()->serviceTerminalStatus() !== SkySend_Terminal_Service_Status_Enum::ENABLED)
		{
			throw new SkySend_Terminal_Service_Exception_IntentionallyDisabled();
		}
		
		return $this;
	}
	
	/**
	 * @param string $bankPersonName
	 * @throws SkySend_Terminal_Service_Exception
	 * @return SkySend_Bridge_Wmt
	 */
	protected function _isWmtAccountExists($bankPersonName)
	{
		$userAccount = UsersCenter_Account_Manager::getInstance()->getByName($bankPersonName, FALSE);
		
		if (is_null($userAccount))
		{
			throw new SkySend_Bridge_Wmt_Exception_Terminal_AccountNotFound();
		}
		
		$this->_userAccount = $userAccount;
		
		$manager = new Bank_Person_Manager();
		
		$person = $manager->getByName($bankPersonName, FALSE);
		if (is_null($person))
		{
			throw new SkySend_Bridge_Wmt_Exception_Terminal_AccountNotFound();
		}
		
		$this->_bankPerson = $person;
		
		return $this;
	}
	
	/**
	 * @throws SkySend_Terminal_Service_Exception
	 * @return SkySend_Bridge_Wmt
	 */
	protected function _isWmtUserBanned()
	{
		if ($this->_userAccount->status() !== UsersCenter_Account_StatusList::NORMAL)
		{
			throw new SkySend_Bridge_Wmt_Exception_Terminal_AccountSuspended();
		}
		
		return $this;
	}
	
	/**
	 * @throws SkySend_Terminal_Service_Exception
	 * @return SkySend_Bridge_Wmt
	 */
	protected function _isBankAccountExists()
	{
		$bankAccount = $this->_bankPerson->getAccountByCurrency('WMTRUB', FALSE);
		if (is_null($bankAccount))
		{
			throw new SkySend_Bridge_Wmt_Exception_Terminal_WalletNotFound();
		}
		
		
		return $this;
	}
	
	
	
}