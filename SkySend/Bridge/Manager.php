<?php

/**
 * @author Saymon
 */
class SkySend_Bridge_Manager
{
	
	use System_Singleton;
	
	/**
	 * @var SkySend_Bridge_Abstract
	 */
	private $_current;
	
	/**
	 * @return SkySend_Bridge_Abstract
	 */
	public function getCurrent()
	{
		if (is_null($this->_current))
		{
			$this->_current = SkySend_Component::getInstance()->getBridge();
		}
		
		return $this->_current;
	}
	
}