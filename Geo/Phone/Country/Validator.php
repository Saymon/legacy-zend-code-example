<?php

/**
 * @author Saymon
 */
class Geo_Phone_Country_Validator extends Core_Validate_Abstract
{
    
    const COUNTRY_UNDEFINED     = 'undefinedCountry';
    const COUNTRY_DISALLOWED    = 'disallowedCountry';
    const INVALID_NUMBER_LENGTH = 'invalidNumberLength';
    
    /**
     * @var array
     */
    protected $_arrCountriesAllowed;
    
    /**
     * @param array $arrCountriesAllowed
     */
    public function __construct($arrCountriesAllowed = NULL)
    {
        $this->_arrCountriesAllowed = $arrCountriesAllowed;
    }
    
    /* (non-PHPdoc)
     * @see Core_Validate_Interface::isValid()
    */
    public function isValid($value)
    {
        $this->_setValue($value);
    
        $arrPhoneData = Geo_Phone_Manager::getInstance()->parseNumber($value);
        if (!System_Array::isArray($arrPhoneData)) 
        {
            $this->_error(self::COUNTRY_UNDEFINED);
            return FALSE;
        }
        
        if (System_Array::isArray($this->_arrCountriesAllowed) && !in_array($arrPhoneData['country_code'], $this->_arrCountriesAllowed)) 
        {
            $this->_error(self::COUNTRY_DISALLOWED);
            return FALSE;
        }
        
        if (!in_array($arrPhoneData['phone_number_length'], Geo_Phone_Manager::getInstance()->getNumberLengthVariantsByCountryCode($arrPhoneData['country_code']))) 
        {
            $this->_error(self::INVALID_NUMBER_LENGTH);
            return FALSE;
        }
    
        return TRUE;
    }
    
}