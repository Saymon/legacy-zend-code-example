<?php

/**
 * @author Saymon
 */
class Geo_Phone_Country_List
{

    use System_Singleton;

    //---------------------------------------------------------------------------------------------------------------------------------------

    /**
     * @var array
     */
    private $_arrData = [
        'UA' => ['code' => '380',	'length' => [12]],
        'RU' => ['code' => '7',		'length' => [11]],
        'GE' => ['code' => '995',	'length' => [12]],
        'TR' => ['code' => '90',	'length' => [12]],
        'BY' => ['code' => '375',	'length' => [12]],
        'MD' => ['code' => '373',	'length' => [11]],
        'BG' => ['code' => '359',	'length' => [12]],
        'HU' => ['code' => '36',	'length' => [11]],
    	'EE' => ['code' => '372',	'length' => [11]],
    	'US' => ['code' => '1',		'length' => [11]],
    	'KZ' => ['code' => '7',		'length' => [10]],
    	'LV' => ['code' => '371',	'length' => [11]],
    	'LT' => ['code' => '370',	'length' => [11]],
    	'PL' => ['code' => '48',	'length' => [11]],
    	'BR' => ['code' => '55',	'length' => [12]],
    	'PT' => ['code' => '351',	'length' => [12]],
    	'ES' => ['code' => '34',	'length' => [11]],
    	'DE' => ['code' => '49',	'length' => [11]],
    	'FR' => ['code' => '33',	'length' => [11]],
    	'CN' => ['code' => '86',	'length' => [13]],
    	'HK' => ['code' => '852',	'length' => [11]],
    	'MO' => ['code' => '853',	'length' => [11]],
    	'GB' => ['code' => '44',	'length' => [13]],
    	'BZ' => ['code' => '501',	'length' => [10]],
    ];

    //---------------------------------------------------------------------------------------------------------------------------------------

    /**
     * @param string $strCountryCode
     * @throws Geo_Phone_Country_Exception
     */
    private function _validateCountryCode($strCountryCode)
    {
        if (!isset($this->_arrData[$strCountryCode]))
        {
            throw new Geo_Phone_Country_Exception('Unknown country code: "' . $strCountryCode . '"');
        }
    }

    //---------------------------------------------------------------------------------------------------------------------------------------

    /**
     * @return array
     */
    public function getAll()
    {
        return $this->_arrData;
    }

    public function getPhoneCodeByCountryCode($strCountryCode)
    {
		$this->_validateCountryCode($strCountryCode);

		return $this->_arrData[$strCountryCode]['code'];
    }

    /**
     * @param string $strCountryCode
     * @return array
     */
    public function getPhoneNumberLengthVariants($strCountryCode)
    {
        $this->_validateCountryCode($strCountryCode);

        return $this->_arrData[$strCountryCode]['length'];
    }

}