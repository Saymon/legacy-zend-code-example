<?php

/**
 * @author Saymon
 */
class Geo_Phone_Manager
{

    use System_Singleton;

    //---------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Парсит номер телефона и возвращает массив вида
     * ['country_phone_code' => 380, 'phone_number' => 501234567, 'country_code' => 'UA'] или
     * NULL, если не заматчилась ни одна страна
     * @param string $strNumber
     * @return array|NULL
     */
    public function parseNumber($strNumber)
    {
        $ssNumber = System_String::f($strNumber);
        if ($ssNumber->isStartedWith('+'))
        {
            $ssNumber->trimLeft('+');
        }

        $strMatchedCountryCode = '';
        $intMaxLength = 0;
        $arrCountriesList = Geo_Phone_Country_List::getInstance()->getAll();
        foreach ($arrCountriesList as $strCounrtyCode => $arrData)
        {
            if ($ssNumber->isStartedWith($arrData['code']))
            {
                $intLength = strlen($arrData['code']);
                if ($intLength > $intMaxLength)
                {
                    $intMaxLength = $intLength;
                    $strMatchedCountryCode = $strCounrtyCode;
                }
            }
        }

        if (!$intMaxLength)
        {
            return NULL;
        }

        return [
            'country_code'        => $strMatchedCountryCode,
            'country_phone_code'  => $arrCountriesList[$strMatchedCountryCode]['code'],
            'phone_number_length' => $ssNumber->length(),
            'phone_number'        => $ssNumber->part(strlen($arrCountriesList[$strMatchedCountryCode]['code']))->__toString(),
        ];
    }

    /**
     * @param string $strCountryCode
     * @return array
     */
    public function getNumberLengthVariantsByCountryCode($strCountryCode)
    {
        return Geo_Phone_Country_List::getInstance()->getPhoneNumberLengthVariants($strCountryCode);
    }

}