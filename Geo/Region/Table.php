<?php

/**
 * @author Saymon
 */
class Geo_Region_Table extends Core_Db_Table_Abstract
{

    /**
     * @var string
     */
    protected $_name = 'geo_region';

    /**
     * @var array
     */
    protected $_arrFields = [
        'id' => ['type' => 'int', 'length' => 10, 'not_null' => true, 'unsigned' => true, 'auto_increment' => true],
        'country_id' => ['type' => 'int', 'length' => 10, 'not_null' => true, 'unsigned' => true],
        'country_code' => ['type' => 'varchar', 'length' => 255, 'not_null' => true],
        'name' => ['type' => 'varchar', 'length' => 255, 'not_null' => true],
        'state_count' => ['type' => 'int', 'length' => 10, 'unsigned' => true],
        'city_count' => ['type' => 'int', 'length' => 10, 'unsigned' => true],
    ];

    /**
     * @var array
     */
    protected $_arrKeys = [
        ['primary key', 'id'],
        ['unique key', ['name', 'country_id']],
        ['unique key', ['name', 'country_code']],
        ['key', 'country_id'],
        ['key', 'country_code'],
        ['key', 'name'],
        ['key', 'state_count'],
        ['key', 'city_count'],
    ];

}