<?php

/**
 * @author Saymon
 */
class Geo_Region_Manager extends Core_Model_Manager
{

    use System_Singleton;

    /**
     * @param string $strCode
     * @return System_Db_List
     */
    public function getAllByCountry($strCode)
    {
    	Geo_Country_Enum::getInstance()->validate($strCode);

        $table = $this->getTable();

        $arrFilter = [
            'country_code = ?' => $strCode
        ];

        return $table->fetchAll($arrFilter);
    }

    /**
     * Обновляет аггрегационное поле state_сount в таблице регионов, на основании данных из таблицы geo_state
     */
    public function updateStateCount()
    {
        $lstRegions = $this->getTable()->fetchAll();
        foreach ($lstRegions as $rowRegion) {
            $lstStates = Geo_State_Manager::getInstance()->getAllByRegion($rowRegion->id());
            $rowRegion->stateCount($lstStates->count())
                      ->save();
        }
    }

    /**
     * Обновляет аггрегационное поле city_сount в таблице регионов, на основании данных из таблицы geo_city
     */
    public function updateCityCount()
    {
        $lstRegions = $this->getTable()->fetchAll();
        foreach ($lstRegions as $rowRegion) {
            $lstCity = Geo_City_Manager::getInstance()->getAllByRegion($rowRegion->id());
            $rowRegion->cityCount($lstCity->count())
                      ->save();
        }
    }

}