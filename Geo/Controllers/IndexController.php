<?php

/**
 * @author Saymon
 */
class Geo_IndexController extends System_Controller_Base
{
	
	/**
	 * Пересчет всех аггрегационных счетчиков
	 */
	public function aggregationCountersUpdateAction()
	{
        Geo_Component::getInstance()->aggregationCountersRecalculate();
	}

}