<?php

/**
 * @author Saymon
 */
class Geo_CityController extends System_Controller_Db_Table
{

    /**
     * Возвращает JSON для jquery autocomplete
     */
    public function autocompleteSearchAction()
    {
        $this->_disableView();

        $strCountryCode = $this->_getParam('cntr');
        $strTerm = $this->_getParam('term');
        if (is_null($strTerm) || !$strCountryCode) {
            return;
        }

        if (preg_match('/^[\d]*$/', $strTerm)) {
            return;
        }

        $strTerm = mb_strtolower($strTerm, 'UTF-8');

        $slct = $this->_table->select();
        $slct->where('city LIKE ?', '%' . $strTerm . '%')
             ->where('country_code = ?', $strCountryCode)
             ->limit(100)
             ;

        $lst = $this->_table->fetchAll($slct);

        $arrResults = [];
        $i = 0;
        foreach ($lst as $rowCity) {
            $arrResults[$i]['id']         = $rowCity->id();
            $arrResults[$i]['name']       = $rowCity->city();
            $arrResults[$i]['details']    = $rowCity->getDetailsString();
            $arrResults[$i]['country']    = $rowCity->countryCode();
            $arrResults[$i]['region_id']  = $rowCity->regionId();
            $arrResults[$i]['state_id']   = $rowCity->stateId();
            $arrResults[$i]['term']       = $strTerm;
            $i++;
        }

        echo Zend_Json::encode($arrResults);
    }

}