<?php

/**
 * @author Saymon
 *
 * @method Geo_Model_Manager modelManager()
 */
class Geo_Component extends System_Component_Abstract
{

    use System_Singleton;

    //---------------------------------------------------------------------------------------------------------------------------------------

	/**
	 * Загрузка данных из дампа при инсталляции
	 *
	 * @return void
	 */
	protected function _importFromDump()
	{
		ini_set('memory_limit', 1000000000);
		$path = APP_FOLDER . DIRECTORY_SEPARATOR . 'install' . DIRECTORY_SEPARATOR . $this->getComponentRow()->name() . DIRECTORY_SEPARATOR . 'dump.sql';

		if (!System_File::isFile($path))
		{
			return;
		}

		$arrStrings = file($path);
		$objDbAdapter = System_Db_Manager::getInstance()->getActualAdapter();

		foreach ($arrStrings as $strSqlQuery)
		{
			$objDbAdapter->query($strSqlQuery);
		}
	}

	//---------------------------------------------------------------------------------------------------------------------------------------

    /* (non-PHPdoc)
     * @see System_Component_Abstract::update()
     */
    public function update($objVersionCurrent)
    {
//         $strVersion = '0.0.2';
//         if ($objVersionCurrent->isOlder($strVersion))
//         {
//             $objVersionCurrent->setVersion($strVersion)->save();
//         }
    }

    //---------------------------------------------------------------------------------------------------------------------------------------

    /* (non-PHPdoc)
     * @see System_Component_Abstract::installData()
     */
    public function installData()
    {
    	$this->_importFromDump();

    	return parent::installData();
    }

    //---------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Пересчет аггрегационных счетчиков
     *
     * @return void
     */
    public function aggregationCountersRecalculate()
    {
        Geo_Country_Manager::getInstance()->updateRegionCount();
        Geo_Country_Manager::getInstance()->updateStateCount();
        Geo_Country_Manager::getInstance()->updateCityCount();

        Geo_Region_Manager::getInstance()->updateStateCount();
        Geo_Region_Manager::getInstance()->updateCityCount();

        Geo_State_Manager::getInstance()->updateCityCount();
    }

}