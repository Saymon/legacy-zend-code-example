<?php

/**
 * @author Saymon
 */
class Geo_Country_Table extends Core_Db_Table_Abstract
{

    /**
     * @var string
     */
    protected $_name = 'geo_country';

    /**
     * @var string
     */
    protected $_primary = 'code';

    /**
     * @var array
     */
    protected $_arrFields = [
        'id'            => ['type' => 'int', 'length' => 11, 'not_null' => true, 'unsigned' => true, 'auto_increment' => true],
        'name_ru'       => ['type' => 'varchar', 'length' => 255, 'not_null' => true],
        'name_en'       => ['type' => 'varchar', 'length' => 255],
        'currency_code' => ['type' => 'varchar', 'length' => 255, 'not_null' => true],
        'currency'      => ['type' => 'varchar', 'length' => 255],
        'code'          => ['type' => 'varchar', 'length' => 10, 'not_null' => true],
        'region_count'  => ['type' => 'int', 'length' => 10, 'unsigned' => true],
        'state_count'   => ['type' => 'int', 'length' => 10, 'unsigned' => true],
        'city_count'    => ['type' => 'int', 'length' => 10, 'unsigned' => true],
        'phone_code'    => ['type' => 'varchar', 'length' => 10],
    ];

    /**
     * @var array
     */
    protected $_arrKeys = [
        ['primary key', 'code'],
        ['unique key', 'id'],
        ['unique key', 'name_ru'],
        ['unique key', 'name_en'],
        ['key', 'region_count'],
        ['key', 'state_count'],
        ['key', 'city_count'],
        ['key', 'phone_code'],
    ];

}