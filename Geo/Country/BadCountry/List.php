<?php

/**
 * @author Storm
 */
class Geo_Country_BadCountry_List
{

	use System_Singleton;

	//---------------------------------------------------------------------------------------------------------------------------------------

	/**
	 * @var array
	 */
	private $_bannedCountry = [
		Geo_Country_Enum::ALBANIA,
		Geo_Country_Enum::AFGHANISTAN,
		Geo_Country_Enum::ALGERIA,
		Geo_Country_Enum::AMERICAN_SAMOA,
		Geo_Country_Enum::ANTIGUA_AND_BARBUDA,
		Geo_Country_Enum::ARUBA,
		Geo_Country_Enum::ANGOLA,
		Geo_Country_Enum::BENIN,
		Geo_Country_Enum::BHUTAN,
		Geo_Country_Enum::BURUNDI,
		Geo_Country_Enum::BANGLADESH,
		Geo_Country_Enum::BOLIVIA,
		Geo_Country_Enum::CAPE_VERDE,
		Geo_Country_Enum::CENTRAL_AFRICAN_REPUBLIC,
		Geo_Country_Enum::CHAD,
		Geo_Country_Enum::COMOROS,
		Geo_Country_Enum::CONGO,
		Geo_Country_Enum::CONGO_DEMOCRATIC_REPUBLIC,
		Geo_Country_Enum::COTE_D_IVOIRE,
		Geo_Country_Enum::DJIBOUTI,
		Geo_Country_Enum::EAST_TIMOR,
		Geo_Country_Enum::EGYPT,
		Geo_Country_Enum::ERITREA,
		Geo_Country_Enum::ETHIOPIA,
		Geo_Country_Enum::ECUADOR,
		Geo_Country_Enum::FALKLAND_ISLANDS,
		Geo_Country_Enum::GAMBIA,
		Geo_Country_Enum::GHANA,
		Geo_Country_Enum::GUADELOUPE,
		Geo_Country_Enum::GREECE,
		Geo_Country_Enum::HONDURAS,
		Geo_Country_Enum::IRAQ,
		Geo_Country_Enum::INDONESIA,
		Geo_Country_Enum::KENYA,
		Geo_Country_Enum::KIRIBATI,
		Geo_Country_Enum::KUWAIT,
		Geo_Country_Enum::LEBANON,
		Geo_Country_Enum::LESOTHO,
		Geo_Country_Enum::LIBERIA,
		Geo_Country_Enum::MOZAMBIQUE,
		Geo_Country_Enum::MOROCCO,
		Geo_Country_Enum::MYANMAR,
		Geo_Country_Enum::NIGER,
		Geo_Country_Enum::NIGERIA,
		Geo_Country_Enum::NEPAL,
		Geo_Country_Enum::PAKISTAN,
		Geo_Country_Enum::PARAGUAY,
		Geo_Country_Enum::PHILIPPINES,
		Geo_Country_Enum::THAILAND,
		Geo_Country_Enum::TANZANIA,
		Geo_Country_Enum::TRINIDAD_AND_TOBAGO,
		Geo_Country_Enum::TURKEY,
		Geo_Country_Enum::TURKMENISTAN,
		Geo_Country_Enum::SYRIA,
		Geo_Country_Enum::SRI_LANKA,
		Geo_Country_Enum::SOMALIA,
		Geo_Country_Enum::SAO_TOME_AND_PRINCIPE,
		Geo_Country_Enum::SUDAN,
		Geo_Country_Enum::VIETNAM,
		Geo_Country_Enum::VENEZUELA,
		Geo_Country_Enum::WESTERN_SAHARA,
		Geo_Country_Enum::YEMEN,
		Geo_Country_Enum::ZAMBIA,
		Geo_Country_Enum::ZIMBABWE,
	];

	/**
	 * @var array
	*/
	private  $_badCountry = [
		Geo_Country_Enum::UKRAINE,
		Geo_Country_Enum::ANDORRA,
		Geo_Country_Enum::ANTIGUA_AND_BARBUDA,
		Geo_Country_Enum::BARBADOS,
		Geo_Country_Enum::BAHRAIN,
		Geo_Country_Enum::BELIZE,
		Geo_Country_Enum::BRUNEI_DARUSSALAM,
		Geo_Country_Enum::VANUATU,
		Geo_Country_Enum::ANGUILLA,
		Geo_Country_Enum::BERMUDA,
		Geo_Country_Enum::VIRGIN_ISLANDS_BRITISH,
		Geo_Country_Enum::MONTSERRAT,
		Geo_Country_Enum::GIBRALTAR,
		Geo_Country_Enum::CAYMAN_ISLANDS,
		Geo_Country_Enum::ISLE_OF_MAN,
		Geo_Country_Enum::GRENADA,
		Geo_Country_Enum::DJIBOUTI,
		Geo_Country_Enum::IRELAND,
		Geo_Country_Enum::CYPRUS,
		Geo_Country_Enum::CHINA,
		Geo_Country_Enum::COSTA_RICA,
		Geo_Country_Enum::COOK_ISLANDS,
		Geo_Country_Enum::LIBERIA,
		Geo_Country_Enum::LEBANON,
		Geo_Country_Enum::LIECHTENSTEIN,
		Geo_Country_Enum::LUXEMBOURG,
		Geo_Country_Enum::MAURITIUS,
		Geo_Country_Enum::MALAYSIA,
		Geo_Country_Enum::MALDIVES,
		Geo_Country_Enum::MALTA,
		Geo_Country_Enum::MARSHALL_ISLANDS,
		Geo_Country_Enum::NAURU,
		Geo_Country_Enum::NETHERLANDS_ANTILLES,
		Geo_Country_Enum::NIUE,
		Geo_Country_Enum::UNITED_ARAB_EMIRATES,
		Geo_Country_Enum::PANAMA,
		Geo_Country_Enum::MACAU,
		Geo_Country_Enum::SAMOA,
		Geo_Country_Enum::SEYCHELLES,
		Geo_Country_Enum::SAINT_KITTS_AND_NEVIS,
		Geo_Country_Enum::SAINT_VINCENT_AND_THE_GRENADINES,
		Geo_Country_Enum::PUERTO_RICO,
		Geo_Country_Enum::TONGA,
		Geo_Country_Enum::FIJI,
		Geo_Country_Enum::FRENCH_POLYNESIA,
		Geo_Country_Enum::SRI_LANKA,
		Geo_Country_Enum::LATVIA,
		Geo_Country_Enum::PALAU,
		Geo_Country_Enum::MONTENEGRO,
	];

	/**
	 * @var array
	 */
	private $_availableCountry = [
		Geo_Country_Enum::UKRAINE,//BAD
		Geo_Country_Enum::RUSSIA,
		Geo_Country_Enum::GEORGIA,
// 		Geo_Country_Enum::TURKEY,//BANNED
		Geo_Country_Enum::BELARUS,
		Geo_Country_Enum::MOLDOVA,
		Geo_Country_Enum::BULGARIA,
		Geo_Country_Enum::HUNGARY,
		Geo_Country_Enum::ESTONIA,
		Geo_Country_Enum::UNITED_STATES,
		Geo_Country_Enum::KAZAKHSTAN,
		Geo_Country_Enum::LATVIA,//BAD
		Geo_Country_Enum::LITHUANIA,
		Geo_Country_Enum::POLAND,
		Geo_Country_Enum::BRAZIL,
		Geo_Country_Enum::PORTUGAL,
		Geo_Country_Enum::SPAIN,
		Geo_Country_Enum::GERMANY,
		Geo_Country_Enum::FRANCE,
		Geo_Country_Enum::CHINA,//BAD
		Geo_Country_Enum::HONG_KONG,
		Geo_Country_Enum::MACAU,//BAD
		Geo_Country_Enum::UK,
		Geo_Country_Enum::BELIZE,//BAD
	];

	//---------------------------------------------------------------------------------------------------------------------------------------

	/**
	 * @return array
	*/
	public function getBannedAndBadCountry()
	{
		return array_unique(array_merge($this->_badCountry, $this->_bannedCountry));
	}

	/**
	 * @return array
	 */
	public function getBannedCountry()
	{
		return $this->_bannedCountry;
	}

	/**
	 * @return array
	 */
	public function getBadCountry()
	{
		return $this->_badCountry;
	}

	/**
	 * @return array
	 */
	public function getAvailableCountry()
	{
		return $this->_availableCountry;
	}

	/**
	 * @param boolean $bTitle
	 * @param string $language
	 * @return multitype:array
	 */
	public function getAvailableCountryOptions($bTitle = FALSE, $language = 'en')
	{
		$arrAllCountry = Geo_Country_Enum::getInstance()->getAll();
		$arrAvailableCountry = $this->getAvailableCountry();
		$arrExclude = array_diff($arrAllCountry, $arrAvailableCountry);

		return Geo_Country_Enum::getInstance()->getOptions($bTitle, $arrExclude, $language);
	}

}