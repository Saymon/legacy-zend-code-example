<?php

/**
 * @author Saymon
 */
class Geo_Country_List
{
    
    use System_Singleton;
    
    /**
     * @var array
     */
    protected $_arrCountries = [
        'UA' => ['name' => 'Ukraine'],
        'RU' => ['name' => 'Russia'],
        'GE' => ['name' => 'Georgia'],
        'TR' => ['name' => 'Turkey'],
        'BY' => ['name' => 'Belarus'],
        'MD' => ['name' => 'Moldova'],
        'BG' => ['name' => 'Bulgari'],
    ];
    
    /**
     * @deprecated
     * @var array
     */
    protected $_arrParamsAllowed = ['code', 'name', 'phone'];

    /**
     * @deprecated
     * @param string $strMethodName
     * @throws System_Exception
     * @return void
     */
    protected function _validateMethodName($strMethodName)
    {
        if (!System_String::f($strMethodName)->isStartedWith('get')) {
            throw new System_Exception('Geo_Country_List Exception: invalid method called: "' . $strMethodName . '".');
        }
        
        $arrParams = $this->_parseParamsFromMethodName($strMethodName);
        
        if (count($arrParams) != 2) {
            throw new System_Exception('Geo_Country_List Exception: invalid method called: "' . $strMethodName . '".');
        }
        
        foreach ($arrParams as $strParamName) {
            $strParamName = strtolower($strParamName);
            if (!in_array($strParamName, $this->_arrParamsAllowed)) {
                throw new System_Exception('Geo_Country_List Exception: invalid method called: "' . $strMethodName . '".');
            }
        }
    }
    
    /**
     * @deprecated
     * @param string $strMethodName
     * @return array
     */
    protected function _parseParamsFromMethodName($strMethodName)
    {
        return System_String::f($strMethodName)->replace('get', '')->toArray('By');
    }
    
    /**
     * @deprecated
     * @param string $strCode
     * @param string $strDesiredParamName
     * @throws System_Exception
     * @return string
     */
    protected function _getSomethingByCode($strCode, $strDesiredParamName)
    {
        if (!array_key_exists($strCode, $this->_arrCountries)) {
            throw new System_Exception('Country with code "' . $strCode . '" doesn\'t exists in list.');
        }
        
        return $this->_arrCountries[$strCode][$strDesiredParamName];
    }
    
    /**
     * @deprecated
     * @param unknown_type $strTag
     * @param unknown_type $strDesiredParamName
     */
    protected function _getSomethigBySomething($strTag, $strDesiredParamName)
    {
        
    }
    
    /**
     * @return array  
     */
    public function getAll()
    {
        return $this->_arrCountries;
    }
    
    /**
     * @return array 
     */
    public function getMultiOptions()
    {
        $arrMultiOptions = [];
        foreach ($this->_arrCountries as $strCode => $arrData) {
            $arrMultiOptions[$strCode] = $arrData['name'];
        }
        
        return $arrMultiOptions;
    }
    
    /**
     * @deprecated
     * @param string $strMethodName
     * @param array $arrArgs
     * @return string
     */
    public function __call($strMethodName, array $arrArgs)
    {
        $this->_validateMethodName($strMethodName);
        $arrParams = $this->_parseParamsFromMethodName($strMethodName);
        
        if ($arrParams[0] == 'code') {
            return $this->_getSomethingByCode($arrParams[0], $arrParams[1]);
        }
        
        return $this->_getSomethigBySomething($arrParams[0], $arrParams[1]);
    }
    
}