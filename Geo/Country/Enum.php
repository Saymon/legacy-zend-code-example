<?php

/**
 * @author Saymon
 */
class Geo_Country_Enum extends Core_Enum_Abstract
{

    use System_Singleton;

    //---------------------------------------------------------------------------------------------------------------------------------------

    const AUSTRALIA = 'AU';
    const AUSTRIA = 'AT';
    const AZERBAIJAN = 'AZ';
    const ALBANIA = 'AL';
    const ALGERIA = 'DZ';
    const AMERICAN_SAMOA = 'AS';
    const ANGUILLA = 'AI';
    const ANGOLA = 'AO';
    const ANDORRA = 'AD';
    const ANTIGUA_AND_BARBUDA = 'AG';
    const ARGENTINA = 'AR';
    const ARMENIA = 'AM';
    const ARUBA = 'AW';
    const AFGHANISTAN = 'AF';
    const BAHAMAS = 'BS';
    const BANGLADESH = 'BD';
    const BARBADOS = 'BB';
    const BAHRAIN = 'BH';
    const BELARUS = 'BY';
    const BELIZE = 'BZ';
    const BELGIUM = 'BE';
    const BENIN = 'BJ';
    const BERMUDA = 'BM';
    const BULGARIA = 'BG';
    const BOLIVIA = 'BO';
    const BOSNIA_AND_HERZEGOVINA = 'BA';
    const BOTSWANA = 'BW';
    const BRAZIL = 'BR';
    const BRUNEI_DARUSSALAM = 'BN';
    const BURKINA_FASO = 'BF';
    const BURUNDI = 'BI';
    const BHUTAN = 'BT';
    const VANUATU = 'VU';
    const UK = 'GB';
    const HUNGARY = 'HU';
    const VENEZUELA = 'VE';
    const VIRGIN_ISLANDS_BRITISH = 'VG';
    const US_VIRGIN_ISLANDS = 'VI';
    const EAST_TIMOR = 'TL';
    const VIETNAM = 'VN';
    const GABON = 'GA';
    const HAITI = 'HT';
    const GUYANA = 'GY';
    const GAMBIA = 'GM';
    const GHANA = 'GH';
    const GUADELOUPE = 'GP';
    const GUATEMALA = 'GT';
    const GUINEA = 'GN';
    const GUINEA_BISSAU = 'GW';
    const GERMANY = 'DE';
    const GIBRALTAR = 'GI';
    const HONDURAS = 'HN';
    const HONG_KONG = 'HK';
    const GRENADA = 'GD';
    const GREENLAND = 'GL';
    const GREECE = 'GR';
    const GEORGIA = 'GE';
    const GUAM = 'GU';
    const DENMARK = 'DK';
    const DJIBOUTI = 'DJ';
    const DOMINICA = 'DM';
    const DOMINICAN_REPUBLIC = 'DO';
    const EGYPT = 'EG';
    const ZAMBIA = 'ZM';
    const WESTERN_SAHARA = 'EH';
    const ZIMBABWE = 'ZW';
    const ISRAEL = 'IL';
    const INDIA = 'IN';
    const INDONESIA = 'ID';
    const JORDAN = 'JO';
    const IRAQ = 'IQ';
    const IRAN = 'IR';
    const IRELAND = 'IE';
    const ICELAND = 'IS';
    const SPAIN = 'ES';
    const ITALY = 'IT';
    const YEMEN = 'YE';
    const CAPE_VERDE = 'CV';
    const KAZAKHSTAN = 'KZ';
    const CAYMAN_ISLANDS = 'KY';
    const CAMBODIA = 'KH';
    const CAMEROON = 'CM';
    const CANADA = 'CA';
    const QATAR = 'QA';
    const KENYA = 'KE';
    const CYPRUS = 'CY';
    const KYRGYZSTAN = 'KG';
    const KIRIBATI = 'KI';
    const CHINA = 'CN';
    const COLOMBIA = 'CO';
    const COMOROS = 'KM';
    const CONGO = 'CG';
    const CONGO_DEMOCRATIC_REPUBLIC = 'CD';
    const COSTA_RICA = 'CR';
    const COTE_D_IVOIRE = 'CI';
    const CUBA = 'CU';
    const KUWAIT = 'KW';
    const LAOS = 'LA';
    const LATVIA = 'LV';
    const LESOTHO = 'LS';
    const LIBERIA = 'LR';
    const LEBANON = 'LB';
    const LIBYA = 'LY';
    const LITHUANIA = 'LT';
    const LIECHTENSTEIN = 'LI';
    const LUXEMBOURG = 'LU';
    const MAURITIUS = 'MU';
    const MAURITANIA = 'MR';
    const MADAGASCAR = 'MG';
    const MACAU = 'MO';
    const MACEDONIA = 'MK';
    const MALAWI = 'MW';
    const MALAYSIA = 'MY';
    const MALI = 'ML';
    const MALDIVES = 'MV';
    const MALTA = 'MT';
    const MOROCCO = 'MA';
    const MARTINIQUE = 'MQ';
    const MARSHALL_ISLANDS = 'MH';
    const MEXICO = 'MX';
    const MICRONESIA_FEDERATED_STATES = 'FM';
    const MOZAMBIQUE = 'MZ';
    const MOLDOVA = 'MD';
    const MONACO = 'MC';
    const MONGOLIA = 'MN';
    const MONTSERRAT = 'MS';
    const MYANMAR = 'MM';
    const NAMIBIA = 'NA';
    const NAURU = 'NR';
    const NEPAL = 'NP';
    const NIGER = 'NE';
    const NIGERIA = 'NG';
    const NETHERLANDS_ANTILLES = 'AN';
    const NETHERLANDS = 'NL';
    const NICARAGUA = 'NI';
    const NIUE = 'NU';
    const NEW_ZEALAND = 'NZ';
    const NEW_CALEDONIA = 'NC';
    const NORWAY = 'NO';
    const UNITED_ARAB_EMIRATES = 'AE';
    const OMAN = 'OM';
    const ISLE_OF_MAN = 'IM';
    const NORFOLK = 'NF';
    const SAINT_HELENA = 'SH';
    const COOK_ISLANDS = 'CK';
    const TURKS_AND_CAICOS_ISLANDS = 'TC';
    const PAKISTAN = 'PK';
    const PALAU = 'PW';
    const PALESTINE = 'PS';
    const PANAMA = 'PA';
    const PAPUA_NEW_GUINEA = 'PG';
    const PARAGUAY = 'PY';
    const PERU = 'PE';
    const PITCAIRN = 'PN';
    const POLAND = 'PL';
    const PORTUGAL = 'PT';
    const PUERTO_RICO = 'PR';
    const SOUTH_KOREA = 'KR';
    const REUNION = 'RE';
    const RUSSIA = 'RU';
    const RWANDA = 'RW';
    const ROMANIA = 'RO';
    const SALVADOR = 'SV';
    const SAMOA = 'WS';
    const SAN_MARINO = 'SM';
    const SAO_TOME_AND_PRINCIPE = 'ST';
    const SAUDI_ARABIA = 'SA';
    const SWAZILAND = 'SZ';
    const NORTH_KOREA = 'KP';
    const NORTHERN_MARIANA_ISLANDS = 'MP';
    const SEYCHELLES = 'SC';
    const SENEGAL = 'SN';
    const SAINT_VINCENT_AND_THE_GRENADINES = 'VC';
    const SAINT_KITTS_AND_NEVIS = 'KN';
    const ST_LUCIA = 'LC';
    const SAINT_PIERRE_AND_MIQUELON = 'PM';
    const SERBIA = 'RS';
    const SINGAPORE = 'SG';
    const SYRIA = 'SY';
    const SLOVAKIA = 'SK';
    const SLOVENIA = 'SI';
    const SOLOMON_ISLANDS = 'SB';
    const SOMALIA = 'SO';
    const SUDAN = 'SD';
    const SURINAME = 'SR';
    const UNITED_STATES = 'US';
    const SIERRA_LEONE = 'SL';
    const TAJIKISTAN = 'TJ';
    const THAILAND = 'TH';
    const TAIWAN = 'TW';
    const TANZANIA = 'TZ';
    const TOGO = 'TG';
    const TOKELAU = 'TK';
    const TONGA = 'TO';
    const TRINIDAD_AND_TOBAGO = 'TT';
    const TUVALU = 'TV';
    const TUNISIA = 'TN';
    const TURKMENISTAN = 'TM';
    const TURKEY = 'TR';
    const UGANDA = 'UG';
    const UZBEKISTAN = 'UZ';
    const UKRAINE = 'UA';
    const WALLIS_AND_FUTUNA = 'WF';
    const URUGUAY = 'UY';
    const FAROE_ISLANDS = 'FO';
    const FIJI = 'FJ';
    const PHILIPPINES = 'PH';
    const FINLAND = 'FI';
    const FALKLAND_ISLANDS = 'FK';
    const FRANCE = 'FR';
    const FRENCH_GUIANA = 'GF';
    const FRENCH_POLYNESIA = 'PF';
    const CROATIA = 'HR';
    const CENTRAL_AFRICAN_REPUBLIC = 'CF';
    const CHAD = 'TD';
    const MONTENEGRO = 'ME';
    const CZECH_REPUBLIC = 'CZ';
    const CHILE = 'CL';
    const SWITZERLAND = 'CH';
    const SWEDEN = 'SE';
    const SVALBARD_AND_JAN_MAYEN = 'SJ';
    const SRI_LANKA = 'LK';
    const ECUADOR = 'EC';
    const EQUATORIAL_GUINEA = 'GQ';
    const ERITREA = 'ER';
    const ESTONIA = 'EE';
    const ETHIOPIA = 'ET';
    const SOUTH_AFRICA = 'ZA';
    const JAMAICA = 'JM';
    const JAPAN = 'JP';

    /**
     * @param boolean $bTitle
     * @param array $arrExclude
     * @param string $language
     * Overriden for alphabetical sorting
     */
    public function getOptions($bTitle = FALSE, $arrExclude = [], $language = 'en')
    {
        $arrOptions = [];
        $arrAlphaSort = [];
        foreach ($this->getAll() as $strName => $value)
        {
            if (in_array($value, $arrExclude))
            {
                continue;
            }

            $title = $this->getTitle($value, $language);
            if ($bTitle) {
                $arrAlphaSort[$value] = $title;
            } else {
                $arrOptions[] = ['key' => $value, 'value' => ($bTitle ? $title : $strName)];
            }
        }

        if (!$bTitle) {
            return $arrOptions;
        }

        asort($arrAlphaSort);
        foreach ($arrAlphaSort as $strCountryCode => $strTranslatedCountryName) {
            $arrOptions[] = ['key' => $strCountryCode, 'value' => $strTranslatedCountryName];
        }

        return $arrOptions;
    }

    public function getTitles()
    {
    	return $this->_arrTitles;
    }

    /**
     * НЕ УДАЛЯТЬ ИБО УБЬЮ!
     * или как минимум дам пизды
     * @var array
     *//***/
    protected $_arrTitles = [
        self::AUSTRALIA => [
            'en' => 'Australia',
            'ru' => 'Австралия',
            'bg' => 'Австралия',
        ],
        self::AUSTRIA => [
            'en' => 'Austria',
            'ru' => 'Австрия',
            'bg' => 'Австрия',
        ],
        self::AZERBAIJAN => [
            'en' => 'Azerbaijan',
            'ru' => 'Азербайджан',
            'bg' => ' Азербайджан',
        ],
        self::ALBANIA => [
            'en' => 'Albania',
            'ru' => 'Албания',
            'bg' => ' Албания',
        ],
        self::ALGERIA => [
            'en' => 'Algeria',
            'ru' => 'Алжир',
            'bg' => 'Алжир',
        ],
        self::AMERICAN_SAMOA => [
            'en' => 'American Samoa',
            'ru' => 'Американское Самоа',
            'bg' => 'Американска Самоа',
        ],
        self::ANGUILLA => [
            'en' => 'Anguilla',
            'ru' => 'Ангилья',
            'bg' => 'Ангуила',
        ],
        self::ANGOLA => [
            'en' => 'Angola',
            'ru' => 'Ангола',
            'bg' => 'Ангола',
        ],
        self::ANDORRA => [
            'en' => 'Andorra',
            'ru' => 'Андорра',
            'bg' => 'Андора',
        ],
        self::ANTIGUA_AND_BARBUDA => [
            'en' => 'Antigua and Barbuda',
            'ru' => 'Антигуа и Барбуда',
            'bg' => 'Антигуа и Барбуда',
        ],
        self::ARGENTINA => [
            'en' => 'Argentina',
            'ru' => 'Аргентина',
            'bg' => 'Аржентина',
        ],
        self::ARMENIA => [
            'en' => 'Armenia',
            'ru' => 'Армения',
            'bg' => 'Армения',
        ],
        self::ARUBA => [
            'en' => 'Aruba',
            'ru' => 'Аруба',
            'bg' => 'Аруба',
        ],
        self::AFGHANISTAN => [
            'en' => 'Afghanistan',
            'ru' => 'Афганистан',
            'bg' => 'Афганистан',
        ],
        self::BAHAMAS => [
            'en' => 'Bahamas',
            'ru' => 'Багамы',
            'bg' => 'Бахамски острови',
        ],
        self::BANGLADESH => [
            'en' => 'Bangladesh',
            'ru' => 'Бангладеш',
            'bg' => 'Бангладеш',
        ],
        self::BARBADOS => [
            'en' => 'Barbados',
            'ru' => 'Барбадос',
            'bg' => 'Барбадос',
        ],
        self::BAHRAIN => [
            'en' => 'Bahrain',
            'ru' => 'Бахрейн',
            'bg' => 'Бахрейн',
        ],
        self::BELARUS => [
            'en' => 'Belarus',
            'ru' => 'Беларусь',
            'bg' => 'Беларус',
        ],
        self::BELIZE => [
            'en' => 'Belize',
            'ru' => 'Белиз',
            'bg' => 'Белиз',
        ],
        self::BELGIUM => [
            'en' => 'Belgium',
            'ru' => 'Бельгия',
            'bg' => 'Белгия',
        ],
        self::BENIN => [
            'en' => 'Benin',
            'ru' => 'Бенин',
            'bg' => 'Бенин',
        ],
        self::BERMUDA => [
            'en' => 'Bermuda',
            'ru' => 'Бермуды',
            'bg' => 'Бермуда',
        ],
        self::BULGARIA => [
            'en' => 'Bulgaria',
            'ru' => 'Болгария',
            'bg' => 'България',
        ],
        self::BOLIVIA => [
            'en' => 'Bolivia',
            'ru' => 'Боливия',
            'bg' => 'Боливия',
        ],
        self::BOSNIA_AND_HERZEGOVINA => [
            'en' => 'Bosnia and Herzegovina',
            'ru' => 'Босния и Герцеговина',
            'bg' => 'Босна и Херцеговина',
        ],
        self::BOTSWANA => [
            'en' => 'Botswana',
            'ru' => 'Ботсвана',
            'bg' => 'Ботсвана',
        ],
        self::BRAZIL => [
            'en' => 'Brazil',
            'ru' => 'Бразилия',
            'bg' => 'Бразилия',
        ],
        self::BRUNEI_DARUSSALAM => [
            'en' => 'Brunei Darussalam',
            'ru' => 'Бруней-Даруссалам',
            'bg' => 'Бруней',
        ],
        self::BURKINA_FASO => [
            'en' => 'Burkina Faso',
            'ru' => 'Буркина-Фасо',
            'bg' => 'Буркина Фасо',
        ],
        self::BURUNDI => [
            'en' => 'Burundi',
            'ru' => 'Бурунди',
            'bg' => 'Бурунди',
        ],
        self::BHUTAN => [
            'en' => 'Bhutan',
            'ru' => 'Бутан',
            'bg' => 'Бутан',
        ],
        self::VANUATU => [
            'en' => 'Vanuatu',
            'ru' => 'Вануату',
            'bg' => 'Вануату',
        ],
        self::UK => [
            'en' => 'UK',
            'ru' => 'Великобритания',
            'bg' => 'Великобритания',
        ],
        self::HUNGARY => [
            'en' => 'Hungary',
            'ru' => 'Венгрия',
            'bg' => 'Унгария',
        ],
        self::VENEZUELA => [
            'en' => 'Venezuela',
            'ru' => 'Венесуэла',
            'bg' => 'Венецуела',
        ],
        self::VIRGIN_ISLANDS_BRITISH => [
            'en' => 'Virgin Islands, British',
            'ru' => 'Виргинские острова, Британские',
            'bg' => 'Британски Вирджински острови',
        ],
        self::US_VIRGIN_ISLANDS => [
            'en' => 'U.S. Virgin Islands',
            'ru' => 'Виргинские острова, США',
            'bg' => 'Американски Вирджински острови',
        ],
        self::EAST_TIMOR => [
            'en' => 'East Timor',
            'ru' => 'Восточный Тимор',
            'bg' => 'Източен Тимор',
        ],
        self::VIETNAM => [
            'en' => 'Vietnam',
            'ru' => 'Вьетнам',
            'bg' => 'Виетнам',
        ],
        self::GABON => [
            'en' => 'Gabon',
            'ru' => 'Габон',
            'bg' => 'Габон',
        ],
        self::HAITI => [
            'en' => 'Haiti',
            'ru' => 'Гаити',
            'bg' => 'Хаити',
        ],
        self::GUYANA => [
            'en' => 'Guyana',
            'ru' => 'Гайана',
            'bg' => 'Гаяна',
        ],
        self::GAMBIA => [
            'en' => 'Gambia',
            'ru' => 'Гамбия',
            'bg' => 'Гамбия',
        ],
        self::GHANA => [
            'en' => 'Ghana',
            'ru' => 'Гана',
            'bg' => 'Гана',
        ],
        self::GUADELOUPE => [
            'en' => 'Guadeloupe',
            'ru' => 'Гваделупа',
            'bg' => 'Гваделупа',
        ],
        self::GUATEMALA => [
            'en' => 'Guatemala',
            'ru' => 'Гватемала',
            'bg' => 'Гватемала',
        ],
        self::GUINEA => [
            'en' => 'Guinea',
            'ru' => 'Гвинея',
            'bg' => 'Гвинея',
        ],
        self::GUINEA_BISSAU => [
            'en' => 'Guinea-Bissau',
            'ru' => 'Гвинея-Бисау',
            'bg' => 'Гвинея-Бисау',
        ],
        self::GERMANY => [
            'en' => 'Germany',
            'ru' => 'Германия',
            'bg' => 'Германия',
        ],
        self::GIBRALTAR => [
            'en' => 'Gibraltar',
            'ru' => 'Гибралтар',
            'bg' => 'Гибралтар',
        ],
        self::HONDURAS => [
            'en' => 'Honduras',
            'ru' => 'Гондурас',
            'bg' => 'Хондурас',
        ],
        self::HONG_KONG => [
            'en' => 'Hong Kong',
            'ru' => 'Гонконг',
            'bg' => 'Хонконг',
        ],
        self::GRENADA => [
            'en' => 'Grenada',
            'ru' => 'Гренада',
            'bg' => 'Гренада',
        ],
        self::GREENLAND => [
            'en' => 'Greenland',
            'ru' => 'Гренландия',
            'bg' => 'Гренландия',
        ],
        self::GREECE => [
            'en' => 'Greece',
            'ru' => 'Греция',
            'bg' => 'Гърция',
        ],
        self::GEORGIA => [
            'en' => 'Georgia',
            'ru' => 'Грузия',
            'bg' => 'Грузия',
        ],
        self::GUAM => [
            'en' => 'Guam',
            'ru' => 'Гуам',
            'bg' => 'Гуам',
        ],
        self::DENMARK => [

            'en' => 'Denmark',
            'ru' => 'Дания',
            'bg' => 'Дания',
        ],
        self::DJIBOUTI => [
            'en' => 'Djibouti',
            'ru' => 'Джибути',
            'bg' => 'Джибути',
        ],
        self::DOMINICA => [
            'en' => 'Dominica',
            'ru' => 'Доминика',
            'bg' => 'Доминика',
        ],
        self::DOMINICAN_REPUBLIC => [
            'en' => 'Dominican Republic',
            'ru' => 'Доминиканская Республика',
            'bg' => 'Доминиканска Република',
        ],
        self::EGYPT => [
            'en' => 'Egypt',
            'ru' => 'Египет',
            'bg' => 'Египет',
        ],
        self::ZAMBIA => [
            'en' => 'Zambia',
            'ru' => 'Замбия',
            'bg' => 'Замбия',
        ],
        self::WESTERN_SAHARA => [
            'en' => 'Western Sahara',
            'ru' => 'Западная Сахара',
            'bg' => 'Западна Сахара',
        ],
        self::ZIMBABWE => [
            'en' => 'Zimbabwe',
            'ru' => 'Зимбабве',
            'bg' => 'Зимбабве',
        ],
        self::ISRAEL => [
            'en' => 'Israel',
            'ru' => 'Израиль',
            'bg' => 'Израел',
        ],
        self::INDIA => [
            'en' => 'India',
            'ru' => 'Индия',
            'bg' => 'Индия',
        ],
        self::INDONESIA => [
            'en' => 'Indonesia',
            'ru' => 'Индонезия',
            'bg' => 'Индонезия',
        ],
        self::JORDAN => [
            'en' => 'Jordan',
            'ru' => 'Иордания',
            'bg' => 'Йордания',
        ],
        self::IRAQ => [
            'en' => 'Iraq',
            'ru' => 'Ирак',
            'bg' => 'Ирак',
        ],
        self::IRAN => [
            'en' => 'Iran',
            'ru' => 'Иран',
            'bg' => 'Иран',
        ],
        self::IRELAND => [
            'en' => 'Ireland',
            'ru' => 'Ирландия',
            'bg' => 'Ирландия',
        ],
        self::ICELAND => [
            'en' => 'Iceland',
            'ru' => 'Исландия',
            'bg' => 'Исландия',
        ],
        self::SPAIN => [
            'en' => 'Spain',
            'ru' => 'Испания',
            'bg' => 'Испания',
        ],
        self::ITALY => [
            'en' => 'Italy',
            'ru' => 'Италия',
            'bg' => 'Италия',
        ],
        self::YEMEN => [
            'en' => 'Yemen',
            'ru' => 'Йемен',
            'bg' => 'Йемен',
        ],
        self::CAPE_VERDE => [
            'en' => 'Cape Verde',
            'ru' => 'Кабо Верде',
            'bg' => 'Кабо Верде',
        ],
        self::KAZAKHSTAN => [
            'en' => 'Kazakhstan',
            'ru' => 'Казахстан',
            'bg' => 'Казахстан',
        ],
        self::CAYMAN_ISLANDS => [
            'en' => 'Cayman Islands',
            'ru' => 'Каймановы Острова',
            'bg' => 'Кайманови Острови',
        ],
        self::CAMBODIA => [
            'en' => 'Cambodia',
            'ru' => 'Камбоджа',
            'bg' => 'Камбоджа',
        ],
        self::CAMEROON => [
            'en' => 'Cameroon',
            'ru' => 'Камерун',
            'bg' => 'Камерун',
        ],
        self::CANADA => [
            'en' => 'Canada',
            'ru' => 'Канада',
            'bg' => 'Канада',
        ],
        self::QATAR => [
            'en' => 'Qatar',
            'ru' => 'Катар',
            'bg' => 'Катар',
        ],
        self::KENYA => [
            'en' => 'Kenya',
            'ru' => 'Кения',
            'bg' => 'Кения',
        ],
        self::CYPRUS => [
            'en' => 'Cyprus',
            'ru' => 'Кипр',
            'bg' => 'Кипър',
        ],
        self::KYRGYZSTAN => [
            'en' => 'Kyrgyzstan',
            'ru' => 'Киргизия',
            'bg' => 'Киргизстан',
        ],
        self::KIRIBATI => [
            'en' => 'Kiribati',
            'ru' => 'Кирибати',
            'bg' => 'Кирибати',
        ],
        self::CHINA => [
            'en' => 'China',
            'ru' => 'Китай',
            'bg' => 'Китай',
        ],
        self::COLOMBIA => [
            'en' => 'Colombia',
            'ru' => 'Колумбия',
            'bg' => 'Колумбия',
        ],
        self::COMOROS => [
            'en' => 'Comoros',
            'ru' => 'Коморские острова',
            'bg' => 'Коморски острови',
        ],
        self::CONGO => [
            'en' => 'Congo',
            'ru' => 'Конго',
            'bg' => 'Конго',
        ],
        self::CONGO_DEMOCRATIC_REPUBLIC => [
            'en' => 'Congo, Democratic Republic',
            'ru' => 'Конго, демократическая республика',
            'bg' => 'Конго, Демократична република',
        ],
        self::COSTA_RICA => [
            'en' => 'Costa Rica',
            'ru' => 'Коста Рика',
            'bg' => 'Коста Рика',
        ],
        self::COTE_D_IVOIRE => [
            'en' => 'Côte d\'Ivoire',
            'ru' => 'Кот д`Ивуар',
            'bg' => 'Кот д`Ивоар',
        ],
        self::CUBA => [
            'en' => 'Cuba',
            'ru' => 'Куба',
            'bg' => 'Куба',
        ],
        self::KUWAIT => [
            'en' => 'Kuwait',
            'ru' => 'Кувейт',
            'bg' => 'Кувейт',
        ],
        self::LAOS => [
            'en' => 'Laos',
            'ru' => 'Лаос',
            'bg' => 'Лаос',
        ],
        self::LATVIA => [
            'en' => 'Latvia',
            'ru' => 'Латвия',
            'bg' => 'Латвия',
        ],
        self::LESOTHO => [
            'en' => 'Lesotho',
            'ru' => 'Лесото',
            'bg' => 'Лесото',
        ],
        self::LIBERIA => [
            'en' => 'Liberia',
            'ru' => 'Либерия',
            'bg' => 'Либерия',
        ],
        self::LEBANON => [
            'en' => 'Lebanon',
            'ru' => 'Ливан',
            'bg' => 'Ливан',
        ],
        self::LIBYA => [
            'en' => 'Libya',
            'ru' => 'Ливия',
            'bg' => 'Либия',
        ],
        self::LITHUANIA => [
            'en' => 'Lithuania',
            'ru' => 'Литва',
            'bg' => 'Литва',
        ],
        self::LIECHTENSTEIN => [
            'en' => 'Liechtenstein',
            'ru' => 'Лихтенштейн',
            'bg' => 'Лихтенщайн',
        ],
        self::LUXEMBOURG => [
            'en' => 'Luxembourg',
            'ru' => 'Люксембург',
            'bg' => 'Люксембург',
        ],
        self::MAURITIUS => [
            'en' => 'Mauritius',
            'ru' => 'Маврикий',
            'bg' => 'Мавриций',
        ],
        self::MAURITANIA => [
            'en' => 'Mauritania',
            'ru' => 'Мавритания',
            'bg' => 'Мавритания',
        ],
        self::MADAGASCAR => [
            'en' => 'Madagascar',
            'ru' => 'Мадагаскар',
            'bg' => 'Мадагаскар',
        ],
        self::MACAU => [
            'en' => 'Macau',
            'ru' => 'Макао',
            'bg' => 'Макао',
        ],
        self::MACEDONIA => [
            'en' => 'Macedonia',
            'ru' => 'Македония',
            'bg' => 'Македония',
        ],
        self::MALAWI => [
            'en' => 'Malawi',
            'ru' => 'Малави',
            'bg' => 'Малави',
        ],
        self::MALAYSIA => [
            'en' => 'Malaysia',
            'ru' => 'Малайзия',
            'bg' => 'Малайзия',
        ],
        self::MALI => [
            'en' => 'Mali',
            'ru' => 'Мали',
            'bg' => 'Мали',
        ],
        self::MALDIVES => [
            'en' => 'Maldives',
            'ru' => 'Мальдивы',
            'bg' => 'Малдивски острови',
        ],
        self::MALTA => [
            'en' => 'Malta',
            'ru' => 'Мальта',
            'bg' => 'Малта',
        ],
        self::MOROCCO => [
            'en' => 'Morocco',
            'ru' => 'Марокко',
            'bg' => 'Мароко',
        ],
        self::MARTINIQUE => [
            'en' => 'Martinique',
            'ru' => 'Мартиника',
            'bg' => 'Мартиника',
        ],
        self::MARSHALL_ISLANDS => [
            'en' => 'Marshall Islands',
            'ru' => 'Маршалловы Острова',
            'bg' => 'Маршаловите острови',
        ],
        self::MEXICO => [
            'en' => 'Mexico',
            'ru' => 'Мексика',
            'bg' => 'Мексико',
        ],
        self::MICRONESIA_FEDERATED_STATES => [
            'en' => 'Micronesia, Federated States',
            'ru' => 'Микронезия, федеративные штаты',
            'bg' => 'Микронезия, Федерални щати',
        ],
        self::MOZAMBIQUE => [
            'en' => 'Mozambique',
            'ru' => 'Мозамбик',
            'bg' => 'Мозамбик',
        ],
        self::MOLDOVA => [
            'en' => 'Moldova',
            'ru' => 'Молдова',
            'bg' => 'Молдова',
        ],
        self::MONACO => [
            'en' => 'Monaco',
            'ru' => 'Монако',
            'bg' => 'Монако',
        ],
        self::MONGOLIA => [
            'en' => 'Mongolia',
            'ru' => 'Монголия',
            'bg' => 'Монголия',
        ],
        self::MONTSERRAT => [
            'en' => 'Montserrat',
            'ru' => 'Монтсеррат',
            'bg' => 'Монсерат',
        ],
        self::MYANMAR => [
            'en' => 'Myanmar',
            'ru' => 'Мьянма',
            'bg' => 'Мианмар',
        ],
        self::NAMIBIA => [
            'en' => 'Namibia',
            'ru' => 'Намибия',
            'bg' => 'Намибия',
        ],
        self::NAURU => [
            'en' => 'Nauru',
            'ru' => 'Науру',
            'bg' => 'Науру',
        ],
        self::NEPAL => [
            'en' => 'Nepal',
            'ru' => 'Непал',
            'bg' => 'Непал',
        ],
        self::NIGER => [
            'en' => 'Niger',
            'ru' => 'Нигер',
            'bg' => 'Нигер',
        ],
        self::NIGERIA => [
            'en' => 'Nigeria',
            'ru' => 'Нигерия',
            'bg' => 'Нигерия',
        ],
        self::NETHERLANDS_ANTILLES => [
            'en' => 'Netherlands Antilles',
            'ru' => 'Нидерландские Антилы',
            'bg' => 'Холандски Антили',
        ],
        self::NETHERLANDS => [
            'en' => 'Netherlands',
            'ru' => 'Нидерланды',
            'bg' => 'Холандия',
        ],
        self::NICARAGUA => [
            'en' => 'Nicaragua',
            'ru' => 'Никарагуа',
            'bg' => 'Никарагуа',
        ],
        self::NIUE => [
            'en' => 'Niue',
            'ru' => 'Ниуэ',
            'bg' => 'Ниуе',
        ],
        self::NEW_ZEALAND => [
            'en' => 'New Zealand',
            'ru' => 'Новая Зеландия',
            'bg' => 'Нова Зеландия',
        ],
        self::NEW_CALEDONIA => [
            'en' => 'New Caledonia',
            'ru' => 'Новая Каледония',
            'bg' => 'Нова Каледония',
        ],
        self::NORWAY => [
            'en' => 'Norway',
            'ru' => 'Норвегия',
            'bg' => 'Норвегия',
        ],
        self::UNITED_ARAB_EMIRATES => [
            'en' => 'United Arab Emirates',
            'ru' => 'Объединенные Арабские Эмираты',
            'bg' => 'Обединени арабски емирства',
        ],
        self::OMAN => [
            'en' => 'Oman',
            'ru' => 'Оман',
            'bg' => 'Оман',
        ],
        self::ISLE_OF_MAN => [
            'en' => 'Isle of Man',
            'ru' => 'Остров Мэн',
            'bg' => 'Остров Ман',
        ],
        self::NORFOLK => [
            'en' => 'Norfolk',
            'ru' => 'Остров Норфолк',
            'bg' => 'Остров Норфолк',
        ],
        self::SAINT_HELENA => [
            'en' => 'Saint Helena',
            'ru' => 'Остров Святой Елены',
            'bg' => 'Света Елена',
        ],
        self::COOK_ISLANDS => [
            'en' => 'Cook Islands',
            'ru' => 'Острова Кука',
            'bg' => 'Острови Кук',
        ],
        self::TURKS_AND_CAICOS_ISLANDS => [
            'en' => 'Turks and Caicos Islands',
            'ru' => 'Острова Теркс и Кайкос',
            'bg' => 'Острови Търкс и Кайкос',
        ],
        self::PAKISTAN => [
            'en' => 'Pakistan',
            'ru' => 'Пакистан',
            'bg' => 'Пакистан',
        ],
        self::PALAU => [
            'en' => 'Palau',
            'ru' => 'Палау',
            'bg' => 'Палау',
        ],
        self::PALESTINE => [
            'en' => 'Palestine',
            'ru' => 'Палестина',
            'bg' => 'Палестина',
        ],
        self::PANAMA => [
            'en' => 'Panama',
            'ru' => 'Панама',
            'bg' => 'Панама',
        ],
        self::PAPUA_NEW_GUINEA => [
            'en' => 'Papua - New Guinea',
            'ru' => 'Папуа - Новая Гвинея',
            'bg' => 'Папуа Нова Гвинея',
        ],
        self::PARAGUAY => [
            'en' => 'Paraguay',
            'ru' => 'Парагвай',
            'bg' => 'Парагвай',
        ],
        self::PERU => [
            'en' => 'Peru',
            'ru' => 'Перу',
            'bg' => 'Перу',
        ],
        self::PITCAIRN => [
            'en' => 'Pitcairn',
            'ru' => 'Острова Питкерн',
            'bg' => 'Острови Питкерн',
        ],
        self::POLAND => [
            'en' => 'Poland',
            'ru' => 'Польша',
            'bg' => 'Полша',
        ],
        self::PORTUGAL => [
            'en' => 'Portugal',
            'ru' => 'Португалия',
            'bg' => 'Португалия',
        ],
        self::PUERTO_RICO => [
            'en' => 'Puerto Rico',
            'ru' => 'Пуэрто-Рико',
            'bg' => 'Пуерто Рико',
        ],
        self::SOUTH_KOREA => [
            'en' => 'South Korea',
            'ru' => 'Республика Корея',
            'bg' => 'Республика Корея',
        ],
        self::REUNION => [
            'en' => 'Reunion',
            'ru' => 'Реюньон',
            'bg' => 'Реюнион',
        ],
        self::RUSSIA => [
            'en' => 'Russia',
            'ru' => 'Россия',
            'bg' => 'Русия',
        ],
        self::RWANDA => [
            'en' => 'Rwanda',
            'ru' => 'Руанда',
            'bg' => 'Руанда',
        ],
        self::ROMANIA => [
            'en' => 'Romania',
            'ru' => 'Румыния',
            'bg' => 'Румъния',
        ],
        self::SALVADOR => [
            'en' => 'Salvador',
            'ru' => 'Сальвадор',
            'bg' => 'Салвадор',
        ],
        self::SAMOA => [
            'en' => 'Samoa',
            'ru' => 'Самоа',
            'bg' => 'Самоа',
        ],
        self::SAN_MARINO => [
            'en' => 'San Marino',
            'ru' => 'Сан-Марино',
            'bg' => 'Сан Марино',
        ],
        self::SAO_TOME_AND_PRINCIPE => [
            'en' => 'Sao Tome and Principe',
            'ru' => 'Сан-Томе и Принсипи',
            'bg' => 'Сан Томе и Принсипе',
        ],
        self::SAUDI_ARABIA => [
            'en' => 'Saudi Arabia',
            'ru' => 'Саудовская Аравия',
            'bg' => 'Саудитска Арабия',
        ],
        self::SWAZILAND => [
            'en' => 'Swaziland',
            'ru' => 'Свазиленд',
            'bg' => 'Свазиленд',
        ],
        self::NORTH_KOREA => [
            'en' => 'North Korea',
            'ru' => 'Северная Корея',
            'bg' => 'Северна Корея',
        ],
        self::NORTHERN_MARIANA_ISLANDS => [
            'en' => 'Northern Mariana Islands',
            'ru' => 'Северные Марианские острова',
            'bg' => 'Северни Мариански острови',
        ],
        self::SEYCHELLES => [
            'en' => 'Seychelles',
            'ru' => 'Сейшельские острова',
            'bg' => 'Сейшелски острови',
        ],
        self::SENEGAL => [
            'en' => 'Senegal',
            'ru' => 'Сенегал',
            'bg' => 'Сенегал',
        ],
        self::SAINT_VINCENT_AND_THE_GRENADINES => [
            'en' => 'Saint Vincent and the Grenadines',
            'ru' => 'Сент-Винсент и Гренадины',
            'bg' => 'Свети Винсент и Гренадини',
        ],
        self::SAINT_KITTS_AND_NEVIS => [
            'en' => 'Saint Kitts and Nevis',
            'ru' => 'Сент-Китс и Невис',
            'bg' => 'Сейнт Китс и Невис',
        ],
        self::ST_LUCIA => [
            'en' => 'St. Lucia',
            'ru' => 'Сент-Люсия',
            'bg' => 'Сейнт Лусия',
        ],
        self::SAINT_PIERRE_AND_MIQUELON => [
            'en' => 'Saint Pierre and Miquelon',
            'ru' => 'Сент-Пьер и Микелон',
            'bg' => 'Сен Пиер и Микелон',
        ],
        self::SERBIA => [
            'en' => 'Serbia',
            'ru' => 'Сербия',
            'bg' => 'Сърбия',
        ],
        self::SINGAPORE => [
            'en' => 'Singapore',
            'ru' => 'Сингапур',
            'bg' => 'Сингапур',
        ],
        self::SYRIA => [
            'en' => 'Syria',
            'ru' => 'Сирия',
            'bg' => 'Сирия',
        ],
        self::SLOVAKIA => [
            'en' => 'Slovakia',
            'ru' => 'Словакия',
            'bg' => 'Словакия',
        ],
        self::SLOVENIA => [
            'en' => 'Slovenia',
            'ru' => 'Словения',
            'bg' => 'Словения',
        ],
        self::SOLOMON_ISLANDS => [
            'en' => 'Solomon Islands',
            'ru' => 'Соломоновы Острова',
            'bg' => 'Соломонови острови',
        ],
        self::SOMALIA => [
            'en' => 'Somalia',
            'ru' => 'Сомали',
            'bg' => 'Сомалия',
        ],
        self::SUDAN => [
            'en' => 'Sudan',
            'ru' => 'Судан',
            'bg' => 'Судан',
        ],
        self::SURINAME => [
            'en' => 'Suriname',
            'ru' => 'Суринам',
            'bg' => 'Суринам',
        ],
        self::UNITED_STATES => [
            'en' => 'United States',
            'ru' => 'США',
            'bg' => 'САЩ',
        ],
        self::SIERRA_LEONE => [
            'en' => 'Sierra Leone',
            'ru' => 'Сьерра-Леоне',
            'bg' => 'Сиера Леоне',
        ],
        self::TAJIKISTAN => [
            'en' => 'Tajikistan',
            'ru' => 'Таджикистан',
            'bg' => 'Таджикистан',
        ],
        self::THAILAND => [
            'en' => 'Thailand',
            'ru' => 'Таиланд',
            'bg' => 'Таиланд',
        ],
        self::TAIWAN => [
            'en' => 'Taiwan',
            'ru' => 'Тайвань',
            'bg' => 'Тайван',
        ],
        self::TANZANIA => [
            'en' => 'Tanzania',
            'ru' => 'Танзания',
            'bg' => 'Танзания',
        ],
        self::TOGO => [
            'en' => 'Togo',
            'ru' => 'Того',
            'bg' => 'Того',
        ],
        self::TOKELAU => [
            'en' => 'Tokelau',
            'ru' => 'Токелау',
            'bg' => 'Токелау',
        ],
        self::TONGA => [
            'en' => 'Tonga',
            'ru' => 'Тонга',
            'bg' => 'Тонга',
        ],
        self::TRINIDAD_AND_TOBAGO => [
            'en' => 'Trinidad and Tobago',
            'ru' => 'Тринидад и Тобаго',
            'bg' => 'Тринидад и Тобаго',
        ],
        self::TUVALU => [
            'en' => 'Tuvalu',
            'ru' => 'Тувалу',
            'bg' => 'Тувалу',
        ],
        self::TUNISIA => [
            'en' => 'Tunisia',
            'ru' => 'Тунис',
            'bg' => 'Тунис',
        ],
        self::TURKMENISTAN => [
            'en' => 'Turkmenistan',
            'ru' => 'Туркменистан',
            'bg' => 'Туркменистан',
        ],
        self::TURKEY => [
            'en' => 'Turkey',
            'ru' => 'Турция',
            'bg' => 'Турция',
        ],
        self::UGANDA => [
            'en' => 'Uganda',
            'ru' => 'Уганда',
            'bg' => 'Уганда',
        ],
        self::UZBEKISTAN => [
            'en' => 'Uzbekistan',
            'ru' => 'Узбекистан',
            'bg' => 'Узбекистан',
        ],
        self::UKRAINE => [
            'en' => 'Ukraine',
            'ru' => 'Украина',
            'bg' => 'Украйна',
        ],
        self::WALLIS_AND_FUTUNA => [
            'en' => 'Wallis and Futuna',
            'ru' => 'Уоллис и Футуна',
            'bg' => 'Уолис и Футуна',
        ],
        self::URUGUAY => [
            'en' => 'Uruguay',
            'ru' => 'Уругвай',
            'bg' => 'Уругвай',
        ],
        self::FAROE_ISLANDS => [
            'en' => 'Faroe Islands',
            'ru' => 'Фарерские острова',
            'bg' => 'Фарьорските острови',
        ],
        self::FIJI => [
            'en' => 'Fiji',
            'ru' => 'Фиджи',
            'bg' => 'Фиджи',
        ],
        self::PHILIPPINES => [
            'en' => 'Philippines',
            'ru' => 'Филиппины',
            'bg' => 'Филипины',
        ],
        self::FINLAND => [
            'en' => 'Finland',
            'ru' => 'Финляндия',
            'bg' => 'Финландия',
        ],
        self::FALKLAND_ISLANDS => [
            'en' => 'Falkland Islands',
            'ru' => 'Фолклендские острова',
            'bg' => 'Фолкландските острови',
        ],
        self::FRANCE => [
            'en' => 'France',
            'ru' => 'Франция',
            'bg' => 'Франция',
        ],
        self::FRENCH_GUIANA => [
            'en' => 'French Guiana',
            'ru' => 'Французская Гвиана',
            'bg' => 'Френска Гвиана',
        ],
        self::FRENCH_POLYNESIA => [
            'en' => 'French Polynesia',
            'ru' => 'Французская Полинезия',
            'bg' => 'Френска Полинезия',
        ],
        self::CROATIA => [
            'en' => 'Croatia',
            'ru' => 'Хорватия',
            'bg' => 'Хърватска',
        ],
        self::CENTRAL_AFRICAN_REPUBLIC => [
            'en' => 'Central African Republic',
            'ru' => 'Центрально-Африканская Республика',
            'bg' => 'Централна Африканска Република',
        ],
        self::CHAD => [
            'en' => 'Chad',
            'ru' => 'Чад',
            'bg' => 'Чад',
        ],
        self::MONTENEGRO => [
            'en' => 'Montenegro',
            'ru' => 'Черногория',
            'bg' => 'Черна гора',
        ],
        self::CZECH_REPUBLIC => [
            'en' => 'Czech Republic',
            'ru' => 'Чехия',
            'bg' => 'Чехия',
        ],
        self::CHILE => [
            'en' => 'Chile',
            'ru' => 'Чили',
            'bg' => 'Чили',
        ],
        self::SWITZERLAND => [
            'en' => 'Switzerland',
            'ru' => 'Швейцария',
            'bg' => 'Швейцария',
        ],
        self::SWEDEN => [
            'en' => 'Sweden',
            'ru' => 'Швеция',
            'bg' => 'Швеция',
        ],
        self::SVALBARD_AND_JAN_MAYEN => [
            'en' => 'Svalbard and Jan Mayen',
            'ru' => 'Шпицберген и Ян Майен',
            'bg' => 'Свалбард и Ян Майен',
        ],
        self::SRI_LANKA => [
            'en' => 'Sri Lanka',
            'ru' => 'Шри-Ланка',
            'bg' => 'Шри Ланка',
        ],
        self::ECUADOR => [
            'en' => 'Ecuador',
            'ru' => 'Эквадор',
            'bg' => 'Еквадор',
        ],
        self::EQUATORIAL_GUINEA => [
            'en' => 'Equatorial Guinea',
            'ru' => 'Экваториальная Гвинея',
            'bg' => 'Екваториална Гвинея',
        ],
        self::ERITREA => [
            'en' => 'Eritrea',
            'ru' => 'Эритрея',
            'bg' => 'Еритрея',
        ],
        self::ESTONIA => [
            'en' => 'Estonia',
            'ru' => 'Эстония',
            'bg' => 'Естония',
        ],
        self::ETHIOPIA => [
            'en' => 'Ethiopia',
            'ru' => 'Эфиопия',
            'bg' => 'Етиопия',
        ],
        self::SOUTH_AFRICA => [
            'en' => 'South Africa',
            'ru' => 'Южно-Африканская Республика',
            'bg' => 'ЮАР',
        ],
        self::JAMAICA => [
            'en' => 'Jamaica',
            'ru' => 'Ямайка',
            'bg' => 'Ямайка',
        ],
        self::JAPAN => [
            'en' => 'Japan',
            'ru' => 'Япония',
            'bg' => 'Япония',
        ],
    ];/**/

    /**
     * @param string $mixValue
     * @param string $language
     * @return string
     */
/*    public function getTitle($mixValue, $language = 'en')
    {
        $this->validate($mixValue);
        return $this->_arrTitles[$mixValue][$language];
    }
*/
/*
    Австралия|AU
    Австрия|AT
    Азербайджан|AZ
    Албания|AL
    Алжир|DZ
    Американское Самоа|AS
    Ангилья|AI
    Ангола|AO
    Андорра|AD
    Антигуа и Барбуда|AG
    Аргентина|AR
    Армения|AM
    Аруба|AW
    Афганистан|AF
    Багамы|BS
    Бангладеш|BD
    Барбадос|BB
    Бахрейн|BH
    Беларусь|BY
    Белиз|BZ
    Бельгия|BE
    Бенин|BJ
    Бермуды|BM
    Болгария|BG
    Боливия|BO
    Босния и Герцеговина|BA
    Ботсвана|BW
    Бразилия|BR
    Бруней-Даруссалам|BN
    Буркина-Фасо|BF
    Бурунди|BI
    Бутан|BT
    Вануату|VU
    Великобритания|GB
    Венгрия|HU
    Венесуэла|VE
    Виргинские острова, Британские|VG
    Виргинские острова, США|VI
    Восточный Тимор|TL
    Вьетнам|VN
    Габон|GA
    Гаити|HT
    Гайана|GY
    Гамбия|GM
    Гана|GH
    Гваделупа|GP
    Гватемала|GT
    Гвинея|GN
    Гвинея-Бисау|GW
    Германия|DE
    Гибралтар|GI
    Гондурас|HN
    Гонконг|HK
    Гренада|GD
    Гренландия|GL
    Греция|GR
    Грузия|GE
    Гуам|GU
    Дания|DK
    Джибути|DJ
    Доминика|DM
    Доминиканская Республика|DO
    Египет|EG
    Замбия|ZM
    Западная Сахара|EH
    Зимбабве|ZW
    Израиль|IL
    Индия|IN
    Индонезия|ID
    Иордания|JO
    Ирак|IQ
    Иран|IR
    Ирландия|IE
    Исландия|IS
    Испания|ES
    Италия|IT
    Йемен|YE
    Кабо-Верде|CV
    Казахстан|KZ
    Каймановы Острова|KY
    Камбоджа|KH
    Камерун|CM
    Канада|CA
    Катар|QA
    Кения|KE
    Кипр|CY
    Киргизия|KG
    Кирибати|KI
    Китай|CN
    Колумбия|CO
    Коморские острова|KM
    Конго|CG
    Конго, демократическая республика|CD
    Коста-Рика|CR
    Кот д`Ивуар|CI
    Куба|CU
    Кувейт|KW
    Лаос|LA
    Латвия|LV
    Лесото|LS
    Либерия|LR
    Ливан|LB
    Ливия|LY
    Литва|LT
    Лихтенштейн|LI
    Люксембург|LU
    Маврикий|MU
    Мавритания|MR
    Мадагаскар|MG
    Макао|MO
    Македония|MK
    Малави|MW
    Малайзия|MY
    Мали|ML
    Мальдивы|MV
    Мальта|MT
    Марокко|MA
    Мартиника|MQ
    Маршалловы Острова|MH
    Мексика|MX
    Микронезия, федеративные штаты|FM
    Мозамбик|MZ
    Молдова|MD
    Монако|MC
    Монголия|MN
    Монтсеррат|MS
    Мьянма|MM
    Намибия|NA
    Науру|NR
    Непал|NP
    Нигер|NE
    Нигерия|NG
    Нидерландские Антилы|AN
    Нидерланды|NL
    Никарагуа|NI
    Ниуэ|NU
    Новая Зеландия|NZ
    Новая Каледония|NC
    Норвегия|NO
    Объединенные Арабские Эмираты|AE
    Оман|OM
    Остров Мэн|IM
    Остров Норфолк|NF
    Остров Святой Елены|SH
    Острова Кука|CK
    Острова Теркс и Кайкос|TC
    Пакистан|PK
    Палау|PW
    Палестина|PS
    Панама|PA
    Папуа - Новая Гвинея|PG
    Парагвай|PY
    Перу|PE
    Питкерн|PN
    Польша|PL
    Португалия|PT
    Пуэрто-Рико|PR
    Республика Корея|KR
    Реюньон|RE
    Россия|RU
    Руанда|RW
    Румыния|RO
    Сальвадор|SV
    Самоа|WS
    Сан-Марино|SM
    Сан-Томе и Принсипи|ST
    Саудовская Аравия|SA
    Свазиленд|SZ
    Северная Корея|KP
    Северные Марианские острова|MP
    Сейшельские острова|SC
    Сенегал|SN
    Сент-Винсент и Гренадины|VC
    Сент-Китс и Невис|KN
    Сент-Люсия|LC
    Сент-Пьер и Микелон|PM
    Сербия|RS
    Сингапур|SG
    Сирия|SY
    Словакия|SK
    Словения|SI
    Соломоновы Острова|SB
    Сомали|SO
    Судан|SD
    Суринам|SR
    США|US
    Сьерра-Леоне|SL
    Таджикистан|TJ
    Таиланд|TH
    Тайвань|TW
    Танзания|TZ
    Того|TG
    Токелау|TK
    Тонга|TO
    Тринидад и Тобаго|TT
    Тувалу|TV
    Тунис|TN
    Туркменистан|TM
    Турция|TR
    Уганда|UG
    Узбекистан|UZ
    Украина|UA
    Уоллис и Футуна|WF
    Уругвай|UY
    Фарерские острова|FO
    Фиджи|FJ
    Филиппины|PH
    Финляндия|FI
    Фолклендские острова|FK
    Франция|FR
    Французская Гвиана|GF
    Французская Полинезия|PF
    Хорватия|HR
    Центрально-Африканская Республика|CF
    Чад|TD
    Черногория|ME
    Чехия|CZ
    Чили|CL
    Швейцария|CH
    Швеция|SE
    Шпицберген и Ян Майен|SJ
    Шри-Ланка|LK
    Эквадор|EC
    Экваториальная Гвинея|GQ
    Эритрея|ER
    Эстония|EE
    Эфиопия|ET
    Южно-Африканская Республика|ZA
    Ямайка|JM
    Япония|JP
    */
}