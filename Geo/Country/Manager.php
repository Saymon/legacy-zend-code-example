<?php

/**
 * @author Saymon
 */
class Geo_Country_Manager extends Core_Model_Manager
{

    use System_Singleton;

    /**
     * @param string $strCode
     * @return Geo_Country_Row
     */
    public function getByCode($strCode)
    {
        return $this->get($strCode);
    }

    /**
     * Обновляет аггрегационное поле region_сount в таблице стран, на основании данных из таблицы geo_region
     * @return void
     */
    public function updateRegionCount()
    {
        $lstCountries = $this->getTable()->fetchAll();
        foreach ($lstCountries as $rowCountry) {
            $lstRegions = Geo_Region_Manager::getInstance()->getAllByCountry($rowCountry->code());
            $rowCountry->regionCount($lstRegions->count())
                       ->save();
        }
    }

    /**
     * Обновляет аггрегационное поле state_сount в таблице стран, на основании данных из таблицы geo_state
     * @return void
     */
    public function updateStateCount()
    {
        $lstCountries = $this->getTable()->fetchAll();
        foreach ($lstCountries as $rowCountry) {
            $lstState = Geo_State_Manager::getInstance()->getAllByCountry($rowCountry->code());
            $rowCountry->stateCount($lstState->count())
                       ->save();
        }
    }

    /**
     * Обновляет аггрегационное поле city_сount в таблице стран, на основании данных из таблицы geo_city
     * @return void
     */
    public function updateCityCount()
    {
        $lstCountries = $this->getTable()->fetchAll();
        foreach ($lstCountries as $rowCountry) {
            $lstCity = Geo_City_Manager::getInstance()->getAllByCountry($rowCountry->code());
            $rowCountry->cityCount($lstCity->count())
                       ->save();
        }
    }

}