<?php

/**
 * @author Saymon
 */
class Geo_City_Manager extends Core_Model_Manager
{

    use System_Singleton;

    //---------------------------------------------------------------------------------------------------------------------------------------

    /**
     * @param string $strCode
     * @return System_Db_List
     */
    public function getAllByCountry($strCode)
    {
    	Geo_Country_Enum::getInstance()->validate($strCode);

        $arrFilter = [
            'country_code = ?' => $strCode
        ];

        return $this->getTable()->fetchAll($arrFilter);
    }

    /**
     * @param integer $intRegionId
     * @return System_Db_List
     */
    public function getAllByRegion($intRegionId)
    {
        $arrFilter = [
        	'region_id = ?' => $intRegionId
        ];

        return $this->getTable()->fetchAll($arrFilter);
    }

    /**
     * @param integer $intStateId
     * @return System_Db_List
     */
    public function getAllByState($intStateId)
    {
        $arrFilter = [
        	'state_id = ?' => $intStateId
        ];

        return $this->getTable()->fetchAll($arrFilter);
    }

    /**
     * @param string $strName
     * @param string $strCountryCode
     * @param string $intStatus
     * @return Geo_City_Row
     */
    public function add($strName, $strCountryCode, $intStatus = NULL)
    {
        if (is_null($intStatus)) {
            $intStatus = Geo_City_Status_Enum::ADDED;
        }

        $rowCountry = Geo_Country_Manager::getInstance()->getByCode($strCountryCode);
        $rowCity = $this->createNew();
        $rowCity
			->city($strName)
			->countryId($rowCountry->id())
			->countryCode($rowCountry->code())
			->status($intStatus)
			->save()
        ;

        return $rowCity;
    }

}