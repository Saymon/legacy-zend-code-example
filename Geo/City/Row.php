<?php

/**
 * @author Saymon
 */
class Geo_City_Row extends System_Db_Object
{

    /**
     * @return string
     */
    public function getDetailsString()
    {
        if (!$this->regionName()) {
            return '';
        }
        
        if ($this->regionName() == $this->stateName()) {
            return $this->regionName();
        }
        
        if ($this->stateName()) {
            return $this->regionName() . ', ' . $this->stateName();
        }
    }

}