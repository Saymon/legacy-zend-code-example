<?php

/**
 * @author Saymon
 */
class Geo_City_Status_Enum extends Core_Enum_Abstract
{
    
    use System_Singleton;
    
    const INITIAL  = 1;
    const ADDED    = 2;
    const APPROVED = 3;
    const DECLINED = 4;
    
}