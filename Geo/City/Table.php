<?php

/**
 * @author Saymon
 */
class Geo_City_Table extends Core_Db_Table_Abstract
{

    /**
     * @var string
     */
    protected $_name = 'geo_city';

    /**
     * @var array
     */
    protected $_arrFields = [
        'id'           => ['type' => 'int', 'length' => 11, 'not_null' => true, 'unsigned' => true, 'auto_increment' => true],
        'country_id'   => ['type' => 'int', 'length' => 11, 'not_null' => true, 'unsigned' => true],
        'city'         => ['type' => 'varchar', 'length' => 255, 'not_null' => true],
        'state_name'   => ['type' => 'varchar', 'length' => 255],
        'region_name'  => ['type' => 'varchar', 'length' => 255],
        'country_code' => ['type' => 'varchar', 'length' => 2, 'not_null' => true],
        'region_id'    => ['type' => 'int', 'length' => 11, 'unsigned' => true],
        'state_id'     => ['type' => 'int', 'length' => 11, 'unsigned' => true],
        'status'       => ['type' => 'tinyint', 'length' => 3, 'unsigned' => true, 'not_null' => true, 'default' => Geo_City_Status_Enum::INITIAL],
    ];

    /**
     * @var array
     */
    protected $_arrKeys = [
        ['primary key', 'id'],
//         ['unique key', ['country_id', 'region_id', 'state_id', 'city']],
        ['key', 'city'],
        ['key', 'country_id'],
        ['key', 'country_code'],
        ['key', 'region_id'],
        ['key', 'region_name'],
        ['key', 'state_id'],
        ['key', 'state_name'],
        ['key', 'status'],
    ];

}