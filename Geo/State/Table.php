<?php

/**
 * @author Saymon
 */
class Geo_State_Table extends Core_Db_Table_Abstract
{

    /**
     * @var string
     */
    protected $_name = 'geo_state';

    /**
     * @var array
     */
    protected $_arrFields = [
        'id' => ['type' => 'int', 'length' => 10, 'not_null' => true, 'unsigned' => true, 'auto_increment' => true],
        'country_id' => ['type' => 'int', 'length' => 10, 'not_null' => true, 'unsigned' => true],
        'country_code' => ['type' => 'varchar', 'length' => 2, 'not_null' => true],
        'region_id' => ['type' => 'int', 'length' => 10, 'not_null' => true, 'unsigned' => true],
        'name' => ['type' => 'varchar', 'length' => 255, 'not_null' => true],
        'city_count' => ['type' => 'int', 'length' => 10, 'unsigned' => true],
    ];

    /**
     * @var array
     */
    protected $_arrKeys = [
        ['primary key', 'id'],
        ['key', 'country_id'],
        ['key', 'country_code'],
        ['key', 'region_id'],
        ['key', 'name'],
        ['key', 'city_count'],
        ['unique key', ['country_id', 'region_id', 'name']],
    ];

}