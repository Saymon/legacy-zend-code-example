<?php

/**
 * @author Saymon
 */
class Geo_State_Manager extends Core_Model_Manager
{

    use System_Singleton;

    //---------------------------------------------------------------------------------------------------------------------------------------

    /**
     * @param string $strCode
     * @return System_Db_List
     */
    public function getAllByCountry($strCode)
    {
    	Geo_Country_Enum::getInstance()->validate($strCode);

        $arrFilter = [
            'country_code = ?' => $strCode
        ];

        return $this->getTable()->fetchAll($arrFilter);
    }

    /**
     * @param integer $intRegionId
     * @return System_Db_List
     */
    public function getAllByRegion($intRegionId)
    {
        $arrFilter = [
            'region_id = ?' => $intRegionId
        ];

        return $this->getTable()->fetchAll($arrFilter);
    }

    /**
     * Обновляет аггрегационное поле city_сount в таблице geo_state, на основании данных из таблицы geo_city
     */
    public function updateCityCount()
    {
        $lstStates = $this->getTable()->fetchAll();
        foreach ($lstStates as $rowState)
        {
            $lstCity = Geo_City_Manager::getInstance()->getAllByState($rowState->id());
            $rowState
				->cityCount($lstCity->count())
				->save()
            ;
        }
    }

}