<?php

/**
 * @author saymon
 */
class Geo_Model_Enum extends Core_Enum_Abstract
{
	
	/**
	 * @var string
	 */
	const CITY						= 'Geo_City';
	
	/**
	 * @var string
	 */
	const COUNTRY					= 'Geo_Country';
	
	/**
	 * @var string
	 */
	const REGION					= 'Geo_Region';
	
	/**
	 * @var string
	 */
	const STATE						= 'Geo_State';
	
}