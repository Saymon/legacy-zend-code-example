<?php

/**
 * @author saymon
 */
class Geo_Model_Manager extends Core_Db_Entity_Model_Manager
{
	
	use System_Singleton;
	
	/**
	 * @return Core_Db_Entity_Model_Base
	 */
	public function modCity()
	{
		return $this->get(Geo_Model_Enum::CITY);
	}
	
	/**
	 * @return Core_Db_Entity_Model_Base
	 */
	public function modCountry()
	{
		return $this->get(Geo_Model_Enum::COUNTRY);
	}
	
	/**
	 * @return Core_Db_Entity_Model_Base
	 */
	public function modRegion()
	{
		return $this->get(Geo_Model_Enum::REGION);
	}
	
	/**
	 * @return Core_Db_Entity_Model_Base
	 */
	public function modState()
	{
		return $this->get(Geo_Model_Enum::STATE);
	}
	
}