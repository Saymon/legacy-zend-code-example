<?php

/**
 * @author Sergey Ivanov
 *
 */
class EasyPay_Option_Manager
{

	use System_Singleton;

	/**
	 * @return string
	 */
	public function authHashMethod()
	{
		$method = EasyPay_Component::getInstance()->getConfig()->authHashMethod;
		System_Exception_Null::getInstance()->validate($method, 'Undefined config parameter: "authHashMethod"');

		return $method;
	}

	/**
	 * @return string
	 */
	public function authKeyHash()
	{
		$hash = EasyPay_Component::getInstance()->getConfig()->authKeyHash;
		System_Exception_Null::getInstance()->validate($hash, 'Undefined config parameter: "authKeyHash"');

		return $hash;
	}

	/**
	 * @return string
	 */
	public function authSignHash()
	{
		$hash = EasyPay_Component::getInstance()->getConfig()->authSignHash;
		System_Exception_Null::getInstance()->validate($hash, 'Undefined config parameter: "authSignHash"');

		return $hash;
	}

	/**
	 * return
	 */
	public function serviceTerminalStatus()
	{
		return EasyPay_Component::getInstance()->getConfig()->services->terminal->enabled;
	}

	/**
	 * @return array
	 */
	public function allowedIps()
	{
		$ips = EasyPay_Component::getInstance()->getConfig()->allowedIps;

		return $ips->toArray();
	}

	/**
	 * @return string
	 */
	public function urlService()
	{
		return EasyPay_Component::getInstance()->getConfig()->urlService;
	}

}