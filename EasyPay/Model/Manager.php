<?php

/**
 * @author Sergey Ivanov
 */
class EasyPay_Model_Manager extends Core_Db_Entity_Model_Manager
{

	use System_Singleton;

	//---------------------------------------------------------------------------------------------------------------------------------------

	/**
	 * @return Core_Db_Entity_Model_Base
	 */
	public function modTerminalRecharge()
	{
		return $this->get(EasyPay_Model_Enum::TERMINAL_RECHARGE);
	}

	/**
	 * @return Core_Db_Entity_Model_Base
	 */
	public function modTerminalRechargeCancel()
	{
		return $this->get(EasyPay_Model_Enum::TERMINAL_RECHARGE_CANCEL);
	}

	/**
	 * @return Core_Db_Entity_Model_Base
	 */
	public function modTerminalRechargeStatus()
	{
		return $this->get(EasyPay_Model_Enum::TERMINAL_RECHARGE_STATUS);
	}

}