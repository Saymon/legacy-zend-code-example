<?php

/**
 * @author Sergey Ivanov
 */
class EasyPay_Model_Enum extends Core_Enum_Abstract
{

	/**
	 * @var string
	 */
	const TERMINAL_RECHARGE									= 'EasyPay_Terminal_Recharge';

	/**
	 * @var string
	 */
	const TERMINAL_RECHARGE_CANCEL							= 'EasyPay_Terminal_Recharge_Cancel';

	/**
	 * @var string
	 */
	const TERMINAL_RECHARGE_STATUS							= 'EasyPay_Terminal_Recharge_Status';

}