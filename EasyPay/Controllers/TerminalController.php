<?php

/**
 * @author Sergey Ivanov
 */
class EasyPay_TerminalController extends System_Controller_Base
{

	/**
	 * @var EasyPay_Terminal_Service
	 */
	private $_service;

	/* (non-PHPdoc)
	 * @see System_Controller_Base::init()
	 */
	public function init()
	{
		$this->_disableView();

		parent::init();
	}

	/**
	 * глобальный action для api
	 */
	public function paymentAddAction()
	{
		$this->_disableView();

		$this->_processRequest();
	}

	/**
	 * для сверки данных
	 */
	public function dayReportAction()
	{
		$this->_disableView();

		$this->_processRequest();
	}

	/**
	 * @return EasyPay_Terminal_Service
	 */
	protected function _getService()
	{
		if (is_null($this->_service))
		{
			$this->_service = new EasyPay_Terminal_Service();
		}

		return $this->_service;
	}

	/**
	 * Выполняет запрос к сервису обслуживания терминалов
	 */
	protected function _processRequest()
	{
		$request = $this->getRequest();
// 		$params = Tools_Xml_Parser::getInstance()->xmlToArray($this->getRequest()->getParam('Request'));
		$params = Tools_Xml_Parser::getInstance()->xmlToArray($request->getRawBody());

		$request->setParams($params);

		if (!$request->isPost())
		{
			return;
		}

		$response = $this->_getService()->processRequest($request);

		echo $response->render();
	}

}