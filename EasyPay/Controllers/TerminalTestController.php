<?php

/**
 * @author Sergey Ivanov
 */
class EasyPay_TerminalTestController extends System_Controller_Base
{
	/**
	 * @param Tools_Xml_Generator $xmlGenerator
	 */
	private function _generateResponseForQueryType(& $xmlGenerator)
	{
		$request = $this->getRequest();

		switch ($request->getParam('QueryType'))
		{
			case 'check':
				$xmlGenerator->openElement('Check', NULL);
// 					$xmlGenerator->openElement('ServiceId', NULL);
// 					$xmlGenerator->closeElement('ServiceId');
					$xmlGenerator->openElement('Account', $request->getParam('Account'));
					$xmlGenerator->closeElement('Account');
				$xmlGenerator->closeElement('Check');
				break;
			case 'Payment':
				$xmlGenerator->openElement('Payment', NULL);
					$xmlGenerator->openElement('OrderId', $request->getParam('OrderId'));
					$xmlGenerator->closeElement('OrderId');
					$xmlGenerator->openElement('Account', $request->getParam('Account'));
					$xmlGenerator->closeElement('Account');
					$xmlGenerator->openElement('Amount', $request->getParam('Amount'));
					$xmlGenerator->closeElement('Amount');
				$xmlGenerator->closeElement('Payment');
				break;
			case 'Confirm':
				$xmlGenerator->openElement('Confirm', NULL);
					$xmlGenerator->openElement('PaymentId', $request->getParam('PaymentId'));
					$xmlGenerator->closeElement('PaymentId');
				$xmlGenerator->closeElement('Confirm');
			case 'Status':
				$xmlGenerator->openElement('Status', NULL);
					$xmlGenerator->openElement('PaymentId', $request->getParam('PaymentId'));
					$xmlGenerator->closeElement('PaymentId');
				$xmlGenerator->closeElement('Status');
				break;
			case 'Cancel':
				$xmlGenerator->openElement('Cancel', NULL);
					$xmlGenerator->openElement('PaymentId', $request->getParam('PaymentId'));
					$xmlGenerator->closeElement('PaymentId');
				$xmlGenerator->closeElement('Cancel');
				break;
		}
	}

	/**
	 * Тестовые формы на функции сервиса обслуживания терминалов
	 */
	public function testAction()
	{

	}

	/**
	 * Генерирует ожидаемый XML от EasyPay
	 */
	public function generateXmlAction()
	{
		$request = $this->getRequest();
		$strFormat = 'YYYY-MM-ddTHH:mm:ss';
		$xmlGenerator = new Tools_Xml_Generator();
		$xmlGenerator->openElement('Request', NULL);
			$xmlGenerator->openElement('DateTime', System_Date_Utc::now()->toString($strFormat));
			$xmlGenerator->closeElement('DateTime');
			$xmlGenerator->openElement('KeyHash', 'admin');
			$xmlGenerator->closeElement('KeyHash');
			$this->_generateResponseForQueryType($xmlGenerator);
		$xmlGenerator->closeElement('Request');

		$this->_disableView();

		echo $xmlGenerator->flush();
	}

}