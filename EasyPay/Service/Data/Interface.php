<?php

interface EasyPay_Service_Data_Interface
{

	/**
	 * формирует xml для отправки
	 */
	public function toXml();

}