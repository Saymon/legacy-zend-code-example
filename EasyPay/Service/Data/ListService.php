<?php

/**
 * @author Sergey Ivanov
 *
 * @method string getDateTime()
 * @method string getSign()
 * @method string getLang()
 *
 * @method EasyPay_Service_Data_ListService setDateTime()
 * @method EasyPay_Service_Data_ListService setSign()
 * @method EasyPay_Service_Data_ListService setLang()
 *
 */
class EasyPay_Service_Data_ListService extends EasyPay_Service_Data_Abstract implements EasyPay_Service_Data_Interface
{

	/**
	 * дата отправки
	 *
	 * @var string
	 */
	protected $_dateTime;

	/**
	 * hex строка
	 *
	 * @var string
	 */
	protected $_sign;

	/**
	 * язык описания услуг
	 *
	 * доступны:
	 * ru - руский
	 * en - английский
	 * uk - украинский
	 *
	 * @var string
	 */
	protected $_lang;

	//---------------------------------------------------------------------------------------------------------------------

	/**
	 * @return boolean
	 */
	private function _validate()
	{
		return TRUE;
	}

	private function _generateSign($xml)
	{
		$xml = str_replace('<?xml version="1.0" encoding="UTF-8"?>', '', $xml);
		$xml = str_replace('<Sign/>', '<Sign></Sign>', $xml);
		$buff = mb_strlen($xml);

		$fp = fopen("/var/www/html/wmt/var/easypay_keys/private.key", "r");
		$pkeyid = fread($fp, 8192);
		fclose($fp);
		$pr_key = openssl_get_privatekey($pkeyid);
		openssl_sign($xml, $sign, $pr_key);
		$hexsign = bin2hex($sign);
		$this->setSign(strtoupper($hexsign));
		return true;
		return str_replace("<Sign></Sign>", "<Sign>".strtoupper($hexsign)."</Sign>", $xml);


		Development_Debug::dumpDie($buff);
		$this->setSign('');
	}

	//---------------------------------------------------------------------------------------------------------------------

	public function __construct()
	{
		$this
			->setDateTime(System_Date_Utc::now()->toString('YYYY-MM-ddTHH:mm:ss'))
			->setLang('ru')
		;
	}

	//---------------------------------------------------------------------------------------------------------------------

	/* (non-PHPdoc)
	 * @see EasyPay_Service_Data_Interface::toXml()
	 */
	public function toXml()
	{
		$this->_validate();

		$xmlGenerator = new Tools_Xml_Generator();

		$xmlGenerator->openElement('Request');
		$this->_generateXmlRow('DateTime', $this->getDateTime(), $xmlGenerator);
		$this->_generateXmlRow('Sign', NULL, $xmlGenerator);
			$xmlGenerator->openElement('Services');
			$this->_generateXmlRow('Lang', $this->getLang(), $xmlGenerator);
			$xmlGenerator->closeElement('Services');
		$xmlGenerator->closeElement('Request');

		$xml =  $xmlGenerator->flush();

		$this->_generateSign($xml);

		$xmlGenerator->__destruct();
		$xmlGenerator = new Tools_Xml_Generator();
		$xmlGenerator->openElement('Request');
		$this->_generateXmlRow('DateTime', $this->getDateTime(), $xmlGenerator);
		$this->_generateXmlRow('Sign', $this->getSign(), $xmlGenerator);
		$xmlGenerator->openElement('Services');
		$this->_generateXmlRow('Lang', $this->getLang(), $xmlGenerator);
		$xmlGenerator->closeElement('Services');
		$xmlGenerator->closeElement('Request');

		$xml = str_replace('<?xml version="1.0" encoding="UTF-8"?>', '', $xmlGenerator->flush());

		$ch = curl_init('https://gateway.easypay.ua/30');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_CAINFO, "/var/www/html/wmt/var/easypay_keys/test.pem");
		curl_setopt($ch, CURLOPT_POST, $xml);

		Development_Debug::dump(curl_exec($ch));
		Development_Debug::dumpDie(curl_error($ch));
	}

}