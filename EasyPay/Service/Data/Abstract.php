<?php

class EasyPay_Service_Data_Abstract
{

	/**
	 * @param string $rowName
	 * @param string $rowValue
	 * @param Tools_Xml_Generator $objGenerator
	 *
	 * @return Tools_Xml_Generator
	 */
	protected function _generateXmlRow($rowName, $rowValue, & $objGenerator)
	{
		$objGenerator->openElement($rowName, $rowValue);
		$objGenerator->closeElement($rowName);

		return $objGenerator;
	}

	//---------------------------------------------------------------------------------------------------------------------

	/**
	 * @param string $strMethodName
	 * @param array $args
	 * @return mixed
	 */
	public function __call($strMethodName, $args)
	{
		if (!preg_match('/^(get)|(set)/', $strMethodName))
		{
			throw new System_Exception_Null("Unrecognized method '$strMethodName()'");
		}

		$strFieldName = '_' . lcfirst(substr($strMethodName, 3, strlen($strMethodName) - 3));

		if (!property_exists($this, $strFieldName))
		{
			throw new System_Exception_Null("Unrecognized property '$strFieldName()'");
		}

		if (preg_match('/^(set)/', $strMethodName))
		{
			$this->{$strFieldName} = current($args);
			return $this;
		}
		else
		{
			return $this->{$strFieldName};
		}
	}


}