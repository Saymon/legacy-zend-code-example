<?php

/**
 * @author Sergey Ivanov
 */
class EasyPay_Client_Exception extends EasyPay_Exception_Abstract
{
	
	/**
	 * @param string $message
	 */
	public function __construct($message)
	{
		$this->setKey(EasyPay_Exception_Enum::EasyPay_CLIENT_EXCEPTION);
		
		parent::__construct($message);
	}
	
}