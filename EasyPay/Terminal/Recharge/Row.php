<?php

/**
 * @author Sergey Ivanov
 *
 * @method EasyPay_Terminal_Recharge_Row EasyPayTransactionId()
 * @method EasyPay_Terminal_Recharge_Row EasyPayTransactionDatetimeCreated()
 * @method EasyPay_Terminal_Recharge_Row EasyPayTerminalId()
 * @method EasyPay_Terminal_Recharge_Row EasyPayTerminalTransactionId()
 * @method EasyPay_Terminal_Recharge_Row recipientName()
 * @method EasyPay_Terminal_Recharge_Row amountReceived()
 * @method EasyPay_Terminal_Recharge_Row amountToEnroll()
 * @method EasyPay_Terminal_Recharge_Row status()
 * @method EasyPay_Terminal_Recharge_Row inserted()
 */
class EasyPay_Terminal_Recharge_Row extends System_Db_Object implements Bank_PaymentOrder_InitialDocument_Interface
{

	/**
	 * @var array
	 */
	protected $_arrPeriodical = [
			'_postInsert' => ['status'],
			'_postUpdate' => ['status'],
	];

	/* (non-PHPdoc)
	 * @see System_Db_Object::_insert()
	 */
	protected function _insert()
	{

		$this
			->status(EasyPay_Terminal_Recharge_Status_Enum::CREATED)
			->inserted($this->_currentDateUtc())
		;
	}

	//---------------------------------------------------------------------------------------------------------------------------------------

	/* (non-PHPdoc)
	 * @see Bank_PaymentOrder_InitialDocument_Interface::id()
	 */
	public function id()
	{
		return $this->id;
	}

	/* (non-PHPdoc)
	 * @see Bank_PaymentOrder_InitialDocument_Interface::getAccount()
	 */
	public function getAccount()
	{
		$bridge = EasyPay_Bridge_Manager::getInstance()->getCurrent();
		return $bridge->getAccount($this->recipientName());
	}

	/* (non-PHPdoc)
	 * @see Bank_PaymentOrder_InitialDocument_Interface::amount()
	 */
	public function getAmount()
	{
		return $this->amountToEnroll();
	}

	/* (non-PHPdoc)
	 * @see Bank_PaymentOrder_InitialDocument_Interface::comments()
	 */
	public function getComments()
	{
		return 'Recharge through EasyPay terminal. Transaction #' . $this->easypayOrderId() . '';
	}

	/* (non-PHPdoc)
	 * @see Bank_PaymentOrder_InitialDocument_Interface::getPaymentOrder()
	 */
	public function getPaymentOrder()
	{
		$tblPaymentOrder = Bank_Model_Manager::getInstance()->modPaymentOrder()->table();

		$rowPeymentOrder =  $tblPaymentOrder->fetchByFitler([
			'initial_document_model =?' => $this->getClassName(),
			'`initial_document_id` =?' 	=> $this->id()
		])->current();

		if (!is_object($rowPeymentOrder))
		{
			return NULL;
		}

		return (new Bank_PaymentOrder_Manager())->loadById($rowPeymentOrder->id());
	}

}