<?php

/**
 * @author Sergey Ivanov
 */
class EasyPay_Terminal_Recharge_Table extends Core_Db_Table_Abstract
{

    /**
     * @var string
     */
    protected $_name = 'easypay_terminal_recharge';

    /**
     * @var array
     */
    protected $_arrFields = [
        'id'									=> ['type' => 'bigint', 'length' => 20, 'not_null' => true, 'unsigned' => true, 'auto_increment' => true],
        'easypay_order_id'						=> ['type' => 'bigint', 'length' => 20, 'not_null' => true, 'unsigned' => true],
        'easypay_transaction_datetime_created'	=> ['type' => 'datetime', 'not_null' => true],
        'easypay_partner_object_id'				=> ['type' => 'varchar', 'length' => 255],
        'easypay_commission'					=> ['type' => 'int', 'length' => 10],
        'recipient_name'						=> ['type' => 'varchar', 'length' => 20, 'not_null' => true],
        'amount_to_enroll'						=> ['type' => 'decimal', 'float_dimension' => 'amount', 'not_null' => true, 'unsigned' => true],
        'session_id'							=> ['type' => 'varchar', 'length' => 255],
        'status'								=> ['type' => 'tinyint', 'length' => 3, 'not_null' => true, 'unsigned' => true, 'periodical' => 'EasyPay_Terminal_Recharge_Status'],
        'inserted'								=> ['type' => 'datetime'],
    ];

    /**
     * @var array
     */
    protected $_arrKeys = [
        ['primary key', 'id'],
        ['unique key', 'easypay_order_id'],
        ['key', 'recipient_name'],
        ['key', 'status'],
    ];

}