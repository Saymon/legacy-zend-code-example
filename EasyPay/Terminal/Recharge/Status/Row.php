<?php

/**
 * @author Sergey Ivanov
 */
class EasyPay_Terminal_Recharge_Status_Row extends System_Db_Row_Periodical_Abstract
{

	/* (non-PHPdoc)
	 * @see Zend_Db_Table_Row_Abstract::_delete()
	*/
	protected function _delete()
	{
		throw new System_Exception('Operation delete not permitted');
	}
	
	/**
	 * @param EasyPay_Terminal_Recharge_Row $parent
	 * @return self
	 */
	public function fill($parent)
	{
		$this
			->rechargeId($parent->id())
			->status($parent->status())
		;
	
		return $this;
	}

}