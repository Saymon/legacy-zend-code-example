<?php

/**
 * @author Sergey Ivanov
 */
class EasyPay_Terminal_Recharge_Status_Enum extends Core_Enum_Abstract
{
	
	const CREATED		= 1;
	const PROCESSING	= 2;
	const COMPLETED 	= 3;
	const FAILED		= 4;
	
}