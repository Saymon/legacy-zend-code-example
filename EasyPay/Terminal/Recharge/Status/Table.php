<?php

/**
 * @author Sergey Ivanov
 */
class EasyPay_Terminal_Recharge_Status_Table extends Core_Db_Table_Abstract
{

    /**
     * @var string
     */
    protected $_name = 'easypay_terminal_recharge_status';

    /**
     * @var array
     */
    protected $_arrFields = [
        'id'			=> ['type' => 'bigint', 'length' => 20, 'not_null' => true, 'unsigned' => true, 'auto_increment' => true],
        'recharge_id'	=> ['type' => 'bigint', 'length' => 20, 'not_null' => true, 'unsigned' => true],
        'status'		=> ['type' => 'tinyint', 'length' => 3, 'not_null' => true, 'unsigned' => true],
        'time'			=> ['type' => 'datetime'],
    ];

    /**
     * @var array
     */
    protected $_arrKeys = [
        ['primary key', 'id'],
        ['key', 'recharge_id'],
        ['key', 'status'],
    ];

    /**
     * @var array
     */
    protected $_referenceMap = [
	    [
		    self::COLUMNS 			=> 'recharge_id',
		    self::REF_TABLE_CLASS  	=> 'EasyPay_Terminal_Recharge_Table',
	    ],
    ];

}