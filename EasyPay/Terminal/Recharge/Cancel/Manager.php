<?php

/**
 * @author Sergey Ivanov
 *
 */
class EasyPay_Terminal_Recharge_Cancel_Manager extends Core_Model_Manager
{

	/**
	 * @param string $revertId
	 * @param string $revertDate
	 * @return Zend_Db_Table_Row_Abstract|System_Db_Object
	 */
	public function addIfNotExists($revertId, $revertDate)
	{
		$row = (new EasyPay_Terminal_Recharge_Cancel_Table())->fetchByFitler([
			'easypay_order_revert_id = ?'	=> $revertId
		])->current();

		if (is_object($row))
		{
			return $row;
		}

		$row = $this->createNew();
		$row
			->easypayOrderRevertId($revertId)
			->easypayOrderRevertDate($revertDate)
			->save()
		;

		return $row;
	}

}