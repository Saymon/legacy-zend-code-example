<?php

/**
 * @author Sergey Ivanov
 */
class EasyPay_Terminal_Recharge_Cancel_Table extends Core_Db_Table_Abstract
{

    /**
     * @var string
     */
    protected $_name = 'easypay_terminal_recharge_cancel';

    /**
     * @var array
     */
    protected $_arrFields = [
        'id'									=> ['type' => 'bigint', 'length' => 20, 'not_null' => true, 'unsigned' => true, 'auto_increment' => true],
        'easypay_order_revert_id'				=> ['type' => 'bigint', 'length' => 20, 'not_null' => true, 'unsigned' => true],
        'revert_id'								=> ['type' => 'datetime'],
        'comment'								=> ['type' => 'varchar', 'length' => 255,],
        'status'								=> ['type' => 'tinyint', 'length' => 3, 'not_null' => true, 'unsigned' => true, 'periodical' => 'EasyPay_Terminal_Recharge_Status'],
        'inserted'								=> ['type' => 'datetime'],
    ];

    /**
     * @var array
     */
    protected $_arrKeys = [
        ['primary key', 'id'],
        ['unique key', 'easypay_order_revert_id'],
        ['key', 'status'],
    ];

    /**
     * @return System_Db_List
     */
    public function getActualRequest()
    {
    	return $this->fetchByFitler([
    		'status =?' => EasyPay_Terminal_Recharge_Cancel_Status_Enum::CREATED
    	]);
    }

}