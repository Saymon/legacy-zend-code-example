<?php

/**
 * @author Sergey Ivanov
 *
 */
class EasyPay_Terminal_Recharge_Cancel_Row extends System_Db_Object
{

	/* (non-PHPdoc)
	 * @see System_Db_Object::_insert()
	 */
	protected function _insert()
	{
		$this
			->status(EasyPay_Terminal_Recharge_Cancel_Status_Enum::CREATED)
			->inserted($this->_currentDateUtc())
		;
	}

}