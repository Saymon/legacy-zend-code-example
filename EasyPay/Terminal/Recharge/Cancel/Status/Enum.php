<?php

/**
 * @author Sergey Ivanov
 */
class EasyPay_Terminal_Recharge_Cancel_Status_Enum extends Core_Enum_Abstract
{

	const CREATED		= 1;
	const PROCESSING	= 2;
	const CONFIRMED 	= 3;
	const REJECTED		= 4;

}