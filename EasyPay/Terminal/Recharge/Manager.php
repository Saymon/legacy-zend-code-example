<?php

/**
 * @author Sergey Ivanov
 *
 * @method EasyPay_Terminal_Recharge_Row createNew()
 */
class EasyPay_Terminal_Recharge_Manager extends Core_Model_Manager
{

	/**
	 * @param string 	$transactionDatetime
	 * @param string 	$recipientName
	 * @param float 	$amountToEnroll
	 * @param string 	$partnerObjectId
	 * @param integer 	$commission
	 * @param integer 	$orderId
	 * @return Zend_Db_Table_Row_Abstract|EasyPay_Terminal_Recharge_Row
	 */
	public function addIfNotExists($transactionDatetime, $recipientName, $amountToEnroll, $partnerObjectId, $commission, $orderId)
	{
		$row = $this->findByFilter([
			'easypay_order_id =?' => $orderId
		])->current();

		if (is_object($row))
		{
			return $row;
		}

		$row = $this->createNew();
		$row
			->easypayOrderId($orderId)
			->easypayTransactionDatetimeCreated($transactionDatetime)
			->easypayPartnerObjectId($partnerObjectId)
			->easypayCommission($commission)
			->recipientName($recipientName)
			->amountToEnroll($amountToEnroll)
			->save()
		;

		return $row;
	}

}