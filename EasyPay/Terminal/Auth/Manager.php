<?php

/**
 * @author Sergey Ivanov
 */
class EasyPay_Terminal_Auth_Manager
{

	use System_Singleton;

	//----------------------------------------------------------------------

	/**
	 * @var boolean
	 */
	private $_isIdentified = FALSE;

	/**
	 * @var boolean
	 */
	private $_isAuthorized = FALSE;

	/**
	 * @var System_Controller_Request_HTTP
	 */
	private $_httpRequest;

	//----------------------------------------------------------------------

	/**
	 * @param string $key
	 * @return EasyPay_Terminal_Auth_Manager
	 */
	protected function _verifyAuthKey($key)
	{
		$optionManager = EasyPay_Option_Manager::getInstance();

		$authKeyHash = $optionManager->authKeyHash();

		if (!$this->_verifyHash($key, $authKeyHash))
		{
			throw new EasyPay_Terminal_Auth_Exception_InvalidAuthKey();
		}

		return $this;
	}

	/**
	 * @param string $sign
	 * @return EasyPay_Terminal_Auth_Manager
	 */
	protected function _verifyAuthSign($sign)
	{
		$optionManager = EasyPay_Option_Manager::getInstance();

		$authSignHash = $optionManager->authSignHash();

		if (!$this->_verifyHash($sign, $authSignHash))
		{
			throw new EasyPay_Terminal_Auth_Exception_InvalidAuthSign();
		}

		return $this;
	}

	/**
	 * @param string $value
	 * @param string $hash
	 * @return boolean
	 */
	protected function _verifyHash($value, $hash)
	{
		$crypter = System_Crypt_Hash_Manager::getInstance();

		$optionManager = EasyPay_Option_Manager::getInstance();

		$hashMethod = $optionManager->authHashMethod();

		$isValid = $crypter->verify($hashMethod, $value, $hash);

		return $isValid;
	}

	/**
	 * @throws EasyPay_Terminal_Auth_Exception_IpUndefined
	 * @throws EasyPay_Terminal_Auth_Exception_IpDenied
	 */
	protected function _authorizeIp()
	{
		$clientIP = $this->_getHttpRequest()->getClientIp();

		if (!$clientIP)
		{
			throw new EasyPay_Terminal_Auth_Exception_IpUndefined();
		}

		$ipsAllowed = EasyPay_Option_Manager::getInstance()->allowedIps();

		if (in_array($clientIP, $ipsAllowed))
		{
			return;
		}

		throw new EasyPay_Terminal_Auth_Exception_IpDenied();
	}

	/**
	 * @return EasyPay_Terminal_Auth_Manager
	 */
	protected function _setIdentified()
	{
		$this->_isIdentified = TRUE;

		return $this;
	}

	/**
	 * @return EasyPay_Terminal_Auth_Manager
	 */
	protected function _setAuthorized()
	{
		$this->_isAuthorized = TRUE;

		return $this;
	}

	/**
	 * @return System_Controller_Request_HTTP
	 */
	protected function _getHttpRequest()
	{
		return $this->_httpRequest;
	}

	//----------------------------------------------------------------------

	/**
	 * @param System_Controller_Request_HTTP $request
	 * @return EasyPay_Terminal_Auth_Manager
	 */
	public function setHttpRequest(System_Controller_Request_HTTP $request)
	{
		$this->_httpRequest = $request;

		return $this;
	}

	/**
     * Идентифицирует агента по ключу и подписи (отказались пожно будет удалить)
	 */
	public function identify()
	{
// 		$crypter = System_Crypt_Hash_Manager::getInstance();

// 		$httpRequest = $this->_getHttpRequest();

// 		$authKey	= $httpRequest->getParam('KeyHash');
// 		$authSign	= $httpRequest->getParam('Sign');
// 		if (!$authKey)
// 		{
// 			throw new EasyPay_Terminal_Auth_Exception_EmptyAuthKey();
// 		}

// 		if (!$authSign)
// 		{
// 			throw new EasyPay_Terminal_Auth_Exception_EmptyAuthSign();
// 		}

		$this
// 			->_verifyAuthKey($authKey)
// 			->_verifyAuthSign($authSign)
			->_setIdentified()
		;

		return $this;
	}

	/**
	 * Выполняет проверки на разрешенный IP адрес, и вызывает метод проверки у моста на соответствующую компоненту
	 */
	public function authorize()
	{
		$this->_authorizeIp();

		EasyPay_Bridge_Manager::getInstance()->getCurrent()->isServiceAvailable();

		$this->_setAuthorized();

		return $this;
	}

}