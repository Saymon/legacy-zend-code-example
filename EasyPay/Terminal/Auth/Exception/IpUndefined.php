<?php

/**
 * @author Sergey Ivanov
 */
class EasyPay_Terminal_Auth_Exception_IpUndefined extends EasyPay_Terminal_Auth_Exception_Abstract
{
	
	/**
	 * @var string
	 */
	protected $_key = EasyPay_Exception_Enum::SERVICE_TERMINAL_AUTHORIZATION_IP_UNDEFINED;

}