<?php

/**
 * @author Sergey Ivanov
 */
class EasyPay_Terminal_Auth_Exception_EmptyAuthSign extends EasyPay_Terminal_Auth_Exception_Abstract
{
	
	/**
	 * @var string
	 */
	protected $_key = EasyPay_Exception_Enum::SERVICE_TERMINAL_EMPTY_AUTH_SIGN;
	
}