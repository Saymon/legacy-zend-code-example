<?php

/**
 * @author Sergey Ivanov
 */
class EasyPay_Terminal_Service_Status_Enum extends Core_Enum_Abstract
{
	
	const DISABLED	= 0;
	const ENABLED	= 1;
	
}