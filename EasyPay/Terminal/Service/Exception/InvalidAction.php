<?php

/**
 * @author Sergey Ivanov
 */
class EasyPay_Terminal_Service_Exception_InvalidAction extends EasyPay_Terminal_Service_Exception
{

	/**
	 * @var string
	 */
	protected $_key = EasyPay_Exception_Enum::SERVICE_TERMINAL_INVALID_FUNCTION;

}