<?php

/**
 * @author Sergey Ivanov
 */
abstract class EasyPay_Terminal_Response_Success extends EasyPay_Terminal_Response_Abstract
{

	/**
	 *
	 */
	public function __construct()
	{
		$this->setCode(0);

		parent::__construct();
	}

}