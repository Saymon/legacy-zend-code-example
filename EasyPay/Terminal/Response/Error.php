<?php

/**
 * @author Sergey Ivanov
 */
class EasyPay_Terminal_Response_Error extends EasyPay_Terminal_Response_Abstract
{

	/**
	 * @var array
	 */
	protected $_errors;

	/**
	 * @param string $key
	 */
	private function _errorKeyToCode($key)
	{
		switch ($key)
		{
			case 'accountNotFound':
				$this->setCode(21);
				break;
			case 'walletNotFound':
				$this->setCode(24);
				break;
			case 'accountSuspended':
				$this->setCode(22);
				break;
			case 'amountToEnroll_notGreaterThan':
				$this->setCode(241);
				break;
			case 'amountToEnroll_notLessThan':
				$this->setCode(242);
				break;
			case 'internalApplicationError':
				$this->setCode(2);
				break;
			default:
				$this->setCode(25);
				break;
		}
	}

	/**
	 *
	 */
	public function __construct($transactionId)
	{
		$this->setTransactionId($transactionId);

		parent::__construct();
	}

	/* (non-PHPdoc)
	 * @see EasyPay_Terminal_Response_Abstract::render()
	 */
	public function render()
	{
		$strDateFormat = 'YYYY-MM-ddTHH:mm:ss';

		$objXmlGenerator = new Tools_Xml_Generator();

		$objXmlGenerator->openElement('Response', NULL);
			$objXmlGenerator->openElement('StatusCode', $this->_code);
			$objXmlGenerator->closeElement('StatusCode');
			$objXmlGenerator->openElement('StatusDetail', Zend_Json_Encoder::encode($this->_errors));
			$objXmlGenerator->closeElement('StatusDetail');
			$objXmlGenerator->openElement('DateTime', System_Date_Utc::now()->toString($strDateFormat));
			$objXmlGenerator->closeElement('DateTime');
		$objXmlGenerator->closeElement('Response');
		return $objXmlGenerator->flush();
	}

	/**
	 * @param array $value
	 * @return self
	 */
	public function setErrors(array $value)
	{
		$this->_errors = $value;

		foreach ($this->_errors as $key => $arrErrors)
		{
			$this->_errorKeyToCode($key);
		}

		return $this;
	}

	/**
	 * @param EasyPay_Exception_Abstract $exception
	 * @return self
	 */
	public function setExceptionErrors(EasyPay_Exception_Abstract $exception)
	{
		$errors = [$exception->getKey() => $exception->getMessage()];

		$this->setErrors($errors);

		return $this;
	}

	/**
	 * @param Core_Validation_Set $validator
	 * @return self
	 */
	public function setValidatorErrors(Core_Validation_Set $validator)
	{
		$errors = [];

		/* @var $errorItem Core_Validation_Error */
		foreach ($validator->getErrors() as $errorItem)
		{
			$item = $errorItem->getItem();

			/* @var $validatorErrors Core_Validate_Abstract */
			foreach ($item->getValidator()->errors() as $validatorErrors)
			{
				foreach ($validatorErrors['errors'] as $error)
				{
					$key = $item->getValidated()->getValidationName() . '_' . $error;
					$errors[] = [$key => $error];
				}
			}
		}

		$this->setErrors($errors);

		return $this;
	}

}