<?php

/**
 * @author Sergey Ivanov
 */
abstract class EasyPay_Terminal_Response_Abstract
{

	/**
	 * @var string
	 */
	protected $_code;

	/**
	 * @var intager
	 */
	protected $_transactionId;

	/**
	 * @return string
	 */
	abstract public function render();

	public function __construct()
	{
		header('Content-type:text/xml');
	}

	/**
	 * @param string $value
	 * @return self
	*/
	public function setCode($code)
	{
		$this->_code = $code;

		return $this;
	}

	public function setTransactionId($value)
	{
		$this->_transactionId = $value;
	}

}