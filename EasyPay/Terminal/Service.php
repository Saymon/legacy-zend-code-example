<?php

/**
 * @author Sergey Ivanov
 */
class EasyPay_Terminal_Service
{

	/**
	 * @var EasyPay_Terminal_Auth_Manager
	 */
	private $_auth;

	//----------------------------------------------------------------------

	/**
	 * @return EasyPay_Terminal_Auth_Manager
	 */
	public function auth()
	{
		if (is_null($this->_auth))
		{
			$this->_auth = EasyPay_Terminal_Auth_Manager::getInstance();
		}

		return $this->_auth;
	}

	/**
	 * @param System_Controller_Request_HTTP $httpRequest
	 * @return EasyPay_Terminal_Response_Abstract
	 */
	public function processRequest(System_Controller_Request_HTTP $httpRequest)
	{
		try
		{
			$this->_dumpRequest($httpRequest);
			$this->_authorize($httpRequest);
			$terminalRequest = $this->_route($httpRequest);
			$terminalRequest->validate();
			$response = $this->_dispatch($terminalRequest);
		}
		catch (EasyPay_Terminal_Request_Exception_ValidatorError $e)
		{
			$response = new EasyPay_Terminal_Response_Error($httpRequest->getParam('TransactionId'));
			$response->setValidatorErrors($e->getValidator());
		}
		catch (EasyPay_Exception_Abstract $e)
		{
			$response = new EasyPay_Terminal_Response_Error($httpRequest->getParam('TransactionId'));
			$response->setExceptionErrors($e);
		}
		catch (Exception $e)
		{
			$response = new EasyPay_Terminal_Response_Error($httpRequest->getParam('TransactionId'));
			$response->setErrors(['internalApplicationError' => $e->getMessage()]);
			(new Application_Error_Manager())->log($e, $httpRequest);
		}

		return $response;
	}

	/**
	 * @param System_Controller_Request_HTTP $httpRequest
	 */
	protected function _dumpRequest(System_Controller_Request_HTTP $httpRequest)
	{
		$paramDump = $httpRequest->getParam('dump_request');
		if (!$paramDump)
		{
			return;
		}

		Development_Debug::dumpDie($httpRequest->getParams());
	}

	/**
	 * @param System_Controller_Request_HTTP $httpRequest
	 * @throws EasyPay_Terminal_Service_Exception_InnvalidAction
	 * @return EasyPay_Terminal_Request_Abstract
	 */
	protected function _route(System_Controller_Request_HTTP $httpRequest)
	{
		$params = $httpRequest->getParams();
		switch (TRUE)
		{
			case isset($params['Check']):
			{
				$terminalRequest = new EasyPay_Terminal_Action_RechargeOpportunityCheck_Request();
				$terminalRequest
					->setMethodName('RechargeOpportunityCheck')
					->setHttpRequest($httpRequest)
				;
				break;
			}
			case isset($params['Payment']):
			{
				$terminalRequest = new EasyPay_Terminal_Action_RechargePayment_Request();
				$terminalRequest
					->setMethodName('RechargePayment')
					->setHttpRequest($httpRequest)
				;
				break;
			}
			case isset($params['Cancel']):
			{
				$terminalRequest = new EasyPay_Terminal_Action_RechargeCancel_Request();
				$terminalRequest
					->setMethodName('RechargeCancel')
					->setHttpRequest($httpRequest)
				;
				break;
			}
			case isset($params['Confirm']) || isset($params['Status']):
			{
				$terminalRequest = new EasyPay_Terminal_Action_RechargePerform_Request();
				$terminalRequest
					->setMethodName('RechargePerform')
					->setHttpRequest($httpRequest)
				;
				break;
			}
			default:
			{
				throw new EasyPay_Terminal_Service_Exception_InvalidAction();
			}
		}

		$terminalRequest->fill($httpRequest);

		return $terminalRequest;
	}

	protected function _authorize(System_Controller_Request_HTTP $httpRequest)
	{
		$auth = $this->auth();
		$auth
			->setHttpRequest($httpRequest)
			->identify()
			->authorize()
		;
	}

	/**
	 * @param EasyPay_Terminal_Request_Abstract $terminalRequest
	 * @return EasyPay_Terminal_Response_Abstract
	 */
	protected function _dispatch(EasyPay_Terminal_Request_Abstract $terminalRequest)
	{
		$action = $this->_actionFactory($terminalRequest);

		$action
			->setAuth($this->_auth)
			->setRequest($terminalRequest)
			->perform()
		;

		$response = $action->getResponse();

		return $response;
	}

	/**
	 * @param EasyPay_Terminal_Request_Abstract $terminalRequest
	 * @return EasyPay_Terminal_Action_Abstract
	 */
	protected function _actionFactory(EasyPay_Terminal_Request_Abstract $terminalRequest)
	{
		$methodName = $terminalRequest->getMethodName();

		if ($methodName == 'RechargeOpportunityCheck')
		{
			$action = new EasyPay_Terminal_Action_RechargeOpportunityCheck_Model();
		}

		if ($methodName == 'RechargePayment')
		{
			$action = new EasyPay_Terminal_Action_RechargePayment_Model();
		}

		if ($methodName == 'RechargePerform')
		{
			$action = new EasyPay_Terminal_Action_RechargePerform_Model();
		}

		if ($methodName == 'RechargeCancel')
		{
			$action = new EasyPay_Terminal_Action_RechargeCancel_Model();
		}

		System_Exception_NotObject::getInstance()->validate($action);

		return $action;
	}

}