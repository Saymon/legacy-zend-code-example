<?php

/**
 * @author Sergey Ivanov
 */
class EasyPay_Terminal_Action_RechargePayment_Response extends EasyPay_Terminal_Response_Success
{

	/**
	 * @var array
	 */
	protected $_result;

	/**
	 * @param array $result
	 */
	public function __construct(array $result)
	{
		$this->_result = $result;

		parent::__construct();
	}

	/* (non-PHPdoc)
	 * @see EasyPay_Terminal_Response_Abstract::render()
	 */
	public function render()
	{
		$statusOrder = 0;
		switch ($this->_result['status'])
		{
			case EasyPay_Terminal_Recharge_Status_Enum::CREATED:
				$this->setCode(0);
				break;
		}

		$strDateFormat = 'YYYY-MM-ddTHH:mm:ss';

		$objXmlGenerator = new Tools_Xml_Generator();
		$objXmlGenerator->openElement('Response', NULL);
			$objXmlGenerator->openElement('StatusCode', $this->_code);
			$objXmlGenerator->closeElement('StatusCode');
			$objXmlGenerator->openElement('StatusDetail', 'Ok');
			$objXmlGenerator->closeElement('StatusDetail');
			$objXmlGenerator->openElement('DateTime', System_Date_Utc::now()->toString($strDateFormat));
			$objXmlGenerator->closeElement('DateTime');
			$objXmlGenerator->openElement('PaymentId', $this->_result['order_id']);
			$objXmlGenerator->closeElement('PaymentId');
		$objXmlGenerator->closeElement('Response');

		return $objXmlGenerator->flush();
	}

}