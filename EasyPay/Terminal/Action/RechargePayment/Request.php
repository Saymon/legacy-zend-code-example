<?php

/**
 * @author Sergey Ivanov
 */
class EasyPay_Terminal_Action_RechargePayment_Request extends EasyPay_Terminal_Request_Abstract
{

	/**
	 * дата платежа
	 *
	 * @var string
	 */
	public $transactionDatetime;

	/**
	 * номер телефона пользователя
	 *
	 * @var string
	 */
	public $recipientName;

	/**
	 * сумма к зачислению
	 *
	 * @var float
	 */
	public $amountToEnroll;

	/**
	 * номер терминала в системе EasyPay
	 *
	 * @var integer
	 */
	public $partnerObjectId;

	/**
	 * Commission
	 *
	 * @var float
	 */
	public $commission;

	/**
	 * orderId
	 *
	 * @var string
	 */
	public $orderId;

	/**
	 * @param System_Controller_Request_HTTP $httpRequest
	 */
	public function fill(System_Controller_Request_HTTP $httpRequest)
	{
		$paramPayment = $httpRequest->getParam('Payment');

		$this->transactionDatetime		= trim($httpRequest->getParam('DateTime'));
		$this->recipientName			= '+' . trim($paramPayment['Account'], ' +');
		$this->amountToEnroll			= trim($paramPayment['Amount']);
		$this->partnerObjectId			= isset($paramPayment['PartnerObjectId']) ? trim($paramPayment['PartnerObjectId']) : '';
		$this->commission				= isset($paramPayment['Commission']) ? trim($paramPayment['Commission']) : '';
		$this->orderId					= trim($paramPayment['OrderId']);
	}

	/* (non-PHPdoc)
	 * @see EasyPay_Terminal_Request_Abstract::_validatorInit()
	 */
	protected function _initValidator()
	{
		$datetimeFormatPattern = '/' . System_Form_Element_Html5_Validate_Pattern::MYSQL_DATETIME_WITH_T . '/';

		$tblRecharge = new EasyPay_Terminal_Recharge_Table();

		$this->_validator()
			->add(
					new Core_Validation_Validated_KeyValue('orderId', $this->orderId),
					(new Core_Validation_Chain())
					->add(new Core_Validate_NotEmpty(), TRUE)
					->add(new Core_Validate_Int(), TRUE)
					->add(new System_Validate_Unsigned(), TRUE)
					->add(new Core_Validate_Digits(), TRUE)
					->add(new Core_Validate_Db_NoRecordExists([
						'adapter' => $tblRecharge->getAdapter(),
						'table'   => $tblRecharge->getTableName(),
						'field'   => 'easypay_order_id'
					]), TRUE)
			)
			->add(
					new Core_Validation_Validated_KeyValue('transactionDatetime', $this->transactionDatetime),
					(new Core_Validation_Chain())
					->add(new Core_Validate_NotEmpty(), TRUE)
					->add(new System_Validate_DateFormat($datetimeFormatPattern), TRUE)
			)
			->add(
					new Core_Validation_Validated_KeyValue('recipientName', $this->recipientName),
					(new Core_Validation_Chain())
						->add(new Core_Validate_NotEmpty(), TRUE)
						->add(new System_Validate_Phone(), TRUE)
			)
			->add(
					new Core_Validation_Validated_KeyValue('amountToEnroll', $this->amountToEnroll),
					(new Core_Validation_Chain())
					->add(new Core_Validate_NotEmpty(), TRUE)
					->add(new System_Validate_Amount_Format(System_Float::dim('amount')), TRUE)
					->add(new System_Validate_Unsigned(), TRUE)
					->add(new Core_Validate_GreaterThan(10), TRUE)
					->add(new Core_Validate_LessThan(5000), TRUE)
			)
		;
	}

}