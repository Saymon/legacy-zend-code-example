<?php

/**
 * @author Sergey Ivanov
 */
class EasyPay_Terminal_Action_DayReport_Request extends EasyPay_Terminal_Request_Abstract
{

	/**
	 * @var string
	 */
	public $checkDateBegin;

	/**
	 * @var string
	 */
	public $checkDateEnd;

	/**
	 * @param System_Controller_Request_HTTP $httpRequest
	 */
	public function fill(System_Controller_Request_HTTP $httpRequest)
	{
		$this->checkDateBegin 	= $httpRequest->getParam('CheckDateBegin');
		$this->checkDateEnd 	= $httpRequest->getParam('CheckDateEnd');
	}

	/* (non-PHPdoc)
	 * @see EasyPay_Terminal_Request_Abstract::_validatorInit()
	 */
	protected function _initValidator()
	{
		$datetimeFormatPattern = '/' . System_Form_Element_Html5_Validate_Pattern::DATE_YYYYmmddHHss . '/';
		$tblRecharge = new EasyPay_Terminal_Recharge_Table();
		$this->_validator()
			->add(
					(new Core_Validation_Validated_KeyValue('checkDateBegin', $this->checkDateBegin)),
					(new Core_Validation_Chain())
						->add(new Core_Validate_NotEmpty(), TRUE)
						->add(new System_Validate_DateFormat($datetimeFormatPattern), TRUE)
			)
			->add(
					(new Core_Validation_Validated_KeyValue('checkDateEnd', $this->checkDateEnd)),
					(new Core_Validation_Chain())
						->add(new Core_Validate_NotEmpty(), TRUE)
						->add(new System_Validate_DateFormat($datetimeFormatPattern), TRUE)
			)
		;
	}

}