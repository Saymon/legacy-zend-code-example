<?php

/**
 * @author Sergey Ivanov
 */
class EasyPay_Terminal_Action_DayReport_Model extends EasyPay_Terminal_Action_Abstract
{

	/* (non-PHPdoc)
	 * @see EasyPay_Terminal_Action_Abstract::perform()
	 */
	public function perform()
	{
		$request	= $this->_getRequest();

		$tblRecharge = new EasyPay_Terminal_Recharge_Table();
		$tblRechargeCancel = new EasyPay_Terminal_Recharge_Cancel_Table();


		$selectRecharge = $tblRecharge->select()
			->from($tblRecharge->getTableName())
			->where('EasyPay_transaction_datetime_created > ? ', $request->checkDateBegin)
			->where('EasyPay_transaction_datetime_created < ? ', $request->checkDateEnd)
			->where('status = ? ', EasyPay_Terminal_Recharge_Status_Enum::COMPLETED)
		;
		$selectRechargeCancel = $tblRechargeCancel->select()
			->from($tblRechargeCancel->getTableName())
			->where('EasyPay_transaction_datetime_created > ? ', $request->checkDateBegin)
			->where('EasyPay_transaction_datetime_created < ? ', $request->checkDateEnd)
			->where('status = ? ', EasyPay_Terminal_Recharge_Cancel_Status_Enum::CONFIRMED)
		;

		$rowsetRecharge = $tblRecharge->fetchAll($selectRecharge);
		$rowsetRechargeCancel = $tblRechargeCancel->fetchAll($selectRechargeCancel);

		$response = new EasyPay_Terminal_Action_DayReport_Response($rowsetRecharge, $rowsetRechargeCancel);
		$this->_setResponse($response);
	}

}