<?php

/**
 * @author Sergey Ivanov
 */
class EasyPay_Terminal_Action_DayReport_Response extends EasyPay_Terminal_Response_Success
{

	/**
	 * @var System_Db_List
	 */
	private $_rowsetRecharge;

	/**
	 * @var System_Db_List
	 */
	private $_rowsetRechargeCancel;

	/**
	 * @param System_Db_List $rowsetRecharge
	 * @param System_Db_List $rowsetRechargeCancel
	 */
	public function __construct($rowsetRecharge, $rowsetRechargeCancel)
	{
		header('Content-type:text/xml');

		$this->_rowsetRecharge 			= $rowsetRecharge;
		$this->_rowsetRechargeCancel 	= $rowsetRechargeCancel;
	}

	/* (non-PHPdoc)
	 * @see EasyPay_Terminal_Response_Abstract::render()
	 */
	public function render()
	{
		$objXmlGenerator = new Tools_Xml_Generator();

		$objXmlGenerator->openElement('Response', NULL);
		// обычные платежи
		foreach ($this->_rowsetRecharge as $row)
		{
			$objXmlGenerator->openElement('Payment', NULL);
				$objXmlGenerator->openElement('TransactionId', $row->EasyPayTransactionId());
				$objXmlGenerator->closeElement('TransactionId');
				$objXmlGenerator->openElement('Account', $row->recipientName());
				$objXmlGenerator->closeElement('Account');
				$objXmlGenerator->openElement('TransactionDate', $row->EasyPayTransactionDatetimeCreated());
				$objXmlGenerator->closeElement('TransactionDate');
				$objXmlGenerator->openElement('Amount', $row->amountToEnroll());
				$objXmlGenerator->closeElement('Amount');
			$objXmlGenerator->closeElement('Payment');

		}
		// отмененные платежи
		foreach ($this->_rowsetRechargeCancel as $row)
		{
			$objXmlGenerator->openElement('Payment', NULL);
				$objXmlGenerator->openElement('TransactionId', $row->EasyPayTransactionId());
				$objXmlGenerator->closeElement('TransactionId');
				$objXmlGenerator->openElement('Account', $row->recipientName());
				$objXmlGenerator->closeElement('Account');
				$objXmlGenerator->openElement('TransactionDate', $row->EasyPayTransactionDatetimeCreated());
				$objXmlGenerator->closeElement('TransactionDate');
				$objXmlGenerator->openElement('Amount', $row->amountRevert());
				$objXmlGenerator->closeElement('Amount');
			$objXmlGenerator->closeElement('Payment');

		}
		$objXmlGenerator->closeElement('Response');
		return $objXmlGenerator->flush();
	}

}