<?php

/**
 * @author Sergey Ivanov
 */
class EasyPay_Terminal_Action_RechargeOpportunityCheck_Response extends EasyPay_Terminal_Response_Success
{

	/**
	 * @param intager $transactionId
	 */
	public function __construct($transactionId)
	{
		$this->setTransactionId($transactionId);

		parent::__construct();
	}

	/* (non-PHPdoc)
	 * @see EasyPay_Terminal_Response_Abstract::render()
	 */
	public function render()
	{
		$strDateFormat = 'YYYY-MM-ddTHH:mm:ss';

		$objXmlGenerator = new Tools_Xml_Generator();

		$objXmlGenerator->openElement('Response', NULL);
			$objXmlGenerator->openElement('StatusCode', $this->_code);
			$objXmlGenerator->closeElement('StatusCode');
			$objXmlGenerator->openElement('StatusDetail', 'ОК');
			$objXmlGenerator->closeElement('StatusDetail');
			$objXmlGenerator->openElement('DateTime', System_Date_Utc::now()->toString($strDateFormat));
			$objXmlGenerator->closeElement('DateTime');
		$objXmlGenerator->closeElement('Response');

		return $objXmlGenerator->flush();
	}

}