<?php

/**
 * @author Sergey Ivanov
 */
class EasyPay_Terminal_Action_RechargeOpportunityCheck_Request extends EasyPay_Terminal_Request_Abstract
{

	/**
	 * @var string
	 */
	public $recipientName;

	/**
	 * @param System_Controller_Request_HTTP $httpRequest
	 */
	public function fill(System_Controller_Request_HTTP $httpRequest)
	{
		$params = $httpRequest->getParam('Check');
		$this->recipientName 			= '+' . trim($params['Account'], ' +');
	}

	/* (non-PHPdoc)
	 * @see EasyPay_Terminal_Request_Abstract::_validatorInit()
	 */
	protected function _initValidator()
	{
		$this->_validator()
			->add(
					(new Core_Validation_Validated_KeyValue('recipientName', $this->recipientName)),
					(new Core_Validation_Chain())
						->add(new Core_Validate_NotEmpty(), TRUE)
						->add(new System_Validate_Phone(), TRUE)
			)
		;
	}

}