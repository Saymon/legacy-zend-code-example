<?php

/**
 * @author Sergey Ivanov
 */
class EasyPay_Terminal_Action_RechargeOpportunityCheck_Model extends EasyPay_Terminal_Action_Abstract
{

	/* (non-PHPdoc)
	 * @see EasyPay_Terminal_Action_Abstract::perform()
	 */
	public function perform()
	{
		$request	= $this->_getRequest();

		$manager = new EasyPay_Bridge_Manager();
		$bridge = $manager->getCurrent();

		$bridge->rechargeOpportunityCheck($request);
		$response = new EasyPay_Terminal_Action_RechargeOpportunityCheck_Response($request->getHttpRequest()->getParam('TransactionId'));

		$this->_setResponse($response);
	}

}