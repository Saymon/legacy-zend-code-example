<?php

/**
 * @author Sergey Ivanov
 */
abstract class EasyPay_Terminal_Action_Abstract
{
	
	/**
	 * @var EasyPay_Terminal_Auth_Manager
	 */
	private $_auth;
	
	/**
	 * @var EasyPay_Terminal_Request_Abstract
	 */
	private $_request;
	
	/**
	 * @var EasyPay_Terminal_Response_Abstract
	 */
	private $_response;
	
	abstract public function perform();
	
	/**
	 * @param EasyPay_Terminal_Auth_Manager $auth
	 * @return EasyPay_Terminal_Action_Abstract
	 */
	public function setAuth($auth)
	{
		$this->_auth = $auth;
		
		return $this;
	}
	
	/**
	 * @param EasyPay_Terminal_Request_Abstract $terminalRequest
	 * @return EasyPay_Terminal_Action_Abstract
	 */
	public function setRequest(EasyPay_Terminal_Request_Abstract $terminalRequest)
	{
		$this->_request = $terminalRequest;
		
		return $this;
	}
	
	/**
	 * @return EasyPay_Terminal_Response_Abstract
	 */
	public function getResponse()
	{
		return $this->_response;
	}
	
	/**
	 * @return EasyPay_Terminal_Auth_Manager
	 */
	protected function _getAuth()
	{
		return $this->_auth;
	}
	
	/**
	 * @return EasyPay_Terminal_Request_Abstract
	 */
	protected function _getRequest()
	{
		return $this->_request;
	}
	
	/**
	 * @param EasyPay_Terminal_Response_Abstract $terminalResponse
	 * @return EasyPay_Terminal_Action_Abstract
	 */
	protected function _setResponse(EasyPay_Terminal_Response_Abstract $terminalResponse)
	{
		$this->_response = $terminalResponse;
		
		return $this;
	}
	
}