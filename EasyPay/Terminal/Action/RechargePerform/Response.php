<?php

/**
 * @author Sergey Ivanov
 */
class EasyPay_Terminal_Action_RechargePerform_Response extends EasyPay_Terminal_Response_Success
{

	/**
	 * @var array
	 */
	protected $_result;

	/**
	 * @param array $result
	 */
	public function __construct(array $result)
	{
		$this->_result = $result;

// 		parent::__construct();
	}

	/* (non-PHPdoc)
	 * @see EasyPay_Terminal_Response_Abstract::render()
	 */
	public function render()
	{
		$statusOrder = 0;
		switch ($this->_result['status'])
		{
			case EasyPay_Terminal_Recharge_Status_Enum::CREATED:
			case EasyPay_Terminal_Recharge_Status_Enum::PROCESSING:
				$statusDetails = 'InProccess';
				$this->setCode(-1);
				break;
			case EasyPay_Terminal_Recharge_Status_Enum::COMPLETED:
				$statusDetails = 'Ok';
				$this->setCode(0);
				break;
			case EasyPay_Terminal_Recharge_Status_Enum::FAILED:
				$statusDetails = 'Failed';
				$this->setCode(2);
				break;
		}
		$strDateFormat = 'YYYY-MM-ddTHH:mm:ss';
		$objXmlGenerator = new Tools_Xml_Generator();
		$objXmlGenerator->openElement('Response', NULL);
			$objXmlGenerator->openElement('StatusCode', $this->_code);
			$objXmlGenerator->closeElement('StatusCode');
			$objXmlGenerator->openElement('StatusDetail', $statusDetails);
			$objXmlGenerator->closeElement('StatusDetail');
			$objXmlGenerator->openElement('DateTime', $this->_result['date']);
			$objXmlGenerator->closeElement('DateTime');
			$objXmlGenerator->openElement('OrderDate', str_replace(' ', 'T', $this->_result['paymentDate']));
			$objXmlGenerator->closeElement('OrderDate');
		$objXmlGenerator->closeElement('Response');

		return $objXmlGenerator->flush();
	}

}