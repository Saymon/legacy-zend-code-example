<?php

/**
 * @author Sergey Ivanov
 */
class EasyPay_Terminal_Action_RechargeCancel_Response extends EasyPay_Terminal_Response_Success
{

	/**
	 * @var EasyPay_Terminal_Recharge_Cancel_Row
	 */
	private $_rowCancel;

	/**
	 * @param intager $transactionId
	 */
	public function __construct($rowCancel)
	{
		header('Content-type:text/xml');

		$this->_rowCancel = $rowCancel;
		$this->setCode(100);
	}

	/* (non-PHPdoc)
	 * @see EasyPay_Terminal_Response_Abstract::render()
	 */
	public function render()
	{
		switch ($this->_rowCancel->status)
		{
			case EasyPay_Terminal_Recharge_Cancel_Status_Enum::CREATED:
			case EasyPay_Terminal_Recharge_Cancel_Status_Enum::PROCESSING:
				$this->setCode(100);
				$statusDetails = 'InProccess';
				break;
			case EasyPay_Terminal_Recharge_Cancel_Status_Enum::CONFIRMED:
				$this->setCode(0);
				$statusDetails = 'Declined';
				break;
			case EasyPay_Terminal_Recharge_Cancel_Status_Enum::REJECTED:
				$this->setCode(22);
				$statusDetails = 'Rejected';
				break;
		}

		$objXmlGenerator = new Tools_Xml_Generator();

		$strDateFormat = 'YYYY-MM-ddTHH:mm:ss';
		$objXmlGenerator = new Tools_Xml_Generator();
		$objXmlGenerator->openElement('Response', NULL);
			$objXmlGenerator->openElement('StatusCode', $this->_code);
			$objXmlGenerator->closeElement('StatusCode');
			$objXmlGenerator->openElement('StatusDetail', $statusDetails);
			$objXmlGenerator->closeElement('StatusDetail');
			$objXmlGenerator->openElement('DateTime', System_Date_Utc::now()->toString($strDateFormat));
			$objXmlGenerator->closeElement('DateTime');
			$objXmlGenerator->openElement('CancelDate', str_replace(' ', 'T', $this->_rowCancel->inserted));
			$objXmlGenerator->closeElement('CancelDate');
		$objXmlGenerator->closeElement('Response');

		return $objXmlGenerator->flush();
	}

}