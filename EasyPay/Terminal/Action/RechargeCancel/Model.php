<?php

/**
 * @author Sergey Ivanov
 */
class EasyPay_Terminal_Action_RechargeCancel_Model extends EasyPay_Terminal_Action_Abstract
{

	/* (non-PHPdoc)
	 * @see EasyPay_Terminal_Action_Abstract::perform()
	 */
	public function perform()
	{
		$request	= $this->_getRequest();

		$manager = new EasyPay_Terminal_Recharge_Cancel_Manager();
		$row = $manager->addIfNotExists($request->paymentId, $request->dateTime);

		$response = new EasyPay_Terminal_Action_RechargeCancel_Response($row);
		$this->_setResponse($response);
	}

}