<?php

/**
 * @author Sergey Ivanov
 */
class EasyPay_Terminal_Action_RechargeCancel_Request extends EasyPay_Terminal_Request_Abstract
{

	/**
	 * @var string
	 */
	public $dateTime;

	/**
	 * @var string
	 */
	public $paymentId;

	/**
	 * @param System_Controller_Request_HTTP $httpRequest
	 */
	public function fill(System_Controller_Request_HTTP $httpRequest)
	{
		$params = $httpRequest->getParam('Cancel');
		$this->dateTime 		= trim($httpRequest->getParam('DateTime'));
		$this->paymentId 		= trim($params['PaymentId']);
	}

	/* (non-PHPdoc)
	 * @see EasyPay_Terminal_Request_Abstract::_validatorInit()
	 */
	protected function _initValidator()
	{
		$datetimeFormatPattern = '/' . System_Form_Element_Html5_Validate_Pattern::MYSQL_DATETIME_WITH_T . '/';
		$tblRecharge = new EasyPay_Terminal_Recharge_Table();

		$this->_validator()
			->add(
					new Core_Validation_Validated_KeyValue('paymentId', $this->paymentId),
					(new Core_Validation_Chain())
					->add(new Core_Validate_NotEmpty(), TRUE)
					->add(new Core_Validate_Int(), TRUE)
					->add(new System_Validate_Unsigned(), TRUE)
					->add(new Core_Validate_Db_RecordExists([
							'adapter' => $tblRecharge->getAdapter(),
							'table'   => $tblRecharge->getTableName(),
							'field'   => 'id'
					]), TRUE)
			)
			->add(
					new Core_Validation_Validated_KeyValue('dateTime', $this->dateTime),
					(new Core_Validation_Chain())
					->add(new Core_Validate_NotEmpty(), TRUE)
					->add(new System_Validate_DateFormat($datetimeFormatPattern), TRUE)
			)
		;
	}

}