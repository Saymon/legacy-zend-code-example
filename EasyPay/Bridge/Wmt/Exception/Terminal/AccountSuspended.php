<?php

/**
 * @author Sergey Ivanov
 */
class EasyPay_Bridge_Wmt_Exception_Terminal_AccountSuspended extends EasyPay_Bridge_Exception_Abstract
{
	
	/**
	 * @var string
	 */
	protected $_key = EasyPay_Exception_Enum::SERVICE_TERMINAL_ACCOUNT_SUSPENDED;
	
}