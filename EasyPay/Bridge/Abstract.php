<?php

/**
 * @author Sergey Ivanov
 */
abstract class EasyPay_Bridge_Abstract
{

	/**
	 * @param EasyPay_Terminal_Request_Abstract $request
	 */
	abstract public function rechargeOpportunityCheck(EasyPay_Terminal_Request_Abstract $request);

	/**
	 * @param EasyPay_Terminal_Request_Abstract $request
	 */
	abstract public function rechargePerform(EasyPay_Terminal_Request_Abstract $request);

	/**
	 * @param EasyPay_Terminal_Request_Abstract $request
	 */
	abstract public function createRecharge(EasyPay_Terminal_Request_Abstract $request);

	/**
	 * @return boolean
	 */
	abstract public function isServiceAvailable();

}