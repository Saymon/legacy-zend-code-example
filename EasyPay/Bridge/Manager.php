<?php

/**
 * @author Sergey Ivanov
 */
class EasyPay_Bridge_Manager
{
	
	use System_Singleton;
	
	/**
	 * @var EasyPay_Bridge_Abstract
	 */
	private $_current;
	
	/**
	 * @return EasyPay_Bridge_Abstract
	 */
	public function getCurrent()
	{
		if (is_null($this->_current))
		{
			$this->_current = EasyPay_Component::getInstance()->getBridge();
		}
		
		return $this->_current;
	}
	
}