<?php

/**
 * @author Sergey Ivanov
 */
class EasyPay_Bridge_Wmt extends EasyPay_Bridge_Abstract
{

	/**
	 * @var Bank_Person_Row
	 */
	private $_bankPerson;

	/**
	 * @var UsersCenter_Account_Row
	 */
	private $_userAccount;

	/* (non-PHPdoc)
	 * Проверка включен ли сервис обслуживания терминалов
	 * @see EasyPay_Bridge_Abstract::isServiceAvailable()
	 */
	public function isServiceAvailable()
	{
		$this->_isTerminalServiceEnabled();
	}

	/* (non-PHPdoc)
	 * @see EasyPay_Bridge_Abstract::rechargeOpportunityCheck()
	 */
	public function rechargeOpportunityCheck(EasyPay_Terminal_Request_Abstract $terminalRequest)
	{
		$this
			->_isWmtAccountExists($terminalRequest->recipientName)
			->_isWmtUserBanned()
			->_isBankAccountExists()
		;
	}

	/* (non-PHPdoc)
	 * @see EasyPay_Bridge_Abstract::createRecharge()
	 */
	public function createRecharge(EasyPay_Terminal_Request_Abstract $terminalRequest)
	{
		$this->rechargeOpportunityCheck($terminalRequest);
		/* @var $terminalRequest EasyPay_Terminal_Action_RechargePerform_Request */
		$rechargeManager = new EasyPay_Terminal_Recharge_Manager();
		$rowRecharge = $rechargeManager->addIfNotExists(
			$terminalRequest->transactionDatetime,
			$terminalRequest->recipientName,
			$terminalRequest->amountToEnroll,
			$terminalRequest->partnerObjectId,
			$terminalRequest->commission,
			$terminalRequest->orderId
		);

		$result = [
			'status'			=> $rowRecharge->status(),
			'transaction_id'	=> $terminalRequest->orderId,
			'order_id'			=> $rowRecharge->id()
		];

		return $result;

	}

	/* (non-PHPdoc)
	 * @see EasyPay_Bridge_Abstract::rechargePerform()
	 */
	public function rechargePerform(EasyPay_Terminal_Request_Abstract $terminalRequest)
	{
		/*@var $rowRecharge EasyPay_Terminal_Recharge_Row */
		$rowRecharge = (new EasyPay_Terminal_Recharge_Table())->fetchAllByField('id', $terminalRequest->orderId)->current();

		if(!is_object($rowRecharge))
		{
			throw new EasyPay_Terminal_Service_Exception_InvalidEasyPayOrder();
		}

		$bank = new Bank_PaymentOrder_Manager();

		try
		{
			$order = $rowRecharge->getPaymentOrder();
			if (is_null($order))
			{
				$rowRecharge->status(EasyPay_Terminal_Recharge_Status_Enum::PROCESSING)->save();
				/* @var $order Bank_PaymentOrder_Model_Load */
				$order = $bank->performLoad($rowRecharge);
				$rowRecharge->status(EasyPay_Terminal_Recharge_Status_Enum::COMPLETED)->save();
			}
		}
		catch (Exception $e)
		{
			$rowRecharge->status(EasyPay_Terminal_Recharge_Status_Enum::FAILED)->save();
			$httpRequest = $terminalRequest->getHttpRequest();
			$httpRequest->setParam('easypay_terminal_recharge_id', $rowRecharge->id());
			(new Application_Error_Manager())->log($e, $httpRequest);//логирование оригинальной ошибки
			throw new EasyPay_Bridge_Wmt_Exception_Terminal_AccountRechargeFailed();
		}

		switch ($order->getStatus())
		{
			case Pay_Order_Status_Enum::CREATED:
			case Pay_Order_Status_Enum::PROCESSING:
			case Pay_Order_Status_Enum::PENDING:
				$strOrderStatus = 'InProcess';
				break;
			case Pay_Order_Status_Enum::COMPLETED:
				$strOrderStatus = 'Accepted';
				break;
			case Pay_Order_Status_Enum::CANCELED:
				$strOrderStatus = 'Declined';
				break;
		}
		$result = [
			'status'			=> $rowRecharge->status(),
			'stausDetails'		=> 'Ok',
			'orderStatus'		=> $strOrderStatus,
			'paymentDate'		=> $order->getDateTimeCreated(),
			'date'				=> $terminalRequest->confirmDateTime
		];

		return $result;
	}

	/**
	 * @param string $recipientName
	 * @return Bank_Account_Row
	 */
	public function getAccount($recipientName)
	{
		$bankPerson = Bank_Person_Manager::getInstance()->getByName($recipientName, TRUE);
		return $bankPerson->getAccountByCurrency('WMTUAH');
	}

	/**
	 * @throws EasyPay_Terminal_Service_Exception
	 * @return EasyPay_Bridge_Wmt
	 */
	protected function _isTerminalServiceEnabled()
	{
		if ((int)EasyPay_Option_Manager::getInstance()->serviceTerminalStatus() !== EasyPay_Terminal_Service_Status_Enum::ENABLED)
		{
			throw new EasyPay_Terminal_Service_Exception_IntentionallyDisabled();
		}

		return $this;
	}

	/**
	 * @param string $bankPersonName
	 * @throws EasyPay_Terminal_Service_Exception
	 * @return EasyPay_Bridge_Wmt
	 */
	protected function _isWmtAccountExists($bankPersonName)
	{
		$userAccount = UsersCenter_Account_Manager::getInstance()->getByName($bankPersonName, FALSE);

		if (is_null($userAccount))
		{
			throw new EasyPay_Bridge_Wmt_Exception_Terminal_AccountNotFound();
		}

		$this->_userAccount = $userAccount;

		$manager = new Bank_Person_Manager();

		$person = $manager->getByName($bankPersonName, FALSE);
		if (is_null($person))
		{
			throw new EasyPay_Bridge_Wmt_Exception_Terminal_AccountNotFound();
		}

		$this->_bankPerson = $person;

		return $this;
	}

	/**
	 * @throws EasyPay_Terminal_Service_Exception
	 * @return EasyPay_Bridge_Wmt
	 */
	protected function _isWmtUserBanned()
	{
		if ($this->_userAccount->status() !== UsersCenter_Account_StatusList::NORMAL)
		{
			throw new EasyPay_Bridge_Wmt_Exception_Terminal_AccountSuspended();
		}

		return $this;
	}

	/**
	 * @throws EasyPay_Terminal_Service_Exception
	 * @return EasyPay_Bridge_Wmt
	 */
	protected function _isBankAccountExists()
	{
		$bankAccount = $this->_bankPerson->getAccountByCurrency('WMTRUB', FALSE);
		if (is_null($bankAccount))
		{
			throw new EasyPay_Bridge_Wmt_Exception_Terminal_WalletNotFound();
		}


		return $this;
	}



}