<?php

/**
 * @author Sergey Ivanov
 *
 * @method EasyPay_Model_Manager modelManager()
 */
class EasyPay_Component extends System_Component_Abstract
{

	use System_Singleton;

	//---------------------------------------------------------------------------------------------------------------------------------------

	/* (non-PHPdoc)
	 * @see System_Component_Abstract::update($objVersionCurrent)
	 */
	public function update($objVersionCurrent)
	{
		/**
		 * Install table
		 * */
		$strVersion = '0.0.2';
		if ($objVersionCurrent->isOlder($strVersion))
		{
			$this->modelManager()->modTerminalRecharge()->table()->install();
			$this->modelManager()->modTerminalRechargeCancel()->table()->install();
			$this->modelManager()->modTerminalRechargeStatus()->table()->install();
			$objVersionCurrent->setVersion($strVersion)->save();
		}

		/**
		 * Install table
		 * */
		$strVersion = '0.0.3';
		if ($objVersionCurrent->isOlder($strVersion))
		{
			$this->modelManager()->modTerminalRechargeCancel()->table()->addField('revert_id');
			$objVersionCurrent->setVersion($strVersion)->save();
		}
	}

	/**
	 * @return EasyPay_Bridge_Abstract
	 */
	public function getBridge()
	{
		$bridgeName			= $this->getConfig()->bridgeName;

		$bridgeModelName	= 'EasyPay_Bridge_' . ucfirst(strtolower($bridgeName));
		return new $bridgeModelName;
	}

}