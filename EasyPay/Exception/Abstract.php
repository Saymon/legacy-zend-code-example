<?php

/**
 * @author Sergey Ivanov
 */
abstract class EasyPay_Exception_Abstract extends System_Exception
{

	/**
	 * Смысл тот же что и у поля $this->code, только строковый.
	 * Необходимо для того чтобы ошибки имели не только текст но и код.
	 * @var string
	 */
	protected $_key;

	/**
	 *
	 */
	public function __construct()
	{
		if (!$this->_key)
		{
			Development_Debug::dumpDie(Tools_ClassName::get($this));
		}
		$enum		= new EasyPay_Exception_Enum();
		$message	= $enum->getMessage($this->_key);

		parent::__construct($message);
	}

	/**
	 * @param string $strKey
	 * @return EasyPay_Exception
	 */
	public function setKey($strKey)
	{
		$this->_key = $strKey;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getKey()
	{
		return $this->_key;
	}

}